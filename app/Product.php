<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        
        'main_image', 
        'back_image', 
        'p_name',
        'release_date',
        'brand_id', 
        'gender_id', 
        'category_id', 
        'st_id',
        'size_chart',
        'moqty_id',
        'margin_id',
        'base_price1',
        'base_price2',
        'description',
        'featured_product',

    ];

     public function product_brand()
    {
        return $this->belongsTo('App\Brands', 'brand_id');
    }

     public function product_category()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }

    public function product_stype()
    {
        return $this->belongsTo('App\Size_types', 'id');
    }

    public function product_sitem()
    {
        return $this->belongsTo('App\Size_items', 'st_id');
    }


    public function product_moqtype()
    {
        return $this->belongsTo('App\Moq_types', 'moqty_id');
    }

     public function product_moqitem()
    {
        return $this->belongsTo('App\Moq_items', 'moqty_id');
    }


    public function product_margin()
    {
        return $this->belongsTo('App\Margins', 'margin_id');
    }


    public function product_price()
    {
        return $this->belongsTo('App\Product_prices', 'id');
    }

    public function variation()
    {
        return $this->hasMany('App\Product_prices', 'p_id');
    }

    public function photo()
    {
        return $this->hasMany('App\ProductPhoto', 'p_id');
    }

    public function product_gender()
    {
        return $this->belongsTo('App\Genders', 'gender_id');
    }

    public function product_profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id');
    }
   
}
