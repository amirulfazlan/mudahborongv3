<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'invoice_number', 
        'product',
        'size',
        'color',
        'quantity',
        'prime',
        'unit_price',
        'sub_price',
        'total'

    ];
}
