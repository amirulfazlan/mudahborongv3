<?php

namespace App\Http\Controllers;



use DB;

use PDF;

use Carbon\Carbon;


use Illuminate\Http\Request;
use App\Product;
use App\Product_prices;
use Cart;
use App\CartTemp;

use Auth;

use App\CartItem;
use App\Size_items;
use App\Size_types;
Use App\Brands ;
use App\Categories;
use App\Materials;
use App\Moq_items;
use App\Prine;
use App\Address;
use App\Currencies;
use App\Margins;
use App\Moq_types;

class InvoiceController extends Controller
{

    public function CreateInvoice(Request $request){

      $id = Auth::user()->id;
      $name = Auth::user()->name;
      $cartrestore = Cart::restore($name);
      $cart = Cart::content($name);
      $date = Carbon::now();

      $moq_item = Moq_items::all()->toArray();
      $moq_type = Moq_types::all()->toArray();    
      $cur = Currencies::all()->toArray();
      $margin = Margins::all()->toArray();
      $pro_var = Product_prices::all()->toArray();
      
      $address = Address::where('user_id' , $id)->firstOrFail(); 

      $cart_group = Cart::content($name)->groupBy('id'); 

      $all_new_array = array();

      foreach ($cart_group as $carts) {

        $same_id = array();
        $cart_total = 0;

        foreach($carts as $ct)
        {


          $cart_total += $ct->qty;


                            $same_id []= array(
                                        'id' => $ct->id,
                                        'qty' => $ct->qty,
                                        'name' => $ct->name,
                                        'size' => $ct->options->size,
                                        'color'=> $ct->options->color,
                                        'price'=>  $ct->price,
                                        'moq_min'=>  $ct->options->min,
                                        'min_check'=> $ct->options->code
                                        
                                   );
        }

            $p_id = $ct->id;

                        $products  = Product::find($p_id);

                        $cart_newprice= $ct->price;

                        if ( $cart_total != $ct->qty ) {

                               foreach ($cur as $yuan) {
                                foreach ($margin as $mar) {

                                    if( $mar['id'] == $products['margin_id']) {

                                        foreach ($moq_item as $moq) {
                                           if($moq['moqty_id'] == $products['moqty_id']){


                                            if( $cart_total >= $moq['min_unit1']  && $cart_total < $moq['min_unit2']){

                                            $admin_price = ( $products['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                                            $whole_price1 =  $admin_price + $mar['mar_whole1'] ;   
                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2;

                                           
                                            }

                                            elseif( $cart_total >= $moq['min_unit2']  && $cart_total < $moq['min_unit3']){

                                            $admin_price = ( $products['base_price2'] +  $mar['mar_admin2']) /  $yuan['cur_value'];
                                            $whole_price1 =  $admin_price + $mar['mar_whole2'] ;
                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                                            }

                                            elseif( $cart_total >= $moq['min_unit3'] ){

                                            $admin_price = ( $products['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                            $whole_price1 =  $admin_price + $mar['mar_whole3'] ;
                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                                            }

                                           

                                           }


                                        }

  
                                    }

                                }

                                 

                               }

                        }

                        $all_new_array [] = array(


                            'item_id'       => $ct->id,
                            'name_item'     => $ct->name,
                            'total_qt'      => $cart_total,                             
                            'unit_price'    => $cart_newprice,
                            'unit_min'      => $ct->options->min,
                            'collection'    => $same_id

                     ); 

      }









     
      return view ('invoice.invoice', compact ('id','cart','address','date','moq_type', 'cur', 'margin','all_new_array', 'cart_group', 'pro_var'))->with('popup','open');

	}	




      public function storeInvoice(Request $request){

            $innum = hexdec(uniqid());



            $invoice = new Invoicestr([

            'invoice_number'          =>      $innum,

            

        ]);
      }
}
