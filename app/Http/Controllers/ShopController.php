<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Product_prices;
use App\Size_types;
use App\Size_items;
use App\Moq_items;
use App\Categories;

use App\Moq_types;
use App\Currencies;
use App\Margins;
use App\ProductPhoto;




use DB;


class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //$product =  Product::all()->toArray();
        //$size_type = Size_types::all()->toArray();
         //$price = Product_prices::paginate(30);

        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $cat = Categories::all()->toArray();

        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();




        

        if (request()->has('category_id')){
        $product = Product::where('gender_id', '1')->where('category_id', request('category_id'))->paginate(30);
        }

        else{

            $product = Product::where('gender_id', '1')->paginate(30);

        }

         return view('shop.index_men', compact('product'), compact('size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin'));
    }

    public function women()
    {

        //$product =  Product::all()->toArray();
        //$size_type = Size_types::all()->toArray();
         //$price = Product_prices::paginate(30);

        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
       
        $cat = Categories::all()->toArray();

        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();

        if (request()->has('category_id')){
        $product = Product::where('gender_id', '2')->where('category_id', request('category_id'))->paginate(30);
        }

        else{

            $product = Product::where('gender_id', '2')->paginate(30);

        }
        
        
         return view('shop.index_women', compact('product'), compact('size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $photo = ProductPhoto::all()->toArray();
       

        
        return view('shop.show', compact('product','id'), compact('size_type', 'size_item', 'moq_item', 'pro_var', 'moq_type', 'cur', 'margin', 'photo') );   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
