<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categories;
use App\Genders;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $cat = Categories::orderBy('category_product')->paginate(20);
        //$cat = Categories::all()->toArray();
        $gen = Genders::all()->toArray();
      
        return view('category.index', compact('cat', 'gen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$category = Categories::all()->toArray();
        //return view('category.create', compact('category'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $cat = new Categories([
            'gender_id'                =>          $request->gender_id,
            'category_product'         =>          $request->category_product
            
        ]);

        $cat->save();
        session()->flash('flash_message', 'Category Added');
        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cat = Categories::find($id);
        $gen = Genders::all()->toArray();
        
        return view('category.edit', compact('cat','id'), compact('gen'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = Categories::findOrFail($id);

         $cat->update([    
            'gender_id'                =>      $request->gender_id,
            'category_product'         =>      $request->category_product    
        ]);

        session()->flash('flash_message', 'Category Updated');
        return redirect('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categories::destroy($id);
        session()->flash('flash_message', 'Category Deleted');
        return redirect('category');
    }
}
