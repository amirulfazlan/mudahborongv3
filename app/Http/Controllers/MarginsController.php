<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Margins;

class MarginsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $margin = Margins::paginate(15);
        $margin = Margins::all()->toArray();
      
        return view('margin.index', compact('margin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $margin = new Margins([
            'name'             =>          $request->name,
            'mar_admin1'       =>          $request->mar_admin1,
            'mar_admin2'       =>          $request->mar_admin2,
            'mar_admin3'       =>          $request->mar_admin3,
            'mar_whole1'       =>          $request->mar_whole1,
            'mar_whole2'       =>          $request->mar_whole2,
            'mar_whole3'       =>          $request->mar_whole3

            
        ]);

        $margin->save();
        session()->flash('flash_message', 'Margin Added');
        return redirect('margin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $margin  = Margins::find($id);
        
       
        return view('margin.edit', compact('margin','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $margin = MArgins::findOrFail($id);

        $margin->update([
            
            'name'             =>          $request->name,
            'mar_admin1'       =>          $request->mar_admin1,
            'mar_admin2'       =>          $request->mar_admin2,
            'mar_admin3'       =>          $request->mar_admin3,
            'mar_whole1'       =>          $request->mar_whole1,
            'mar_whole2'       =>          $request->mar_whole2,
            'mar_whole3'       =>          $request->mar_whole3

           
        ]);

        session()->flash('flash_message', 'Margin Updated');
        return redirect('margin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Margins::destroy($id);
        session()->flash('flash_message', 'Margin Deleted');
        return redirect('margin');
    }
}
