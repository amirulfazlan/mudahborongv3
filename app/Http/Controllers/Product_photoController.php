<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductPhotoRequest;
use App\Product_prices;
use App\ProductPhoto;
use App\Product;  
use DB;

class Product_photoController extends Controller
{
    public function index()
    {
    	$product = Product::paginate(20);
        $product_photo = ProductPhoto::paginate(10);
        $product_price = Product_prices::all()->toArray();
        //$brand = Brands::all()->toArray();

         return view('product_photo.index', compact('product_photo','product','product_price'));
    }

    public function uploadSubmit(Request $request){

    	$product = Product::all()->toArray();


         if($request->hasFile('filename')){


                 // $filename = uniqid().'.'.$request->file('filename')->getClientOriginalExtension();
                 // $request->file('filename')->move(base_path() . '/public/images/other_image', $filename);


            foreach ($request->filename as $file) {  

                $filename = uniqid().'.'.$file->getClientOriginalExtension();
                $file->move(base_path() . '/public/images/other_image', $filename);

    
                 $photo = new ProductPhoto([

                'p_id'   => $request->p_id,
                'filename'   => $filename

                ]);
                 $photo->save();

        }
        return redirect('product_photo');

    }
}

public function show($id)
    {
        $product = Product::find($id);
        $product_photo = ProductPhoto::paginate(10);

       return view('product_photo.show', compact('product','id'));   
    }

}
