<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moq_types;
use App\Moq_items;

class Moq_ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$moq_item = Moq_items::paginate(20);
        $moq_type = Moq_types::all()->toArray();
        $namedata= $request->get('search_name');  


         if (request()->has('search_name') ) {
            
             $moq_item = Moq_items::join('moq_types','moq_items.moqty_id','=','moq_types.id')->where('moq_types.moq_name', 'LIKE', '%'. $namedata . '%')->paginate(10);
           return view('moq_item.index', compact('moq_item', 'moq_type' ));
        }

            
        else{

            $moq_item = Moq_items::paginate(20);

             return view('moq_item.index', compact('moq_item', 'moq_type' ));
        }
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $moq_item = new Moq_items([
            'moqty_id'         =>          $request->moqty_id,
            'min_unit1'         =>          $request->min_unit1,
            'min_unit2'         =>          $request->min_unit2,
            'min_unit3'         =>          $request->min_unit3
            
        ]);

        $moq_item->save();
        session()->flash('flash_message', 'Moq Item Added');
        return redirect('moq_item');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $moq_type = Moq_types::all()->toArray();
        $moq_item  = Moq_items::find($id);
        
       
        return view('moq_item.edit', compact('moq_item','id'), compact('moq_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $moq_item = Moq_items::findOrFail($id);

         $moq_item->update([
            
            'moqty_id'         =>          $request->moqty_id,
            'min_unit1'         =>          $request->min_unit1,
            'min_unit2'         =>          $request->min_unit2,
            'min_unit3'         =>          $request->min_unit3
              
        ]);

        session()->flash('flash_message', 'Moq Item Updated');
        return redirect('moq_item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Moq_items::destroy($id);
        session()->flash('flash_message', 'Moq Item Deleted');
        return redirect('moq_item');
    }
}
