<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Pesanan;
use App\User;
use DB;
use App\Categories; 
use App\Product_prices;
use App\Product;
use Cart;
Use App\Newtracking;
use App\Address;
 

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         // Get the currently authenticated user...
        $user = Auth::user();  

       // Get the currently authenticated user's ID...
        $id = Auth::id();

        $user_id = Auth::id();


        //$user = User::all()->toArray();

        //$user= User::where('id', $id)->paginate(20); 
        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);
        $order = Pesanan::where('user_id', $id)->orderByDesc('created_at')->paginate(30);
        $cat = Categories::all()->toArray();
        $cart = Cart::content();
        $pro_var = Product_prices::all()->toArray();
        $pro = Product::all()->toArray();  

        return view('history.index', compact('order', 'user', 'cat', 'cart', 'pro_var', 'pro', 'order_truck')); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Pesanan::find($id);

        $user_id = Auth::id();

        //$address = Address::where('user_id' , $id)->firstOrFail(); 

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $cat = Categories::all()->toArray();
        $cart = Cart::content();
        $pro_var = Product_prices::all()->toArray();
        $pro = Product::all()->toArray();



         $obj = $order->cart_item;

         $json_decode = json_decode($obj);




        //dd($order);

        return view('history.show', compact('order','id') , compact('json_decode', 'cat', 'cart', 'pro_var', 'pro', 'order_truck', 'address'));
    }

    public function showtracking($id)
    {
        $order = Pesanan::find($id);

        $tracking = Newtracking::where('order_id', $id)->orderByDesc('created_at')->paginate(40);


        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $cat = Categories::all()->toArray();
        $cart = Cart::content();
        $pro_var = Product_prices::all()->toArray();
        $pro = Product::all()->toArray();

         $obj = $order->cart_item;

         $json_decode = json_decode($obj);


        return view('history.showtracking', compact('order','id') , compact('json_decode', 'cat', 'cart', 'pro_var', 'pro', 'order_truck', 'tracking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
