<?php

namespace App\Http\Controllers;

use App\Pesanan;


use DB;

use Illuminate\Http\Request;
use App\Product;
use App\Product_prices;
use Cart;
use App\CartTemp;

use Auth;

use App\CartItem;
use App\Size_items;
use App\Size_types;
Use App\Brands ;
use App\Categories;
use App\Materials;
use App\Moq_items;
use App\Prine;
use App\CartStore;

use App\Moq_types;
use App\Currencies;
use App\Margins;



class CartController extends Controller
{

    public function addItem(Request $request)
    {

               
       $id        = $request->id;
       $p_color   = $request->color_id;
               
       $p_price   = Product_prices::where('id', request('color_id'));
       $products  = Product::find($id);
       $size      = Size_items::find($id);
       
       $ayam      = $request->cart_size;
       $lembu     = $request->cart_quantity;
       $kambing   = $request->cart_color;


       $cur = Currencies::all()->toArray();
       $margin = Margins::all()->toArray();
       $moq_item =MOq_items::all()->toArray();


     
       foreach ($cur as $yuan) {
         foreach ($margin as $mar) {
           if ($mar['id'] == $products['margin_id']) {
             foreach ($moq_item as $moq) {
               if($moq['moqty_id'] == $products['moqty_id']){

                 if( $lembu <= $moq['min_unit1'] || $lembu >= $moq['min_unit1']  && $lembu < $moq['min_unit2'])
                 {

                 $admin_price = ( $products['base_price1']  /  $yuan['cur_value'] ) +  $mar['mar_admin1'];
                 $before_bundar =  $admin_price + $mar['mar_whole1'] ; 

                 $whole_price1 = ceil($before_bundar * 2 ) / 2; 

                 $min_moq = $moq['min_unit1'];

                            
               }

                elseif( $lembu >= $moq['min_unit2']  && $lembu < $moq['min_unit3']){

                 $admin_price = ( $products['base_price2'] /  $yuan['cur_value'] ) +  $mar['mar_admin2'];
                 $before_bundar =  $admin_price + $mar['mar_whole2'] ;  
                 $whole_price1 = ceil($before_bundar * 2 ) / 2; 
                 $min_moq = $moq['min_unit1'];
              
               }

              elseif( $lembu >= $moq['min_unit3'] ){

                 $admin_price = ( $products['base_price2'] /  $yuan['cur_value'] ) +  $mar['mar_admin3'];
                 $before_bundar =  $admin_price + $mar['mar_whole3'] ; 
                 $whole_price1 = ceil($before_bundar * 2 ) / 2;  
                 $min_moq = $moq['min_unit1'];
               }

               
             }
           }
           
         }
       }
     }

     if ($lembu >= $min_moq) 
     {
        $check_val = 1;
      
     }
     else{

      $check_val = 0;

     }

      

      // Cart::add($id, $p_price->product_name->p_name, 1, $p_price->price, "option"['image' => $p_price->main_image]);

    


       $cartItem = Cart::add([ 'id' =>$id, 'name' =>$products->p_name, 'qty' => $lembu, 'price' => round($whole_price1 ,1,PHP_ROUND_HALF_UP), 'options' => ['size' => $ayam , 'color' => $kambing, 'min' => $min_moq, 'code' => $check_val]]);

       
       // session()->flash('message', 'Product has been added to your Shopping Cart. Please be noted that you are only allowed to purchase a mininum of 5 items before allowed to submit order.');
        //return redirect('cart');
       return redirect()->back();
       
    }

    public function showCart()
    {
        $cart = Cart::content();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $prines = Prine::all()->toArray();
        $pro = Product::all()->toArray();

        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $cat = Categories::all()->toArray();

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        
        return view('cart.index', compact('cart'), compact('moq_item', 'pro_var', 'prines', 'moq_type', 'cur', 'margin', 'pro', 'subTotal', 'cat', 'order_truck'));
    }

    public function store(Request $request)

    {
      $id = Auth::id();  
       
      Cart::store($id);
      return redirect('cart');

    }

    public function simpanCart(Request $request)

    {

        $cart = Cart::content();


        $arr_tojson = array( 

            'user_id'             =>          Auth::user()->id,
            'product_name'        =>          $request->product_name,
            'quantity'            =>          $request->quantity,
            'color'               =>          $request->color,
            'size'                =>          $request->size,
            'price'               =>          $request->price,
            'total'               =>          $request->total       

        );

       

        $arr_tojson = json_encode($arr_tojson);

        CartStore::create([
          'product_name' => $arr_tojson

           ]);

        //  $cartstore->save();

    

    }


    public function updateItem(Request $request)
    {




      //Cart::update($rowId, 7);

      //increment the quantity
       if (request()->get('id') &&  (request()->get('increase')) == 1) {
      
        $rowId =  $request->get('id');

        $item = Cart::get($rowId);

        $add_qty = $item->qty + 1;

        $products = Product::where('p_name', $item->name)->get();
        $moq_item = Moq_items::all()->toArray(); 
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $cart_newprice= $item->price;

        if ( $add_qty >= $item->qty)
        {
          foreach ($cur as $yuan) 
          {
            foreach ($margin as $mar) {
              foreach ($products as $pro) {
                if( $mar['id'] == $pro['margin_id']) {
                  foreach ($moq_item as $moq) {
                    if($moq['moqty_id'] == $pro['moqty_id']){

                      if( $add_qty >= $moq['min_unit1']  && $add_qty < $moq['min_unit2']){
                        $admin_price = ( $pro['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole1'] ;   
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;
                      }

                      elseif( $add_qty >= $moq['min_unit2']  && $add_qty < $moq['min_unit3']){

                        $admin_price = ( $pro['base_price2'] +  $mar['mar_admin2']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole2'] ;
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                      }

                      elseif( $add_qty >= $moq['min_unit3'] ){

                        $admin_price = ( $pro['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole3'] ;
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                      }
                  }

                }
             }
              
            }                     

          }

        }
      }

        Cart::update( $rowId , $item->qty = $add_qty, $item->price = $cart_newprice );
    }

    //decrease the quantity
    if (request()->get('id') &&  (request()->get('decrease')) == 1) {
        $rowId =  $request->get('id');
        $item = Cart::get($rowId);

        $minus_qty = $item->qty - 1;

        $products = Product::where('p_name', $item->name)->get();
        $moq_item = Moq_items::all()->toArray(); 
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $cart_newprice= $item->price;

        if ( $minus_qty <= $item->qty)
        {
          foreach ($cur as $yuan) 
          {
            foreach ($margin as $mar) {
              foreach ($products as $pro) {
                if( $mar['id'] == $pro['margin_id']) {
                  foreach ($moq_item as $moq) {
                    if($moq['moqty_id'] == $pro['moqty_id']){

                      if( $minus_qty >= $moq['min_unit1']  && $minus_qty < $moq['min_unit2']){
                        $admin_price = ( $pro['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole1'] ;   
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;
                      }

                      elseif( $minus_qty >= $moq['min_unit2']  && $minus_qty < $moq['min_unit3']){

                        $admin_price = ( $pro['base_price2'] +  $mar['mar_admin2']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole2'] ;
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                      }

                      elseif( $minus_qty >= $moq['min_unit3'] ){

                        $admin_price = ( $pro['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                        $whole_price1 =  $admin_price + $mar['mar_whole3'] ;
                        $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                      }
                  }

                }
             }
              
            }                     

          }

        }
      }


        Cart::update($rowId, $item->qty = $minus_qty, $item->price = $cart_newprice );
    }

      
      return redirect('cart');

    }



    public function removeItem($rowId)
    {
        
       
        Cart::remove($rowId);
        return redirect('cart');  
    }

    public function destroyItem()
    {       
        Cart::destroy();
        return redirect('shopping_catalog');  
    }
    

}