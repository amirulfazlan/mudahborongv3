<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product_prices;

class UpdatePColorImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $price = Product_prices::find($id);
      
        return view('update_pc_main-image.create', compact('price','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $price = Product_prices::findOrFail($id);


        if($request->hasFile('main_image'))
        {
            $main_image = uniqid().'.'.$request->file('main_image')->getClientOriginalExtension();
            $request->file('main_image')->move(base_path() . '/public/images/product/', $main_image);

        }

         $price->update([
                  
            'main_image'          =>          $main_image,
           
        ]);
        
        $price->save();

        session()->flash('flash_message', 'Product Color Updated');
        //return redirect('product');

       
    /* ... SQL EXECUTION TO UPDATE DB ... */

    echo "<script>window.close();</script>";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
