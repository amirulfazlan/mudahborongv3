<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pesanan;

use DB;
use App\Product;
use App\Product_prices;
use App\Address;
use Cart;
use App\Categories;




class ProfileController extends Controller

{
    public function index(){

        // Get the currently authenticated user...
        $user = Auth::user();  

       // Get the currently authenticated user's ID...
        $id = Auth::id();

        $order_truck = Pesanan::where('user_id', $id)->orderByDesc('created_at')->paginate(30);

        $address = Address::where('user_id' , $id)->firstOrFail();
        
        $cart = Cart::content();
        $pro = Product::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $cat = Categories::all()->toArray();

                    
    	return view('profile.index', compact('address', 'cart', 'pro', 'pro_var', 'cat','order_truck'));
    }



    public function orders(){

    	$pro_var = Product_prices::all()->toArray();

    	$pro = Product::all()->toArray();
        $cat = Categories::all()->toArray();
        

    	$orders = DB::table('orders_product')->leftJoin('products','products.id', '=' , 'orders_product.product_id')->leftJoin('orders','orders.id', '=' , 'orders_product.orders_id')->get();

    	return view('profile.order', compact('orders'), compact('pro_var', 'pro', 'cat'));
    }

    public function product_profile()
    {
        return $this->belongsTo('App\Product', 'p_id');
    }

    public function address(){

        $alamat = Address::all()->toArray();
        $cat = Categories::all()->toArray();



        return view('profile.address' , compact('alamat', 'cat'));
    }

    public function changeAddress(){

        

        return view('profile.updateaddress');

    }

    public function tukarAddress(Request $request){

        echo 'here update query for address';
        dd($request->all);

    }


    public function Password(){

        return view('profile.changepassword');

    }

     public function updatePassword(){

        echo 'here update query for password';

    }   



}
