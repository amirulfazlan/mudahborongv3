<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


use App\Product;
Use App\Brands ;
use App\Categories;
use App\Size_types;
use App\Genders;
use App\Moq_types;
use App\Moq_items;
use App\Currencies;
use App\Margins;
use App\Size_items;

use DB;
use Input;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function myformAjax($id)
    {
        $item = DB::table("categories")
                    ->where("gender_id",$id)
                    ->pluck("category_product","id");
        return json_encode($item);
    }

    

    public function index(Request $request)
    {
        //$product = Product::paginate(20);
        //$product = DB::table('product')->paginate(10);
        //$product = Product::all()->toArray();

        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $category = Categories::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $gen = DB::table("genders")->pluck("g_name","id");  
        $moq_type = Moq_types::all()->toArray();
        $moq_item = Moq_items::all()->toArray();

        $genderdata= $request->get('gender_id');
        $catdata= $request->get('category_id');
        $branddata= $request->get('brand_id');
        $namedata= $request->get('search_name');
       
        if (request()->has('gender_id') || request()->has('category_id') || request()->has('brand_id'))

         {
            if( $genderdata == 0 && $catdata == 0 && $branddata == 0 )
            {
                $product = Product::orderByDesc('release_date')->paginate(20);
                 return view('product.index', compact('product','brand', 'gen', 'category'));
            }

            elseif( $genderdata == 0 && $catdata == 0 && $branddata != 0 )
            {
                $product = Product::where('brand_id', request('brand_id'))->orderByDesc('release_date')->paginate(20);
                 return view('product.index', compact('product','brand', 'gen', 'category'));
            }

             elseif( $genderdata != 0 && $catdata != 0 && $branddata == 0 )
            {
                $product = Product::where('category_id', request('category_id'))->orderByDesc('release_date')->paginate(20);
                 return view('product.index', compact('product','brand', 'gen', 'category'));
            }

            else
            {

             $product = Product::where('category_id', request('category_id'))->where('brand_id', request('brand_id'))->orderByDesc('release_date')->paginate(20);
             return view('product.index', compact('product','brand', 'gen', 'category'));
             } 
        }

        elseif (request()->has('search_name') ) {
            
            $product = Product::where('p_name', 'LIKE', '%'. $namedata . '%')->orderByDesc('release_date')->paginate(20);
                 return view('product.index', compact('product','brand', 'gen', 'category'));
        }

            
        else{

            $product = Product::orderByDesc('release_date')->paginate(20);

            return view('product.index', compact('product','brand', 'gen', 'category'));
        }



        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $category = Categories::all();
        $moq_type = Moq_types::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $gen = DB::table("genders")->pluck("g_name","id"); 
        $moq_item = Moq_items::all()->toArray();
        $margin = Margins::all()->toArray();

        return view('product.create', compact('brand' , 'category', 'size_type', 'gen', 'moq_type', '$moq_item', 'margin'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

        if($request->hasFile('main_image'))
        {
            $main_image = uniqid().'.'.$request->file('main_image')->getClientOriginalExtension();
            $request->file('main_image')->move(base_path() . '/public/images/main_product/', $main_image);
        }
    

        if($request->hasFile('back_image'))
        {
            $back_image = uniqid().'.'.$request->file('back_image')->getClientOriginalExtension();
            $request->file('back_image')->move(base_path() . '/public/images/main_product/', $back_image);
        }

        if($request->hasFile('size_chart'))
        {
            $size_chart = uniqid().'.'.$request->file('size_chart')->getClientOriginalExtension();
            $request->file('size_chart')->move(base_path() . '/public/images/size_chart/', $size_chart);
        }
        
        $products = new Product([

            'main_image'          =>          $main_image,
            'back_image'          =>          $back_image,
            'p_name'              =>          $request->p_name,
            'release_date'        =>          $request->release_date,
            'brand_id'            =>          $request->brand_id,
            'gender_id'           =>          $request->gender_id,
            'category_id'         =>          $request->category_id,
            'st_id'               =>          $request->st_id,
            'size_chart'          =>          $size_chart,
            'moqty_id'            =>          $request->moqty_id,
            'margin_id'           =>          $request->margin_id,
            'base_price1'         =>          $request->base_price1,
            'base_price2'         =>          $request->base_price2,
            'description'         =>          $request->description,
            'featured_product'    =>          $request->featured_product
        ]);

        $products->save();
        session()->flash('flash_message', 'Product Added');
        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $product = Product::find($id);       
        $brand = Brands::all()->sortBy('brand_name')->toArray();

        $category = Categories::all()->toArray();       
        $gen = DB::table("genders")->pluck("g_name","id"); 
        
        $moq_type = Moq_types::all()->toArray();    
        $moq_item = Moq_items::all()->toArray();
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $size_type = Size_types::all()->toArray();

        
        return view('product.show', compact('product','id'), compact( 'brand', 'category', 'gen', 'moq_type', 'moq_item', 'cur', 'margin', 'size_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::find($id);
        
        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $category = Categories::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $moq_type = Moq_types::all()->toArray();
        $gen = DB::table("genders")->pluck("g_name","id"); 
        $moq_item = Moq_items::all()->toArray();
        $margin = Margins::all()->toArray();
         
        
        return view('product.edit', compact('product','id'), compact('brand', 'category', 'size_type', 'gen', 'moq_type', '$moq_item', 'margin'));

        //return view('product.edit', compact('products','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);

       
         $products->update([
                   
            'p_name'              =>          $request->p_name,
            'release_date'        =>          $request->release_date,
            'brand_id'            =>          $request->brand_id,
            'gender_id'           =>          $request->gender_id,
            'category_id'         =>          $request->category_id,
            'st_id'               =>          $request->st_id,
            'moqty_id'            =>          $request->moqty_id,
            'margin_id'           =>          $request->margin_id,
            'base_price1'         =>          $request->base_price1,
            'base_price2'         =>          $request->base_price2,
            'description'         =>          $request->description,
            'featured_product'    =>          $request->featured_product
        ]);
    
        
        $products->save();

        session()->flash('flash_message', 'Product Updated');
        

        return back();
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        session()->flash('flash_message', 'Product Deleted');
        return redirect('product');
    }
}
