<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function getSession(Request $request){

    	if($request->session()->has('session_name'))

    		echo $request->session()->get('session_name');

    	else{

    		echo 'No data in Session';
    	}
    }

    public function storeSessionData(Request $request){

    	$request->session()->put('session_name' , 'Amirul Fazlan Hebat');

    	echo 'A data as been inserted into Session';
    }

    public function forgetSession(Request $request){

    	$request->session()->forget('session_name');

    	echo 'Data has been removed from session';
    }
}
