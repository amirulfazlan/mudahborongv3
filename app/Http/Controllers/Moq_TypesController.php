<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moq_types;

class Moq_TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $moq_type = Moq_types::paginate(20);
      
        return view('moq_type.index', compact('moq_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $moq_type = new Moq_types([
            'moq_name'         =>          $request->moq_name
            
        ]);

        $moq_type->save();
        session()->flash('flash_message', 'Moq Type Added');
        return redirect('moq_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $moq_type = Moq_types::find($id);
        
        return view('moq_type.edit', compact('moq_type','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $moq_type = Moq_types::findOrFail($id);

         $moq_type->update([    
            'moq_name'         =>      $request->moq_name    
        ]);

        session()->flash('flash_message', 'Moq Type Updated');
        return redirect('moq_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Moq_types::destroy($id);
        session()->flash('flash_message', 'Moq Type Deleted');
        return redirect('moq_type');
    }
}
