<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;


class UpdateBackImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
                 
        return view('update_backimage.create', compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);


         if($request->hasFile('back_image'))
        {
            $back_image = uniqid().'.'.$request->file('back_image')->getClientOriginalExtension();
            $request->file('back_image')->move(base_path() . '/public/images/main_product/', $back_image);

        }


         $products->update([
           
            'back_image'          =>          $back_image,
           
        ]);
  
        
        $products->save();

        session()->flash('flash_message', 'Product Updated');
        //return redirect('product');

       
    /* ... SQL EXECUTION TO UPDATE DB ... */

    echo "<script>window.close();</script>";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
