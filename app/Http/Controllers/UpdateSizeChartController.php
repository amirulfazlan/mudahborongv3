<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;


use App\Size_items;


class UpdateSizeChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $product = Product::find($id);
                 
        return view('update_size.create', compact('product','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);


        if($request->hasFile('size_chart'))
        {
            $size_chart = uniqid().'.'.$request->file('size_chart')->getClientOriginalExtension();
            $request->file('size_chart')->move(base_path() . '/public/images/size_chart/', $size_chart);
        }


         $products->update([
            
            
            'size_chart'          =>          $size_chart,
           
        ]);
  
        
        $products->save();

        session()->flash('flash_message', 'Product Updated');
        //return redirect('product');

       
    /* ... SQL EXECUTION TO UPDATE DB ... */

    echo "<script>window.close();</script>";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
