<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_prices;
use App\ProductPhoto;
use App\Product;  
use DB;
Use App\Brands ;
use App\Genders;
use App\Categories;


class Product_PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function myformAjax($id)
    {
        $item = DB::table("categories")
                    ->where("gender_id",$id)
                    ->pluck("category_product","id");
        return json_encode($item);
    }

    public function index(Request $request)
    {
        
        //$product = Product::paginate(20);
        $product_photo = ProductPhoto::orderByDesc('created_at')->paginate(20);
        $product_price = Product_prices::all()->toArray();
        //$brand = Brands::all()->toArray();

        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $category = Categories::all()->toArray();
        $gen = DB::table("genders")->pluck("g_name","id");


        $genderdata= $request->get('gender_id');
        $catdata= $request->get('category_id');
        $branddata= $request->get('brand_id');
        $namedata= $request->get('search_name');

         if (request()->has('gender_id') || request()->has('category_id') || request()->has('brand_id'))

         {
            if( $genderdata == 0 && $catdata == 0 && $branddata == 0 )
            {
                $product = Product::orderBy('release_date')->paginate(20);
                 return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
            }

            elseif( $genderdata == 0 && $catdata == 0 && $branddata != 0 )
            {
                $product = Product::where('brand_id', request('brand_id'))->orderBy('release_date')->paginate(20);
                 return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
            }

             elseif( $genderdata != 0 && $catdata != 0 && $branddata == 0 )
            {
                $product = Product::where('category_id', request('category_id'))->orderBy('release_date')->paginate(20);
                 return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
            }

            else
            {

             $product = Product::where('category_id', request('category_id'))->where('brand_id', request('brand_id'))->orderByDesc('release_date')->paginate(20);
             return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
             } 
        }

        elseif (request()->has('search_name') ) {
            
            $product = Product::where('p_name', 'LIKE', '%'. $namedata . '%')->orderByDesc('release_date')->paginate(20);
                return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
        }

            
        else{

            $product = Product::orderByDesc('release_date')->paginate(20);

           return view('product_photo.index', compact('product_photo','product','product_price', 'brand', 'gen', 'category'));
        }






         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::all()->toArray();

        $p_price = Product_prices::all()->toArray();


         if($request->hasFile('filename')){


                 // $filename = uniqid().'.'.$request->file('filename')->getClientOriginalExtension();
                 // $request->file('filename')->move(base_path() . '/public/images/other_image', $filename);


            foreach ($request->filename as $file) {  

                $filename = uniqid().'.'.$file->getClientOriginalName();
                $file->move(base_path() . '/public/images/other_image', $filename);

    
                 $photo = new ProductPhoto([

                'p_id'   => $request->p_id,
                'filename'   => $filename

                ]);
                 $photo->save();

        }
        // return redirect('product_photo');
        return back()->withInput();

    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $product_photo = ProductPhoto::all()->toArray();

       return view('product_photo.show', compact('product','id'), compact('product_photo'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductPhoto::destroy($id);
        session()->flash('flash_message', 'Product Photo Deleted');
        return back()->withInput();
    }

}
