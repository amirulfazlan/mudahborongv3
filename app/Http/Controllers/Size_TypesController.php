<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Size_types;


class Size_TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $size_type = Size_types::paginate(20);

        return view('size_type.index', compact('size_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $size_type = new Size_types([
            'size_name'         =>          $request->size_name
            
        ]);

        $size_type->save();
        session()->flash('flash_message', 'Size Type Added');
        return redirect('size_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size_type = Size_types::find($id);
        
        return view('size_type.edit', compact('size_type','id'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $size_type = Size_types::findOrFail($id);

         $size_type->update([    
            'size_name'         =>      $request->size_name    
        ]);

        session()->flash('flash_message', 'Size Type Updated');
        return redirect('size_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Size_types::destroy($id);
        session()->flash('flash_message', 'Size Type Deleted');
        return redirect('size_type');
    }
}
