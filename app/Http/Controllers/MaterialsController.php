<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Materials;

class MaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mat = Materials::paginate(10);
        $mat = Materials::all()->toArray();
      
        return view('material.index', compact('mat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [  
        'material_name'     => 'required',
        ]);

        $mat = new Materials([
            'material_name'         =>          $request->material_name
            
        ]);

        $mat->save();
        session()->flash('flash_message', 'Material Added');
        return redirect('material');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $mat = Materials::find($id);
        
        return view('material.edit', compact('mat','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mat = Materials::findOrFail($id);

         $mat->update([    
            'material_name'         =>      $request->material_name    
        ]);

        session()->flash('flash_message', 'Material Updated');
        return redirect('material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Materials::destroy($id);
        session()->flash('flash_message', 'Material Deleted');
        return redirect('material');
    }
}
