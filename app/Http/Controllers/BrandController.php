<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Brands;



class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brands::orderBy('brand_name')->paginate(10);
        //$brand = Brands::all()->toArray();

         return view('brand.index', compact('brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('brand_image'))
        {
            $brand_image = uniqid().'.'.$request->file('brand_image')->getClientOriginalExtension();
            $request->file('brand_image')->move(base_path() . '/public/images/brand/', $brand_image);
        }

        $brands = new Brands([

            'brand_image'         =>          $brand_image,
            'brand_name'          =>          $request->brand_name,
            
            

        ]);

       // dd($request->all());


        $brands->save();
        session()->flash('flash_message', 'New Brand Added');
        //return redirect('brand');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        
        $brand = Brands::find($id); 
        
        return view('brand.edit', compact('brand','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $brands = Brands::findOrFail($id);

          if($request->hasFile('brand_image'))
        {
            $brand_image = uniqid().'.'.$request->file('brand_image')->getClientOriginalExtension();
            $request->file('brand_image')->move(base_path() . '/public/images/brand/', $brand_image);
        }


         $brands->update([    
            'brand_name'      =>          $request->brand_name,
            'brand_image'     =>          $brand_image,
           
           
        ]);
         
        $brands->save();
        session()->flash('flash_message', 'Brand Updated');
        return back();
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Brands::destroy($id);
        session()->flash('flash_message', 'Brand Deleted');
        return redirect('brand');
    }
}
