<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class CustomerController extends Controller
{

  public function index()
  {
      $users = User::all();
      return view('customer.index', compact('users'));
  }


  public function edit($id)
  {
      $users = User::findOrFail($id);
      return view('customer.edit', compact('users'));
  }

  public function update($id, Request $request)
  {
      $users = User::findOrFail($id);
      $users->update($request->all());
      session()->flash('flash_message', 'Customer Updated');
      return redirect('/');
  }

  public function destroy($id)
  {
      User::destroy($id);
      session()->flash('flash_message', 'Customer Deleted');
      return redirect('customer.index');
  }

  public function customers()
  {
      $users = DB::table('users')
      ->where ('is_admin', 0)
      ->get();
      return view('customers.index', compact('users'));
  }



}
