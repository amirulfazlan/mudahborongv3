<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Size_types;
use App\Size_items;

class Size_ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         //$size_item = Size_items::paginate(10);
         $size_type = Size_types::all()->toArray();
         $namedata= $request->get('search_name');  


         if (request()->has('search_name') ) {
            
             $size_item = Size_items::join('size_types','size_items.st_id','=','size_types.id')->where('size_types.size_name', 'LIKE', '%'. $namedata . '%')->paginate(20);
           return view('size_item.index', compact('size_item', 'size_type' ));
        }

            
        else{

             $size_item = Size_items::paginate(20);

             return view('size_item.index', compact('size_item', 'size_type' ));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $size_item = new Size_items([
            'st_id'         =>          $request->st_id,
            'st_item'       =>          $request->st_item
            
        ]);

        $size_item->save();
        session()->flash('flash_message', 'Size Item Added');
        return redirect('size_item');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $size_type = Size_types::all()->toArray();
        $size_item  = Size_items::find($id);
        
       
        return view('size_item.edit', compact('size_item','id'), compact('size_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $size_item = Size_items::findOrFail($id);

        $size_item->update([
            
            'st_id'         =>      $request->st_id,
            'st_item'       =>      $request->st_item
           
        ]);

        session()->flash('flash_message', 'Size Item Updated');
        return redirect('size_item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Size_items::destroy($id);
        session()->flash('flash_message', 'Size Item Deleted');
        return redirect('size_item');
    }
}
