<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Address;
use App\Orders;
use Cart;
use DB;

class CheckoutController extends Controller
{
    public function index(){

    	if(Auth::check()) {

        $id = Auth::user()->id;

        $address = Address::where('user_id' , $id)->first();


        

    		return view ('checkout.shipping' , compact('address'));

    	}

    	return redirect ('/');	
    }

     public function checkoutform(){


        $id = Auth::user()->id;

        $address = Address::where('user_id' , $id)->firstOrFail();


        

        return view ('checkout.shipping' , compact('address'));

      }



    public function formvalidate(Request $request){

    	$this->validate($request, [
    	'firstname' 		=> 'required|max:255',
    	'lastname' 			=> 'required',
    	'company'			=> 'required',
    	'street' 			=> 'required',
    	'city' 				=> 'required',
    	'postcode' 			=> 'required|max:5',
    	'state' 			=> 'required',
    	'country' 			=> 'required',
    	'phone' 			=> 'required|max:14',
    	'email' 			=> 'required',
  		]);

  		$address = new Address;
  		$address->user_id	= Auth::user()->id;
  		$address->firstname = $request->firstname;
  		$address->lastname  = $request->lastname;
  		$address->company   = $request->company;
  		$address->street    = $request->street;
  		$address->city      = $request->city;
  		$address->postcode  = $request->postcode;
  		$address->state     = $request->state;
  		$address->country   = $request->country;
  		$address->phone     = $request->phone;
  		$address->email     = $request->email;

      $address->payment_method = $request->payment_method;
      $address->delivery_method = $request->delivery;
      


  		$address->save();

      Orders::createOrder();

      //Cart::destroy();
      return redirect('thankyou');

  		//dd($request->all());

    }

   
}
