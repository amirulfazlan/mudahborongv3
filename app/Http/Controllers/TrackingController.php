<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
Use App\Pesanan ;
Use App\Tracking ;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

          $pesanan = Pesanan::orderBy('created_at','asc')->paginate(20);    
   
        
         return view('tracking.index', compact('pesanan'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $pesanan = Pesanan::find($id);
        $tracking = Tracking::all();
        
        return view('tracking.create', compact('pesanan','id'), compact('tracking'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $tracking = new Tracking([

            'order_id'              =>          $request->order_id,
            'tracking_num'          =>          $request->tracking_num,
            'berat'                 =>          $request->berat,
            'harga'                 =>          $request->harga,
                 

        ]);

         $pesanan = Pesanan::findOrFail($request->order_id);

         $staus_change= "Pending Shipment Payment";

         $pesanan->update([    
            'status'              =>        $staus_change,
                  
           
        ]);

       // dd($request->all());

        $tracking->save();
        $pesanan->save();

        session()->flash('flash_message', 'New Tracking Added');
        return redirect('admin-orderhistory');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pesanan = Pesanan::find($id);
        $tracking = Tracking::all();

        
        return view('tracking.create', compact('pesanan','id'), compact('tracking'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $pesanan = Pesanan::find($id);

       $tracking = Tracking::where('order_id', $id)->get();
        
        return view('tracking.edit', compact('pesanan','id'), compact('tracking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pesanan= Pesanan::findOrFail($id);

        $tracking = Tracking::where('order_id', $id)->first();

         $tracking->update([    
            'order_id'              =>          $request->order_id,
            'tracking_num'          =>          $request->tracking_num,
            'berat'                 =>          $request->berat,
            'harga'                 =>          $request->harga,
  
        ]);  

         
        $tracking->save();
        session()->flash('flash_message', 'Tracking Number Updated');
        return back();
        //return redirect('admin-orderhistory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
