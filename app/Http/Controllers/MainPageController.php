<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Pesanan;
use Cart;
use App\PoductPhoto;

use App\Product;
use App\Product_prices;
use App\Size_types;
use App\Size_items;
use App\Moq_items;
use App\Categories;
use App\Genders;
use App\Moq_types;
use App\Currencies;
use App\Margins;
use App\Brands;

use DB;
use Input;


class MainPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cart = Cart::content();
        $cart->count();

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $pro = Product::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
       
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $cat = Categories::all()->toArray();
        $category = Categories::all()->toArray();   

        $shoes = DB::table('products')->where('category_id', 5)->orderBy('created_at', 'desc')->get();

        $men_cloth = DB::table('products')->whereIn('category_id', array('1','2','3','12'))->orderBy('created_at', 'desc')->get();

        $women_cloth = DB::table('products')->whereIn('category_id', array('4','6','7','13','14'))->orderBy('created_at', 'desc')->get();

         return view('main_page.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck', 'shoes', 'men_cloth', 'women_cloth'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
