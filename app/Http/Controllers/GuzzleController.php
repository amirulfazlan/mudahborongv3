<?php

namespace App\Http\Controllers;

use Guzzle

use Illuminate\Http\Request;

class GuzzleController extends Controller
{
    public function handleWebhook(){



    	$bill_id = Input::get('id');
		$email = Input::get('email');
		$mobile = Input::get('mobile');
		$name = Input::get('name');
		$collection_id = Input::get('collection_id');
		$paid = Input::get('paid');
		$state = Input::get('state');
		$paid_amount = Input::get('paid_amount');
		$url = Input::get('url');
		$paid_at = Input::get('paid_at');

		$pymnt_billplz = new PaymentsBillplz();
		$pymnt_billplz->bill_id = $bill_id;
		$pymnt_billplz->email = $email;
		$pymnt_billplz->mobile = $mobile;
		$pymnt_billplz->name = $name;
		$pymnt_billplz->collection_id = $collection_id;
		$pymnt_billplz->paid_status = $paid;
		$pymnt_billplz->state = $state;
		$pymnt_billplz->paid_amount = number_format(($paid_amount / 100), 2, '.', ' ');
		$pymnt_billplz->url = $url;
		$pymnt_billplz->paid_at = $paid_at;
		$pymnt_billplz->save();
    }
}
