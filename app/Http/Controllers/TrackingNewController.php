<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;

Use App\Pesanan ;
Use App\Newtracking;
use App\Product_prices;

class TrackingNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $pesanan = Pesanan::orderBy('created_at','asc')->paginate(20);    
   
        
         return view('newtracking.index', compact('pesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         

        $newtracking = new Newtracking([
            'order_id'              =>          $request->order_id,
            'item_id'               =>          $request->item_id,
            'tracking_no'           =>          $request->tracking_no,    
            'weight'                =>          $request->weight,
            'ship_price'            =>          $request->ship_price
            
        
        ]);

       

        $newtracking->save();
        session()->flash('flash_message', 'New Product Tracking');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $pesanan = Pesanan::find($id);

         $obj = $pesanan->cart_item;

         $json_decode = json_decode($obj);

         $tracking = Newtracking::where('order_id', $id)->orderByDesc('created_at')->paginate(40);

         $pro_var = Product_prices::all()->toArray();

        $count_item = 0;

        foreach($json_decode as $item) {
         $count_item ++;
        }

        $data_tracking= $tracking->count();

        $remaining = $count_item - $data_tracking;

       
        return view('newtracking.show', compact('pesanan','id'),compact('json_decode', 'tracking', 'pro_var', 'count_item', 'data_tracking', 'remaining'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        

        $pesanan = Pesanan::find($id);

        $tracking_id = $_SERVER['QUERY_STRING'];

        $tracking = Newtracking::where('id', $tracking_id )->first();



         $obj = $pesanan->cart_item;

         $json_decode = json_decode($obj);

         
      
        return view('newtracking.edit', compact('pesanan','id'), compact('tracking', 'json_decode', 'tracking_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tracking= Newtracking::findOrFail($id);

         $tracking->update([    
            'item_id'               =>          $request->item_id,
            'tracking_no'           =>          $request->tracking_no,
            'weight'                =>          $request->weight,
            'ship_price'            =>          $request->ship_price,
  
        ]);  

         
        $tracking->save();
        session()->flash('flash_message', 'Tracking Details Updated');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Newtracking::destroy($id);
        session()->flash('flash_message', 'Product Tracking Details Deleted');
         return back();
    }
}
