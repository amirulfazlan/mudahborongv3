<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


use App\Product_prices;
use App\Product;
use App\Size_items;
use App\Size_types;
use App\Moq_types;
use App\Genders;
Use App\Brands ;




class Product_PricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function myformAjax($id)
    {
        $item = DB::table("size_items")
                    ->where("st_id",$id)
                    ->pluck("st_item","id");
        return json_encode($item);
    }


    public function index(Request $request)
    {
        //$price = Product_prices::paginate(30);
        $product = DB::table('products')->orderBy('release_date', 'DESC')->get(); 
        $item = Size_items::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $moq_type = Moq_types::all()->toArray();
        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $gen = DB::table("genders")->pluck("g_name","id");
        $namedata= $request->get('search_name');  

        //$type = DB::table("size_types")->pluck("size_name","id");

       if (request()->has('search_name') ) {
            
            $price = Product_prices::join('products','products.id','=','product_prices.p_id')->select('products.p_name', 'product_prices.p_id', 'product_prices.main_image', 'product_prices.sku')->where('products.p_name', 'LIKE', '%'. $namedata . '%')->orderByDesc('created_at')->paginate(20);

            //dd($price);

            return view('product_price.index', compact('price', 'product', 'item', 'size_type' ,  'moq_type', 'brand', 'gen'));
        }

            
        else{

            $price = Product_prices::orderByDesc('created_at')->paginate(30);

              return view('product_price.index', compact('price', 'product', 'item', 'size_type' ,  'moq_type', 'brand', 'gen'));
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        if($request->hasFile('main_image'))
        {
            $main_image = uniqid().'.'.$request->file('main_image')->getClientOriginalExtension();
            $request->file('main_image')->move(base_path() . '/public/images/product/', $main_image);
        }

        $auto_no = rand();

        $auto_sku = "sku" . $auto_no;

        $price = new Product_prices([
            'p_id'           =>          $request->p_id,
            'sku'            =>          $auto_sku,
            'main_image'     =>          $main_image,
            'p_color'        =>          $request->p_color
            
        
        ]);

        //dd($price);

        $price->save();
        session()->flash('flash_message', 'New Product Color Added');
        return redirect('product_price');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $price = Product_prices::find($id);
        $product = Product::all()->toArray();
        $type = DB::table("size_types")->pluck("size_name","id");
        $size_type = Size_types::find($id);
        $moq_type = Moq_types::all()->toArray();

        
        return view('product_price.show', compact('price','id'), compact('size_type','id'), compact('product', 'type','moq_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $price = Product_prices::find($id);
       $product = Product::all()->toArray();
       $type = DB::table("size_types")->pluck("size_name","id");
       //$item = Size_items::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $moq_type = Moq_types::all()->toArray();

        
        return view('product_price.edit', compact('price','id'), compact('product', 'type', 'size_type', 'moq_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id)
    {
        $price = Product_prices::findOrFail($id);


          $price->update([
            
           
            'sku'                 =>          $request->sku,
            'p_color'             =>          $request->p_color
            
        ]);

    
        $price->save();

        session()->flash('flash_message', 'Product Updated');
        return redirect('product_price');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product_prices::destroy($id);
        session()->flash('flash_message', 'Product Price Deleted');
        return redirect('product_price');
        
    }
}
