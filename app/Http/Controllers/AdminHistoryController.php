<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesanan;
use App\User;
use App\Address;

use PDF;

use App\Product_prices;
use App\Product;
use App\Currencies;
use App\Margins;
use App\Moq_items;
Use App\Newtracking;



class AdminHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all()->toArray();
        
        $tracking = Newtracking::all()->toArray();


        if (request()->has('search_date') ) {
            
            $order = Pesanan::where('created_at', 'like', '%'.request('search_date').'%')->paginate(30);
             
        }   

        else{

              $order = Pesanan::orderByDesc('created_at')->paginate(30);

        }


    

        return view('admin_history.index', compact('order', 'user', 'tracking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $order = Pesanan::find($id);

         $pro_var = Product_prices::all()->toArray();
         $products = Product::all()->toArray();

         $address = Address::where('user_id' , $order->user_id)->firstOrFail();
         $nama = $address->firstname.' '.$address->lastname;

         
         $cur = Currencies::all()->toArray();
         $margin = Margins::all()->toArray();
         $moq_item =MOq_items::all()->toArray();

         $obj = $order->cart_item;

         $json_decode = json_decode($obj);

         $output = array();

         foreach($json_decode as $object) {

             if (isset($output[$object->id])) {
             $output[$object->id]->qty += $object->qty;
             }

             else {
              $output[$object->id] = clone $object;
              }

         }
      
         $output = array_values($output);

         $admin_price = 0;  
         $total_price = 0;
         $grand_total = 0;
         $round = 0;

        $all_new_output = array();

         foreach($output as $combine){

         foreach ($cur as $yuan) {
            foreach ($products as $product){
               
                   
                    if($combine->id == $product['id'])   {

                        foreach ($margin as $mar) {
                           if( $mar['id'] == $product['margin_id']) {
                             foreach ($moq_item as $moq){

                                if($moq['moqty_id'] == $product['moqty_id']){

                                if($combine->qty >= $moq['min_unit1']  && $combine->qty < $moq['min_unit2']){
                                $admin_price = ( $product['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];}

                                elseif ($combine->qty >= $moq['min_unit2']  && $combine->qty < $moq['min_unit3']) {
                                $admin_price = ( $product['base_price2'] +  $mar['mar_admin2']) /  $yuan['cur_value'];}

                                elseif( $combine->qty >= $moq['min_unit3'] ){
                                $admin_price = ( $product['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];}

                                 $total_price =  number_format($admin_price, 2) * $combine->qty ;
                                 $round= round($total_price ,1,PHP_ROUND_HALF_UP) ;
                                 $grand_total += $round;

                                 }

                            

                             }

                            }

                         }


                    }
                }
            }

             $all_new_output [] = array(

                                        'item_id'       => $combine->id,
                                        'name_item'     => $combine->name,
                                        'total_qt'      => $combine->qty,                             
                                        'admin'         => $admin_price
                                        

                                     );
        
         }

      



         return view('admin_history.show', compact('order','id') , compact('json_decode', 'pro_var','output', 'products', 'cur', 'margin', 'moq_item', 'all_new_output', 'address', 'nama'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadPDF($id){

      $order = Pesanan::find($id);


      $pro_var = Product_prices::all()->toArray();

      $user = $order->user_id;

        $obj = $order->cart_item;

        $address = Address::where('user_id' , $user)->firstOrFail();
        $nama = $address->firstname.' '.$address->lastname;
        $products = Product::all()->toArray();

    $json_decode = json_decode($obj);

    $cur = Currencies::all()->toArray();
         $margin = Margins::all()->toArray();
         $moq_item =MOq_items::all()->toArray();


         $date = $order->created_at;
         $filename = $date.'-'.$nama.'.'.'pdf';

         

         

         $output = array();

         foreach($json_decode as $object) {

             if (isset($output[$object->id])) {
             $output[$object->id]->qty += $object->qty;
             }

             else {
              $output[$object->id] = clone $object;
              }

         }
      
         $output = array_values($output);

            $admin_price = 0;  
         $total_price = 0;
         $grand_total = 0;
         $round = 0;

        $all_new_output = array();

         foreach($output as $combine){

         foreach ($cur as $yuan) {
            foreach ($products as $product){
               
                   
                    if($combine->id == $product['id'])   {

                        foreach ($margin as $mar) {
                           if( $mar['id'] == $product['margin_id']) {
                             foreach ($moq_item as $moq){

                                if($moq['moqty_id'] == $product['moqty_id']){

                                if($combine->qty >= $moq['min_unit1']  && $combine->qty < $moq['min_unit2']){
                                $admin_price = ( $product['base_price1'] /  $yuan['cur_value'] ) +  $mar['mar_admin1'] ;}

                                elseif ($combine->qty >= $moq['min_unit2']  && $combine->qty < $moq['min_unit3']) {
                                $admin_price = ( $product['base_price2'] /  $yuan['cur_value']) +  $mar['mar_admin2'] ;}

                                elseif( $combine->qty >= $moq['min_unit3'] ){
                                $admin_price = ( $product['base_price2'] /  $yuan['cur_value']) +  $mar['mar_admin3'] ;}

                                 $total_price =  number_format($admin_price, 2) * $combine->qty ;
                                 $round= round($total_price ,1,PHP_ROUND_HALF_UP) ;
                                 $grand_total += $round;

                                 }

                            

                             }

                            }

                         }


                    }
                }
            }

             $all_new_output [] = array(

                                        'item_id'       => $combine->id,
                                        'name_item'     => $combine->name,
                                        'total_qt'      => $combine->qty,                             
                                        'admin'         => $admin_price
                                        

                                     );

         }
        

    //dd($address);

     $pdf = PDF::loadView('pdf-old', compact('order','id','user_id','address','pro_var','all_new_output'), compact('json_decode'));
     return $pdf->download($filename);

    }
}
