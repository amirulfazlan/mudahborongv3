<?php

namespace App\Http\Controllers;



use Cart;
use App\Prine;
use App\Product;
use App\Product_prices;
use App\Currencies;
use App\Margins;
use App\Moq_items;
use App\Moq_types;
use App\Pesanan;
use Auth;
use App\Categories;
use App\PaymentPrime;



use Illuminate\Http\Request;

class BasketController extends Controller
{
    public function index(Request $request){

        $prines = Prine::all()->toArray();
        $pro = Product::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $cat = Categories::all()->toArray();
  
        $nama = $request['product_name'];
        $quantity = $request['quantity'];
        $color    = $request['color'];
        $size     = $request['size'];  
        $id       = $request['pid'];

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $pro_var = Product_prices::all()->toArray();

      

        //$totalqty = array_sum($quantity);


        $cart = Cart::content()->groupBy('id');
        //$cart1    = $cart['1'];

        $all_new_array = array();

        foreach ($cart as $carts) {
            
            $same_id = array();
            $cart_total = 0;

            foreach($carts as $ct)
            {

                 
                   
                    


                $cart_total += $ct->qty;


                            $same_id []= array(
                                        'id' => $ct->id,
                                        'qty' => $ct->qty,
                                        'name' => $ct->name,
                                        'size' => $ct->options->size,
                                        'color'=> $ct->options->color,
                                        'price'=>  $ct->price,
                                        'moq_min'=>  $ct->options->min,
                                        'min_check'=> $ct->options->code
                                        
                                   );
                        
                        

                        
                        }



                        $p_id = $ct->id;

                        $products  = Product::find($p_id);

                        $cart_newprice= $ct->price;




                        if ( $cart_total != $ct->qty ) {

                               foreach ($cur as $yuan) {
                                foreach ($margin as $mar) {

                                    if( $mar['id'] == $products['margin_id']) {

                                        foreach ($moq_item as $moq) {
                                           if($moq['moqty_id'] == $products['moqty_id']){


                                            if( $cart_total >= $moq['min_unit1']  && $cart_total < $moq['min_unit2']){

                                            $admin_price = ( $products['base_price1'] /  $yuan['cur_value']) +  $mar['mar_admin1'];

                                            $whole_price1 =  $admin_price + $mar['mar_whole1'] ;   
                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2;

                                           
                                            }

                                            elseif( $cart_total >= $moq['min_unit2']  && $cart_total < $moq['min_unit3']){

                                            $admin_price = ( $products['base_price2']  /  $yuan['cur_value']) +  $mar['mar_admin2'] ;

                                            $whole_price1 =  $admin_price + $mar['mar_whole2'] ;

                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2; 

                                            }

                                            elseif( $cart_total >= $moq['min_unit3'] ){

                                            $admin_price = ( $products['base_price2']  /  $yuan['cur_value'] ) +  $mar['mar_admin3'] ;

                                            $whole_price1 =  $admin_price + $mar['mar_whole3'] ;
                                            
                                            $cart_newprice = ceil($whole_price1 * 2 ) / 2;   
                                            }

                                           

                                           }


                                        }

  
                                    }

                                }

                                 

                               }

                        }

                        $cart_newcode = $ct->options->code;

                        if ( $cart_total >= $ct->options->min ) {
                         
                         $cart_newcode =1;

                        }
                        else{

                            $cart_newcode =0;

                        }

            

            $all_new_array [] = array(


                                        'item_id'       => $ct->id,
                                        'name_item'     => $ct->name,
                                        'total_qt'      => $cart_total,                             
                                        'unit_price'    => $cart_newprice,
                                        'unit_min'      => $ct->options->min,
                                        'unit_check'    => $cart_newcode,
                                        'collection'    => $same_id

                                     );         
        }



    
     return view ('basket.index', compact('cart','all_new_array','arrid', 'same_id','unit_price','totalprice','ayam', 'prines', 'moq_item', 'pro_var', 'prines', 'moq_type', 'cur', 'margin', 'pro', 'cat', 'products', 'order_truck'));

    }

    public function store(Request $request){
     
  

        // // $checkprime = $request['check'];
        // // $checkid = $request['item_id'];

    

        // // $all_new_array = array();           


        // //       $all_new_array [] = array(


        // //                 'item_id'  => $checkid,
        // //                 'prime'    => $checkprime

        // //         );   


        //    dd(Cart::content());



        $auto_no = rand();

        $auto_ord = "MB" . $auto_no;

        $simpanOrder = new Pesanan([

            'id'                  =>          $auto_ord,        
            'user_id'             =>          Auth::id(),
            'grand_total'         =>          $request->final_price,
            'status'              =>          'Pending',
            'cart_item'           =>          Cart::content()
            
        ]);

        $simpanOrder->save();

         $combinedua = array( 

             'pro_id'             =>          $request->array_namepro,
             'prime_price'        =>          $request->array_prime,
                  

        );

         /*echo '<pre>';
         print_r($combinedua);
         echo '</pre>';*/

         $boboyhebat = array();

         foreach( $request->array_namepro as $index => $code ) {
           //echo $code . '-' . $request->array_prime[$index];

           $boboyhebat[] = array(
                                    'product_id' => $code,
                                    'prime_price'=> $request->array_prime[$index]
                               );

        }

        /*echo '<pre>';
         print_r($boboykacak);
         echo '</pre>';*/

         

        $testsatu = $request->array_namepro;
        $testdua = $request->array_prime;

       

         $all_new_array [] = array(


                                         'item_id'       => $testsatu,
                                         'name_item'     => $testdua

                                      ); 


        $json_prime = json_encode($boboyhebat);    

        //dd($json_prime);    


        // $prime = new PaymentPrime([

        //     'ord_id'              =>          $auto_ord,        
        //     'prime_item'          =>          $json_prime
             
        // ]);


         //dd($all_new_array);

        $insertedId = $simpanOrder->id;

       // echo $insertedId ;

       // exit();

        return redirect()->action(
            'PaymentController@CreatePayment', ['id' => $insertedId]
        );


    }

    

}
