<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Pesanan;
use Cart;
use App\PoductPhoto;

use App\Product;
use App\Product_prices;
use App\Size_types;
use App\Size_items;
use App\Moq_items;
use App\Categories;
use App\Genders;
use App\Moq_types;
use App\Currencies;
use App\Margins;
use App\ProductPhoto;
use App\Brands;

use DB;
use Input;

class ShoppingCatalog_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function myformAjax($id)
    {
        $item = DB::table("categories")
                    ->where("gender_id",$id)
                    ->pluck("category_product","id");
        return json_encode($item);
    }


    public function searchbox(Request $request)
    {

        $cart = Cart::content();
        $cart->count();  

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $pro = Product::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $cat = Categories::all()->toArray();
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $brand = Brands::all()->sortBy('brand_name')->toArray();





        $category = Categories::all()->toArray();       
        $gen = DB::table("genders")->pluck("g_name","id");
 
        
        //$branddata= $request->get('brand_id');
        //$sortdata= $request->get('filter');

       if ( request()->has('brand_id') &&  request()->has('price'))
        {
     
            if( $request->get('brand_id') != 0 && $request->get('price') == 1 )
            {
                $product = Product::where('brand_id', request('brand_id'))->orderBy('base_price2')->paginate(32);
                 return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

             elseif( $request->get('brand_id') != 0 && $request->get('price') == 2 )
            {
                $product = Product::where('brand_id', request('brand_id'))->orderByDesc('base_price2')->paginate(32);
                 return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

            elseif ( $request->get('brand_id') != 0 && $request->get('price') == 0 )
            {

            $product = Product::where('brand_id', request('brand_id'))->orderBy('release_date')->paginate(32);

            return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

            elseif ( $request->get('brand_id') == 0 && $request->get('price') == 1 )
            {

            $product = Product::orderBy('base_price2')->paginate(32);

            return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

            elseif ( $request->get('brand_id') == 0 && $request->get('price') == 2 )
            {

            $product = Product::orderByDesc('base_price2')->paginate(32);

            return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

            elseif ( $request->get('brand_id') == 0 && $request->get('price') == 0 )
            {

            $product = Product::orderBy('release_date')->paginate(32);

            return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'order_truck'));
            }

        }
   
    }

    public function index(Request $request)
    {

        $cart = Cart::content();
        $cart->count();

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $pro = Product::all()->toArray();
        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
       
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
        $brand = Brands::all()->sortBy('brand_name')->toArray();
        $cat = Categories::all()->toArray();

        //$product = Product::paginate(30);

        $category = Categories::all()->toArray();       
        $gen = DB::table("genders")->pluck("g_name","id"); 

        
        $genderdata= $request->get('gender_id');
        $catdata= $request->get('category_id');
        $sortdata= $request->get('filter');

        
        if (request()->has('category_id') ) {

        $product = Product::where('category_id', request('category_id'))->orderByDesc('release_date')->paginate(32);

        return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck'));
        }


        elseif (request()->has('gender_id') ) {

        $product = Product::where('gender_id', request('gender_id'))->orderByDesc('release_date')->paginate(32);

        return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck'));
        }

        
        elseif (request()->has('cat_id') ) {

        $product = Product::whereIn('category_id', $request->get('cat_id'))->orderByDesc('release_date')->paginate(32);

        return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck'));

        }

        elseif (request()->has('t_brand_id') ) {

        $product = Product::where('brand_id', $request->get('t_brand_id'))->orderByDesc('release_date')->paginate(32);
        
        return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck'));
        }

     
        
        else{

            $product = Product::orderByDesc('release_date')->paginate(32);

            return view('shopping.index', compact('product','size_type', 'size_item', 'moq_item', 'pro_var', 'cat', 'moq_type', 'cur', 'margin', 'cart', 'pro', 'brand', 'gen', 'category', 'cat', 'order_truck'));
        }

      

            }

     

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $cart = Cart::content();
        $pro = Product::all()->toArray();

        $user_id = Auth::id();

        $order_truck = Pesanan::where('user_id', $user_id)->orderByDesc('created_at')->paginate(30);

        $product = Product::find($id);
        $size_type = Size_types::all()->toArray();
        $size_item = Size_items::all()->sortBy('id')->toArray();
        $moq_item = Moq_items::all()->toArray();
        $pro_var = Product_prices::all()->toArray();
        $moq_type = Moq_types::all()->toArray();    
        $cur = Currencies::all()->toArray();
        $margin = Margins::all()->toArray();
       
        $photo = ProductPhoto::all()->toArray();
        $brand = Brands::all()->toArray();
        $cat = Categories::all()->toArray();


        return view('shopping.show', compact('product','id'), compact('size_type', 'size_item', 'moq_item', 'pro_var', 'moq_type', 'cur', 'margin', 'photo', 'cart', 'pro', 'brand', 'cat', 'order_truck') );   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
