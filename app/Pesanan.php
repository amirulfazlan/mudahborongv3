<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
     protected $fillable = [
        'id',
    	'user_id',
    	'grand_total',
    	'status',
    	'cart_item'

    ];

    
     public function wh_name()
    {
        return $this->belongsTo('App\User', 'user_id');

    }

    public function track_num()
    {
        return $this->hasOne('App\Tracking', 'order_id');
    }


}


