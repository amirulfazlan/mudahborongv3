<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhoto extends Model
{
    protected $fillable = ['p_id','filename'];

    public function product_name()

    {
    	return $this->belongsTo('App\Product', 'p_id');
    }
}
