<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartStore extends Model
{
    protected $fillable = [
        'user_id', 
        'product_name',
        'quantity',
        'color',
        'size',
        'price',
        'total',

    ];

    
}
