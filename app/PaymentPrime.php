<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentPrime extends Model
{
    protected $fillable = [

    	'ord_id',
    	'prime_item',

    ];
}
