<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'gender_id', 
        'category_product', 
    ];


    public function cat_gender()
    {
        return $this->belongsTo('App\Genders', 'gender_id');
    }
}
