<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id', 
        'firstname',
        'lastname',
        'company',
        'street',
        'city',
        'postcode',
        'state',
        'contry',
        'phone',
        'email',
        'payment_method',
        'delivery_method',

    ];
    
    public function user_add()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
