<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
   protected $fillable = [
        'order_id', 
        'tracking_num',
        'berat', 
        'harga',
    ];

    public function pesanan_track()
    {
        return $this->belongsTo('App\Pesanan');
    }
}
