<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentsBillplz extends Model
{
        protected $fillable = [
        'bill_id', 
        'email',
        'mobile',
        'name',
        'collection_id',
        'paid_status',
        'state',
        'paid_amount',
        'url',
        'paid_at',


    ];
}
