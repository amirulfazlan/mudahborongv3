<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newtracking extends Model
{
     protected $fillable = [
        'order_id',
        'item_id',
        'tracking_no',
        'weight', 
        'ship_price',
    ];

    public function pesanan_track()
    {
        return $this->belongsTo('App\Pesanan');
    }
}
