<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $fillable = [
        'brand_name', 
        'brand_image',

    ];


    public function product_brand()
    {
    	return $this->belongsTo('Product');
    }
}
