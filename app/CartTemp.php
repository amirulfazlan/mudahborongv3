<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartTemp extends Model
{
    protected $fillable = [
        'product_name',
        'quantity',
        'color',
        'size',
        'price',


    ];
}
