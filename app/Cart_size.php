<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart_size extends Model
{
    
    protected $fillable = [
        'cart_size', 
        'cart_quantity',
    ];
}
