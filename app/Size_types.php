<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size_types extends Model
{
    protected $fillable = [
        'size_name', 
    ];


    public function sizes()
    {
        return $this->hasMany('App\Size_items', 'st_id');
    }

    public function list_product()
    {
        return $this->hasMany('App\Product_prices', 'st_id');
    }

    public function size_list()
    {
        return $this->hasManyThrough('App\Product_prices', 'App\Size_items', 'st_id', 'st_id');
    }
    
}
