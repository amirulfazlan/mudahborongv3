<?php

namespace App;

use Cart;
use App\Address;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Orders extends Model
{
    protected $fillable = [

    	'total',
    	'status'

    ];

    public function orderFields() {

    	return $this->belongsToMany(Product::class)->withPivot('qty', 'total');
    }

    public static function createOrder() {

    	$user = Auth::user();
    	$order = $user->orders()->create(['total'=>Cart::total(),'status' => 'Pending']);
    	$cartItems = Cart::content();

    	foreach ($cartItems as $cartItem) {

    		$order->orderFields()->attach($cartItem->id, ['qty' => $cartItem->qty, 'total' => $cartItem->total ]);
    		
    	}

    	

    }
}
