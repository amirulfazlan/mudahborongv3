<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moq_items extends Model
{
    protected $fillable = [
        'moqty_id', 
        'min_unit1',
        'min_unit2',
        'min_unit3',
       
    ];

    public function moqtype()
    {
    	return $this->belongsTo('App\Moq_types', 'moqty_id');
    }
    
}
