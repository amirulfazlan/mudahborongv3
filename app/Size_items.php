<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size_items extends Model
{
        protected $fillable = [
        'st_id', 
        'st_item', 
    ];

    public function stype()
    {
    	return $this->belongsTo('App\Size_types', 'st_id');
    }
    
    public function list()
    {
        return $this->hasManyThrough('App\Size_types', 'App\Product_prices', 'st_id', 'st_id');
    }

}
