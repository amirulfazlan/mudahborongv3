<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_prices extends Model
{
     protected $fillable = [
        
        'p_id',
        'sku',
        'main_image', 
        'p_color', 
        

    ];

     public function product_name()
    {
        return $this->belongsTo('App\Product', 'p_id');
    }


    public function moqtype()
    {
        return $this->belongsTo('App\Moq_types', 'moqty_id');
    }


    public function list()
    {
        return $this->hasManyThrough('App\Size_types', 'App\Size_items', 'st_id', 'st_id');
    }

    
    
    
}
