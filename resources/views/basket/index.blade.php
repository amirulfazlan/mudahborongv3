<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>

<script type="text/javascript">
                           

                           $(document).ready(function () {
                           var $inputs = $('input[type="checkbox"]')
                           $inputs.on('change', function () {
                           var sum = 0;
                           $inputs.each(function() {
                           if(this.checked)
                           
                           sum += parseFloat(this.value);
                           });
                           var cal = sum;

                           var cal2= Math.round(cal * 10)/10;

                           $("#prine_price").val(cal2.toFixed(2));


                           var prime = document.getElementById('prine_price').value;
                            
                           var all_price = document.getElementById('sum_all').value;

                           var kira = (parseFloat(prime) + parseFloat(all_price)).toFixed(2) ;
                           
                           var result=Math.round(kira*10)/10

                            document.getElementById('final_price').value = result.toFixed(2);


                           
                           console.log (kira);
                          
                           });
                           });
  </script>

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
           
            <!-- HEADER -->

            @include('layouts.mango.layout.header_checkout')

            <!-- HEADER -->
            

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="/shopping_catalog">Shop</a>
                    <a href="/basket">Shopping Cart</a>
                </div>

                <form action="/hantarla" method="POST">
                 {!! csrf_field() !!}


                <div class="information-blocks">
                    <div class="table-responsive">
                        <table class="cart-table">
                            <tr>
                                <th class="column-1">Product Name</th>
                                <th class="column-2">Unit Price</th>
                                <th class="column-3">Quantity</th>
                                <th class="column-3">Min Moq</th>
                                <th class="column-4">Subtotal</th>
                                <th class="column-5">Prime</th>
                                <th class="column-5">Prime Price</th>

                                <th class="column-6"></th>
                            </tr>

                            <?php $total = 0; 
                                  $i = 0;
                                  $totalharga = 0;
                                  $totalcheck = 0;        
                            ?>
                                     
                             @foreach($all_new_array as $selipar)
                             
                             <?php 

                             $i++;    

                             $bundar = ceil($selipar['unit_price'] * 2 ) / 2;

                             $sub = $bundar * $selipar['total_qt'] ;

                             $totalharga += $sub;

                             $subcheck = $selipar['unit_check'];

                             $totalcheck += $subcheck;

                             ?>

                             
                             



                            <tr>
                                <td>
                                    <div class="traditional-cart-entry">
                                         
                                         @foreach ($pro as $product)
                                         @if ( $product['id'] == $selipar['item_id'] )
                                        <a href="/shopping_catalog/{{$selipar['item_id']}}" class="image"><img src="{{ 'images/main_product/'.$product['main_image'] }}" alt=""></a>
                                        <div class="content">
                                            <div class="cell-view">
                                                <a href="#" class="tag">
                                                    
                                                 @if ($product['gender_id'] == 1)
                                                 Men's Clothing
                                                 @else
                                                 Women's Clothing
                                                 @endif

                                                </a>
                                         @endif
                                         @endforeach 



                                        
                                            @if($selipar['total_qt'] < $selipar['unit_min'])
                                                 <a href="/shopping_catalog/{{$selipar['item_id']}}" class="title">{{$selipar['name_item']}}</a>

                                                <p style="color:red;">Please add more quantity for this item. Minimum item must be equal or more than {{$selipar['unit_min']}}</p>
                                              
                                            @else
                                            <a href="/shopping_catalog/{{$selipar['item_id']}}" class="title">{{$selipar['name_item']}}</a>
                                            @endif

                                            <input type="hidden" name="item_id[{{$i-1}}]" value="{{$selipar['item_id']}}">
                                  
                                                
                                            </div>
                                        </div>
                                    </div>
                                </td>

                                
                                <td>{{ number_format($bundar, 2) }}</td>
                               
                                
                                <td>
                                     <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title"></div>
                                            
                                           
                                                <div class="entry number">{{$selipar['total_qt']}}</div>
                                           
                                       
            
                                        </div>
                                </td>

                                <td>
                                     <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title"></div>
                                            
                                           
                                                <div class="entry number">{{$selipar['unit_min']}}</div>
                                           
                                       
            
                                        </div>
                                </td>



                                <td><div class="subtotal">{{number_format($sub, 2)}}</div></td>

                                <td>
                                    <center>

                                     @foreach($prines as $price)
                                            <?php $prine_total =  $sub * $price['prine_price'] ;
                                              $rounprime = round($prine_total ,1,PHP_ROUND_HALF_UP) ;
                                             ?>

                                            <div class="checkbox-entry-inline">
                                            <label class="checkbox-entry">
                                            <input type="hidden" name="check[{{$i-1}}]" value=0>
                                            <input type="checkbox" name="check[{{$i-1}}]" value="{{$rounprime}}"> Yes
                                            <span class="check"></span>
                                            </label>
                                            </div>

                                     @endforeach
                                   </center>
                                   
                                </td>

                                 <td>
                                    <center>

                                     @foreach($prines as $price)
                                            <?php $prine_total =  $sub * $price['prine_price'] ;
                                                  $rounprime = round($prine_total ,1,PHP_ROUND_HALF_UP) ;
                                             ?>


      
                                             <div class="subtotal">{{number_format($rounprime, 2)}}</div>
                                             
                                             <input type="hidden" name="array_prime[]"  value="{{number_format($rounprime, 2)}}">

                                              <input type="hidden" name="array_namepro[]"  value="{{$selipar['item_id']}}">

                                           
                                     @endforeach
                                   </center>
                                   
                                </td>

                                
                            </tr>

                             @endforeach

                            
                        </table>
                    </div>
                    <div class="cart-submit-buttons-box">
                        <a href="/cart" class="button style-15">Edit Basket Item</a>
                        <
                    </div>
                    <div class="row">
                        <div class="col-md-4 information-entry">
                            
                        </div>
                        <div class="col-md-4 information-entry">
                            
                        </div>



                        <div class="col-md-4 information-entry">
                            <div class="cart-summary-box">

                                <div class="sub-total">Subtotal: RM {{number_format($totalharga,2)}} <input type="hidden" name="sum_all" value="{{number_format($totalharga,2)}}"><input type="hidden" id="sum_all" value="{{$totalharga}}"> </div>

                                <div class="sub-total">Prime:  RM <input type="textbox" name="prine_price" id="prine_price" style="width:80px" value="0.00" /></div>

                                <div class="grand-total">Grand Total: RM <input type="textbox" name="final_price" id="final_price" style="width:80px" value="{{number_format($totalharga,2)}}" /></div>

                               @if($totalcheck < $i)

                                <input type="submit" name="submit" value = "Proceed To Checkout"  class="button style-10" disabled="true">
                                <H2>Please make sure total quantity for an item is equal to or more than minimum moq before proceeding</H2>

                                @else

                                <input type="submit" name="submit" value = "Proceed To Checkout"  class="button style-10">

                                @endif




                                <!--<input type="submit" name="pay" value = "Proceed To Pay"  class="button style-10">-->
                               
                            </div>
                        </div>

                    </div>
                </div>
                 </form>


               <!--  @include('layouts.mango.layout.product_features_inline') 
 -->
                 <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

   

    @include('layouts.mango.layout.scripts')

</body>
</html>
