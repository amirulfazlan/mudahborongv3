
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

   <div class="page-container ">

     @include('layouts.topbar')

    
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">View Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">View Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

          
      


       <div class="form-group row">
                    
                      <label class="col-sm-2 control-label">Main & Back Image</label>
                        
                         
                         <a class="image hover-class-1" href="#"><img src="{{ asset('images/main_product/'.$product->main_image) }}" alt="" style="height: 350px; width: 350px" /></a>
              
                          <a class="image hover-class-1" href="#"><img src="{{ asset('images/main_product/'.$product->back_image) }}" alt="" style="height: 350px; width: 350px" /></a>
                        
                  
                    </div>


      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" name="p_name" value="{{$product->p_name}}">
          

        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Release Date</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="release_date" name="release_date" value="{{$product->release_date}}">
           

        </div>
      </div>
      </div>

      
      <div class="form-group row">
       <div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Brand Name</label>
        <div class="col-sm-5">

          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="brand_id" name="brand_id" value="{{$product->product_brand->brand_name}}">
            

        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Gender</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="gender" name="gender" value="{{$product-> product_gender->g_name}}">

        </div>
      </div>
      </div>


       <div class="form-group row">
       <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product Category</label>
        <div class="col-sm-5">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="category_id" name="category_id" value="{{$product->product_category->category_product}}">

         
        
        </div>
      </div>
      </div> 

       <div class="form-group row">
                    
                      <label class="col-sm-2 control-label">Size Chart</label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/size_chart/'.$product['size_chart']) }}" class="img-responsive"/> 
                        </div>
                  
                    </div>

      
       
       <div class="form-group row">
      <div class="form-group {{ $errors->has('st_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size Type</label>
        <div class="col-sm-5">
          @foreach($size_type as $saiz)
          @if ($product['st_id'] == $saiz['id'] )
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="st_id" name="st_id" value="{{$saiz['size_name'] }}">
          @endif
          @endforeach
            @if ($errors->has('st_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('st_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>


      <div class="form-group row">
       <div class="form-group {{ $errors->has('base_price1') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 1</label>
        <div class="col-sm-5">
          <div class="input-group">
           <span class="input-group-addon">RM</span>
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="base_price1" name="base_price1" value="{{$product->base_price1}}">
            </div>  
        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('base_price2') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 2</label>
        <div class="col-sm-5">
          <div class="input-group">
           <span class="input-group-addon">RM</span>
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="base_price2" name="base_price2" value="{{$product->base_price2}}">
            </div>  
        </div>
      </div>
      </div>

       <div class="form-group row">
       <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product Moq Type</label>
        <div class="col-sm-5">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="moqty_id" name="moqty_id" value="{{$product->product_moqtype->moq_name}}">

          
        </div>
      </div>
      </div>

       <div class="form-group row">
       <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product Margin Type</label>
        <div class="col-sm-5">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="margin_id" name="margin_id" value="{{$product->product_margin->name}}">

          
        </div>
      </div>
      </div>


        


      
      @foreach ($cur as $yuan)

      @foreach ($margin as $mar)
 
      @if( $mar['id'] == $product['margin_id'] )

       @foreach ($moq_item as $moq)

        @if( $moq['moqty_id'] == $product['moqty_id'] )
       
      
        <?php $admin_price1 = ( $product['base_price1'] /  $yuan['cur_value'])  +  $mar['mar_admin1'];
              $admin_price2 = ( $product['base_price2'] /  $yuan['cur_value'])  + $mar['mar_admin2']  ;
              $admin_price3 = ( $product['base_price2'] / $yuan['cur_value'])   + $mar['mar_admin3'] ;
                                         
             $whole_price1 =  $admin_price1 + $mar['mar_whole1'] ;
             $whole_price2 =  $admin_price2 + $mar['mar_whole2'] ;
             $whole_price3 =  $admin_price3 + $mar['mar_whole3'] ;

                                    ?>



        <div class="form-group row">
        <div class="form-group {{ $errors->has('admin_price1') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq {{ $moq['min_unit1'] }}</label>
        <div class="col-sm-5">  
           <div class="input-group">
           <span class="input-group-addon">RM</span>
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="admin_price1" name="admin_price1" value="{{ number_format($admin_price1, 2) }}">
            </div>  
        </div>
      </div>
      </div>

     


        <div class="form-group row">
        <div class="form-group {{ $errors->has('admin_price3') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq {{ $moq['min_unit2'] }}</label>
        <div class="col-sm-5">  
           <div class="input-group">
           <span class="input-group-addon">RM</span>
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="admin_price3" name="admin_price3" value="{{ number_format($admin_price2, 2) }}">
            </div>  
        </div>
      </div>
      </div>


     

       <div class="form-group row">
        <div class="form-group {{ $errors->has('admin_price2') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq {{ $moq['min_unit3'] }}</label>
        <div class="col-sm-5">  
           <div class="input-group">
           <span class="input-group-addon">RM</span>
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="admin_price2" name="admin_price2" value="{{ number_format($admin_price3, 2) }}">
            </div>  
        </div>
      </div>
      </div>


      
      
     
      @endif 

      @endforeach 
       


      @endif 


       
     
      @endforeach

       
     
      @endforeach



      <div class="form-group row">
      <div class="form-group {{ $errors->has('escription') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
        <div class="col-sm-5">
          <textarea name="description" rows="8" cols="80" readonly>{{$product->description}}</textarea>
          
        </div>
      </div>
      </div>

      <div class="form-group row">
        <div class="form-group {{ $errors->has('featured_product') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Featured Product</label>
        <div class="col-sm-5">  
           <div class="input-group">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="No" name="featured_product" value="{{$product->featured_product}}">
            </div>  
        </div>
      </div>
      </div>

      <div class="form-group row">
        <div class="col-md-2"></div>
        <a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-primary">Edit <span class="glyphicon glyphicon-pencil"></span></a>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>

      
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>