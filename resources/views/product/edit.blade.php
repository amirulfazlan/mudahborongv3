
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
   <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

    <form method="post" action="{{action('ProductController@update', $id)}}" enctype="multipart/form-data">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}

     <div class="form-group row">
                    
                      <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/main_product/'.$product->main_image) }}" alt="{{$product->brand_image}}" class="img-responsive"/> 
                        </div>
                  
                    </div>

          

           <div class="form-group row">
           <div class="form-group {{ $errors->has('main_image') ? ' has-error' : '' }}">
           <label class="col-sm-2 control-label">Main Image</label>
           <div class="col-sm-6">
           <a class="btn btn-primary" onclick='javascript:window.open(" {{action('UpdateMainImageController@edit', $product['id'])}} ", "_$product->id", "scrollbars=1,resizable=1,height=400,width=450");' title="Update Main Image">Update</a>
           </div> 
           </div>
           </div>

          <div class="form-group row">
           <div class="form-group {{ $errors->has('back_image') ? ' has-error' : '' }}">
           <label class="col-sm-2 control-label">Back Image</label>
           <div class="col-sm-6">
           <a class="btn btn-primary" onclick='javascript:window.open(" {{action('UpdateBackImageController@edit', $product['id'])}} ", "_$product->id", "scrollbars=1,resizable=1,height=400,width=450");' title="Update Back Image">Update</a>
           </div> 
           </div>
           </div>



           <div class="form-group row">
           <div class="form-group {{ $errors->has('size_chart') ? ' has-error' : '' }}">
           <label class="col-sm-2 control-label">Size Chart</label>
           <div class="col-sm-6">
           <a class="btn btn-primary" onclick='javascript:window.open(" {{action('UpdateSizeChartController@edit', $product['id'])}} ", "_$product->id", "scrollbars=1,resizable=1,height=400,width=450");' title="Update Size Image">Update</a>
           </div> 
           </div>
           </div>
     

      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" name="p_name" value="{{$product->p_name}}">
          @if ($errors->has('p_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_name') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Release Date</label>
        <div class="col-sm-5">
          <input type="date" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="release_date" name="release_date" value="{{$product->release_date}}">
           @if ($errors->has('release_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('release_date') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      
      <div class="form-group row">
       <div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Brand Name</label>
        <div class="col-sm-5">
          <div class="dropdown">
            <select id="brand_id" name="brand_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->brand_id}}">{{$product->product_brand->brand_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($brand as $post)
                <option value="{{$post['id']}}">{{$post['brand_name']}}</option>
                @endforeach      
              </ul>
            </select>
            @if ($errors->has('brand_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>


      <script type="text/javascript">
             $(document).ready(function() {
             $('select[name="gender_id"]').on('change', function() {
            var typeID = $(this).val();
            if( typeID) {
                $.ajax({
                    url: '/myform/ajax/'+ typeID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="category_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="category_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                       }
                   });
                 }else{
                $('select[name="category_id"]').empty();
                  }
                });
                });
              </script>


            <div class="form-group row">
             <div class="form-group {{ $errors->has('gender_id') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Gender</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="gender_id" name="gender_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->gender_id}}">{{$product->product_gender->g_name}}</option>
                 @foreach ($gen as $key => $value)
                        <option value="{{ $key }}" >{{ $value }}</option>
                    @endforeach
              </ul>
            </select>
            </div>
             @if ($errors->has('gender_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('gender_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


            <div class="form-group row">
               <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
               <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Category</label>
               <div class="col-sm-5">
               <div class="dropdown">
               <select id="category_id" name="category_id" class="form-control" required>
               <span class="caret"></span>
               <ul class="dropdown-menu">
                <option value="{{$product->category_id}}">{{$product->product_category->category_product}}</option>
                <div class="dropdown-divider"></div>
               
              </ul>
              </select>
              @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
              </div>
              </div>
              </div>
              </div>


              <div class="form-group row">
              <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
              <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq Type</label>
              <div class="col-sm-5">
              <div class="dropdown">
              <select id="moqty_id" name="moqty_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->moqty_id}}">{{$product->product_moqtype->moq_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($moq_type as $post2)
                <option value="{{$post2['id']}}">{{$post2['moq_name']}}</option>
                @endforeach
              </ul>
              </select>
             
               </div>
               </div>
               </div>
               </div>

               <div class="form-group row">
              <div class="form-group {{ $errors->has('margin_id') ? ' has-error' : '' }}">
              <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Margin Type</label>
              <div class="col-sm-5">
              <div class="dropdown">
              <select id="moqty_id" name="margin_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->margin_id}}">{{$product->product_margin->name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($margin as $post7)
                <option value="{{$post7['id']}}">{{$post7['name']}}</option>
                @endforeach
              </ul>
              </select>
             
               </div>
               </div>
               </div>
               </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('st_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size Type</label>
        <div class="col-sm-5">
          <div class="dropdown">
          <select id="st_id" name="st_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                @foreach($size_type as $saiz)
                @if ($product['st_id'] == $saiz['id'] )
                <option value="{{$product->st_id}}">{{$saiz['size_name'] }}</option>
                 @endif
                 @endforeach
                <div class="dropdown-divider"></div>
                @foreach($size_type as $post2)
                <option value="{{$post2['id']}}">{{$post2['size_name']}}</option>
                @endforeach
              </ul>
            </select>
           
          </div>
        </div>
      </div>
      </div>





       <div class="form-group row">
             <div class="form-group {{ $errors->has('base_price1') ? ' has-error' : '' }}">
             <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 1</label>
             <div class="col-sm-5">
             <div class="input-group">
             <span class="input-group-addon">RM</span>
             <input name="base_price1" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Base Price 1" value="{{$product->base_price1}}">
             <span class="input-group-addon">.00</span>
             </div>
             @if ($errors->has('base_price1'))
                  <span class="help-block">
                      <strong>{{ $errors->first('base_price1') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

             <div class="form-group row">
             <div class="form-group {{ $errors->has('base_price2') ? ' has-error' : '' }}">
             <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 2</label>
             <div class="col-sm-5">
             <div class="input-group">
             <span class="input-group-addon">RM</span>
             <input name="base_price2" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Base Price 2" value="{{$product->base_price2}}">
             <span class="input-group-addon">.00</span>
             </div>
             @if ($errors->has('base_price2'))
                  <span class="help-block">
                      <strong>{{ $errors->first('base_price2') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('escription') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
        <div class="col-sm-5">
          <textarea name="description" rows="8" cols="80">{{$product->description}}</textarea>
          @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
             <div class="form-group {{ $errors->has('featured_product') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Featured Product</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="featured_product" name="featured_product" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
               
                @if( $product['featured_product'] == null )
                <option value="No">No</option>
                <option value="Yes">Yes</option>
                @else
                <option value="{{$product->featured_product}}">{{$product->featured_product}}</option>
                <option value="No">No</option>
                @endif           
               
              </ul>
            </select>
            </div>
             @if ($errors->has('featured_product'))
                  <span class="help-block">
                      <strong>{{ $errors->first('featured_product') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>


    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>