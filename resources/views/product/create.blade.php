
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Insert Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Insert New Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

              <form action="/product" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}


              <div class="form-group row">
                    <div class="form-group {{ $errors->has('main_image') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Main Image</label>
                        <div class="col-sm-5">
                          <input type="file" name="main_image" class="form-control">
                          @if ($errors->has('main_image'))
                          <span class="help-block">
                          <strong>{{ $errors->first('main_image') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>


                    <div class="form-group row">
                    <div class="form-group {{ $errors->has('Back_image') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Back Image</label>
                        <div class="col-sm-5">
                          <input type="file" name="back_image" class="form-control">
                          @if ($errors->has('back_image'))
                          <span class="help-block">
                          <strong>{{ $errors->first('back_image') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>


              <div class="form-group row">
              <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Product Name</label>
              <div class="col-sm-5">
              <input type="text" name="p_name" class="form-control" required>
              @if ($errors->has('p_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_name') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


            <div class="form-group row">
              <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Release Date</label>
              <div class="col-sm-2">
              <input type="date" name="release_date" class="form-control">
              @if ($errors->has('release_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('release_date') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

             <div class="form-group row">
             <div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Brand Name</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="brand_id" name="brand_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Brand</option>
                @foreach($brand as $post2)
                <option value="{{$post2['id']}}">{{$post2['brand_name']}}</option>
                @endforeach    
              </ul>
            </select>
            </div>
             @if ($errors->has('brand_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('brand_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


              <script type="text/javascript">
             $(document).ready(function() {
             $('select[name="gender_id"]').on('change', function() {
            var typeID = $(this).val();
            if( typeID) {
                $.ajax({
                    url: '/myform/ajax/'+ typeID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="category_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="category_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                       }
                   });
                 }else{
                $('select[name="category_id"]').empty();
                  }
                });
                });
              </script>


            <div class="form-group row">
             <div class="form-group {{ $errors->has('gender_id') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Gender</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="gender_id" name="gender_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Gender</option>
                 @foreach ($gen as $key => $value)
                        <option value="{{ $key }}" >{{ $value }}</option>
                    @endforeach
              </ul>
            </select>
            </div>
            
            </div>
            </div>
            </div>



            <div class="form-group row">
               <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
               <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Category</label>
               <div class="col-sm-5">
               <div class="dropdown">
               <select id="category_id" name="category_id" class="form-control" required>
               <span class="caret"></span>
               <ul class="dropdown-menu">
                <option value="">Choose Category</option>
                <div class="dropdown-divider"></div>
               
              </ul>
              </select>
              @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
              </div>
              </div>
              </div>
              </div>

              
              <div class="form-group row">
              <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
              <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq Type</label>
              <div class="col-sm-5">
              <div class="dropdown">
              <select id="moqty_id" name="moqty_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Moq Type</option>
                <div class="dropdown-divider"></div>
                @foreach($moq_type as $post2)
                <option value="{{$post2['id']}}">{{$post2['moq_name']}}</option>
                @endforeach
              </ul>
              </select>
              @if ($errors->has('moqty_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('moqty_id') }}</strong>
                    </span>
                @endif
               </div>
               </div>
               </div>
               </div>


               <div class="form-group row">
              <div class="form-group {{ $errors->has('margin_id') ? ' has-error' : '' }}">
              <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Magrin Type</label>
              <div class="col-sm-5">
              <div class="dropdown">
              <select id="moqty_id" name="margin_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Margin Type</option>
                <div class="dropdown-divider"></div>
                @foreach($margin as $post7)
                <option value="{{$post7['id']}}">{{$post7['name']}}</option>
                @endforeach
              </ul>
              </select>
              @if ($errors->has('margin_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('margin_id') }}</strong>
                    </span>
                @endif
               </div>
               </div>
               </div>
               </div>


            <div class="form-group row">
             <div class="form-group {{ $errors->has('st_id') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Size Type</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="st_id" name="st_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Size Type</option>
                @foreach($size_type as $size)
                <option value="{{$size['id']}}">{{$size['size_name']}}</option>
                @endforeach 
              </ul>
            </select>
            </div>
             @if ($errors->has('st_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('st_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


            <div class="form-group row">
                    <div class="form-group {{ $errors->has('size_chart') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Size Chart</label>
                        <div class="col-sm-5">
                          <input type="file" name="size_chart" class="form-control">
                          @if ($errors->has('size_chart'))
                          <span class="help-block">
                          <strong>{{ $errors->first('size_chart') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>

             <div class="form-group row">
             <div class="form-group {{ $errors->has('base_price1') ? ' has-error' : '' }}">
             <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 1</label>
             <div class="col-sm-5">
             <div class="input-group">
             <span class="input-group-addon">RM</span>
             <input name="base_price1" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Base Price" required>
             <span class="input-group-addon">.00</span>
             </div>
             @if ($errors->has('base_price1'))
                  <span class="help-block">
                      <strong>{{ $errors->first('base_price1') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

             <div class="form-group row">
             <div class="form-group {{ $errors->has('base_price2') ? ' has-error' : '' }}">
             <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Base Price 2</label>
             <div class="col-sm-5">
             <div class="input-group">
             <span class="input-group-addon">RM</span>
             <input name="base_price2" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Base Price" required>
             <span class="input-group-addon">.00</span>
             </div>
             @if ($errors->has('base_price2'))
                  <span class="help-block">
                      <strong>{{ $errors->first('base_price2') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

    

            <div class="form-group row">
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-5">
              <textarea class="form-control" name="description" rows="8" cols="40"></textarea>
              @if ($errors->has('description'))
                  <span class="help-block">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
            </div>
          </div>
          </div>

           <div class="form-group row">
             <div class="form-group {{ $errors->has('featured_product') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Featured Product</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="featured_product" name="featured_product" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </ul>
            </select>
            </div>
             @if ($errors->has('featured_product'))
                  <span class="help-block">
                      <strong>{{ $errors->first('featured_product') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>
        

         


      <center><input type="submit" name="submit" value = "Create Product"  class="btn btn-primary"></center>
      </form>

    <div class="panel-heading"> </div>

  </div>
  </div>
  </div>
  </div>
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>