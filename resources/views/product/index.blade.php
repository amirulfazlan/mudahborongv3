
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>


   <script type="text/javascript">
             $(document).ready(function() {
             $('select[name="gender_id"]').on('change', function() {
            var typeID = $(this).val();
            if( typeID) {
                $.ajax({
                    url: '/myform/ajax/'+ typeID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="category_id"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="category_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                       }
                   });
                 }else{
                $('select[name="category_id"]').empty();
                  }
                });
                });
              </script>


  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="panel panel-transparent">
              <div class="panel-heading">

              <form method="get" action="/product">

              <div class="form-group row">
              <div class="form-group {{ $errors->has('search_name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Product Name</label>
              <div class="col-sm-5">
              <input type="text" name="search_name" class="form-control">
              @if ($errors->has('search_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('search_name') }}</strong>
                  </span>
              @endif


            </div>

             <button type="submit" class="btn btn-primary">Search</button>
            
            </div>

           

            </div>

          </form>

            </br>

                <div class="panel-title">List Of Product
                </div>
                </br>
                <div class="btn-group pull-right m-b-10">
                 <a  href="{{route('product.create')}}" class="btn btn-primary" >Add New Product
                 </a>
                 </br>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">

                  <form method="get" action="/product">

                               
                                   <div class="form-group row">
                                        <label class="col-sm-2 control-label">Filter</label>

                                        <script type="text/javascript">
                                        $(document).ready(function() {
                                        $('select[name="gender_id"]').on('change', function() {
                                        var typeID = $(this).val();
                                        if( typeID) {
                                        $.ajax({
                                        url: '/myform/ajax/'+ typeID,
                                        type: "GET",
                                        dataType: "json",
                                        success:function(data) {

                        
                                       $('select[name="category_id"]').empty();
                                       $.each(data, function(key, value) {
                                       $('select[name="category_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                                       });

                                       }
                                       });
                                       }else{
                                       $('select[name="category_id"]').empty();
                                       }
                                       });
                                       });
                                       </script>

                                        <div class="col-sm-2">    
                                        <select id="gender_id" name="gender_id" class="form-control" >
                                        <option value="0">All</option>  
                                        @foreach ($gen as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                        @endforeach     
                                        </select>

                                        </div>


                                         <div class="col-sm-2">
                                         <select id="category_id" name="category_id" class="form-control" >
                                         <option value="0">All</option>      
                                         </select>
                                         </div>
                                        

                                         <div class="col-sm-2">  
                                         <select name="brand_id" class="form-control" >
                                         <option value="0">All</option>    
                                         @foreach ($brand as $label)
                                         <option value="{{$label['id']}}">{{$label['brand_name']}}</option>
                                          @endforeach
                                         </select>
                                         </div>

                                        
                                        <button type="submit" class="btn btn-primary">Filter</button>
                                    </div>

                                    </form>



                 </br>

                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Image</th>
                        <th style="width:20%">Name</th>
                        <th style="width:20%">Release Date</th>
                        <th style="width:20%">Brand</th>
                        <th style="width:20%">Category</th>
                        <th style="width:15%">View</th>                       
                        <th style="width:15%">Edit</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                       @foreach($product as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        <td class="v-align-middle">
                          <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="{{ $post['main_image'] }}" height="80px"/>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{$post['p_name']}}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{$post['release_date']}}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $post->product_brand['brand_name']}}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $post->product_category['category_product']}}</p>
                        </td>

                        <td class="v-align-middle">

                         <a href="{{action('ProductController@show', $post['id'])}}" class="btn btn-info">View <span class="glyphicon glyphicon-eye-open"></span></a>

                        </td>
                        
                        <td class="v-align-middle">

                         <a href="{{action('ProductController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a>

                        </td>
                        <td class="v-align-middle">
                          <form action="{{action('ProductController@destroy', $post['id'])}}" method="post">
                          {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                           </form>
                        </td>
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>

                  {{$product->links() }}
                  
                </div>
              </div>
            </div>


           
         
             
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

    </div>
      @include('layouts.scripts')

  </body>
</html>