<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">{{Auth::user()->name}}</a>
                  </li>
                  <li>
                    <a href="#" class="active">My Order</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

          <div class="container-fluid">
            <div class="row">
             @include('layouts.profileside')
              <div class="col-md-10 ">
                  <div class="panel panel-default">
                      <div class="panel-heading"><h1><strong>Hi , {{Auth::user()->name}} &#9825</strong></h1></div>

                        <div class="panel-body">
                        Here is the list of your order.<br>
                        <br>

                         <div class="table-responsive">
                         <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Product Name</th>
                        <th style="width:20%">Product Code</th>
                        <th style="width:20%">Order Date</th>
                        <th style="width:20%">Order Total</th>
                         <th style="width:20%">Order Status</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                        @foreach($orders as $order)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        <td class="v-align-middle">
                           @foreach ($pro_var as $name)
                                                  @if( $name['id'] == $order->product_id) 
                                                        @foreach ($pro as $pro_name)
                                                        @if( $pro_name['id'] == $name['p_id'])
                                                        <p>{{$pro_name['p_name']}}</p>
                                                        @endif
                                                        @endforeach
                                                  @endif 
                                                  @endforeach
                        </td>
                        <td class="v-align-middle ">
                          @foreach ($pro_var as $name)
                                                  @if( $name['id'] == $order->product_id) 
                                                       <p>{{$name['sku']}}</p>
                                                  @endif 
                                                  @endforeach
                        </td>
                        <td class="v-align-middle ">
                          <p>{{$order->created_at}}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{$order->total}}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{$order->status}}</p>
                        </td>                 
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>
                </div>


                        
                         
                   		</div>

                        

                      </div>
                  </div>
              </div>
          </div>
            





            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>