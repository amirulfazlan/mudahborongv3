    <!DOCTYPE html>
    <html>

    @include('layouts.mango.layout.head')


    <body class="style-10">

        <!-- LOADER -->
        <div id="loader-wrapper">
            <div class="bubbles">
                <div class="title">loading</div>
                <span></span>
                <span id="bubble2"></span>
                <span id="bubble3"></span>
            </div>
        </div>

        <div id="content-block">

            <div class="content-center fixed-header-margin">
            
                 <!-- HEADER -->
                @include('layouts.mango.layout.header')
                <!-- HEADER -->

                <div class="content-push">

                    <div class="breadcrumb-box">
                        <a href="#">Home</a>
                        <a href="#">My Account</a>
                        <a href="#">Profile</a>
                    </div>

                    <div class="information-blocks">
                        <div class="row">
                            <div class="col-sm-9 col-sm-push-3">
                                <div class="wishlist-header hidden-xs">
                                    <div class="title-1">Basic Information</div>
                                    
                                </div>
                                <div class="wishlist-box">
                                    <div class="wishlist-entry">


                                       
                                        <div class="column-1">
                                            <div class="traditional-cart-entry">
                                                <a class="image" href="#"><img alt="" src="img/product-minimal-1.jpg" /></a>
                                                <div class="content">
                                                    <div class="cell-view">
                                                        <!-- <a class="tag" href="#">woman clothing</a> -->
                                                            

                                                    

                                                            <div class="description"><h6>Username : {{Auth::user()->name}}</h6></div><br>

                                                            <div class="description"><h6>Email    : {{ Auth::user()->email }}</h6></div><br>
                                                        

                                                        <div class="inline-description"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        


                                    </div>     
                                                   
                                </div>


                            </div>
                            
                            <div class="col-sm-3 col-sm-pull-9 blog-sidebar">
                                <div class="information-blocks">
                                    <div class="categories-list account-links">
                                        <div class="block-title size-3">My Account</div>
                                        
                                    </div>
                                    <div class="article-container">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- FOOTER -->
                     @include('layouts.mango.layout.footer')
                     <!-- FOOTER -->

                   
                </div>

            </div>
            <div class="clear"></div>

        </div>

        <div class="overlay-popup" id="image-popup">
            
            <div class="overflow">
                <div class="table-view">
                    <div class="cell-view">
                        <div class="close-layer"></div>
                        <div class="popup-container">
                            <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                            <div class="close-popup"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @include('layouts.mango.layout.searchbox_popup')


        @include('layouts.mango.layout.cart_popup_item')

       
       @include('layouts.mango.layout.scripts')

       


    </body>
    </html>
