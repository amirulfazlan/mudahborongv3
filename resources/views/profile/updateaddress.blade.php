<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">{{Auth::user()->name}}</a>
                  </li>
                  <li>
                    <a href="#" class="active">My Address</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="container-fluid">
                    <div class="row">

@include('layouts.profileside')



                        <div class="col-md-10 ">
                          <div class="panel panel-default">
                              <div class="panel-heading"><h1><strong>Hi , {{Auth::user()->name}} &#9825</strong></h1></div>

                          <div class="panel-body">
                          Change your address.<br>
                          <br>

                          <form method="post" action="{{url('/changeAddress')}}">
                            {!! csrf_field() !!}

                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Firstname</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname">
                                            <span style ="color:red">{{$errors->first('firstname')}}</span>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Lastname</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname">
                                            <span style ="color:red">{{$errors->first('lastname')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Company</label>
                                            <input type="text" class="form-control" id="company" name="company">
                                            <span style ="color:red">{{$errors->first('company')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" id="street" name="street">
                                            <span style ="color:red">{{$errors->first('street')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">Company</label>
                                            <input type="text" class="form-control" id="city" name="city">
                                            <span style ="color:red">{{$errors->first('city')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="postcode">Postcode</label>
                                            <input type="text" class="form-control" id="postcode" name="postcode">
                                            <span style ="color:red">{{$errors->first('postcode')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state" name="state">
                                            <option value="">Choose State</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Kuala Lumpur/Putrajaya">Kuala Lumpur/Putrajaya</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Terengganu">Terengganu</option>
                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah/Labuan">Sabah/Labuan</option>
                                            <option value="Sarawak">Sarawak</option>

                                            </select>
                                            <span style ="color:red">{{$errors->first('state')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" id="country" name="country">
                                            <option value="Malaysia">Malaysia</option>
                                            </select>
                                            <span style ="color:red">{{$errors->first('country')}}</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Telephone</label>
                                            <input type="text" class="form-control" id="phone" name="phone">
                                            <span style ="color:red">{{$errors->first('phone')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email">
                                            <span style ="color:red">{{$errors->first('email')}}</span>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>
                            <input type="submit" value="Update" class="btn btn-primary">
                              

                          </form>


                        

                   		     </div>

                        

                      
                          </div>
                      </div>
                </div>

              </div>

              
            





            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>