    <!DOCTYPE html>
    <html>

    @include('layouts.mango.layout.head')


    <body class="style-10">

        <!-- LOADER -->
        <div id="loader-wrapper">
            <div class="bubbles">
                <div class="title">loading</div>
                <span></span>
                <span id="bubble2"></span>
                <span id="bubble3"></span>
            </div>
        </div>

        <div id="content-block">

            <div class="content-center fixed-header-margin">
            
                 <!-- HEADER -->
                  <!-- @include('layouts.mango.layout.header_checkout') -->

                  <div class="header-wrapper style-10">
                <header class="type-1">

                    <div class="header-product">
                        <div class="logo-wrapper">
                            <a href="#" id="logo"><img alt="" src="{{ asset ('mango/img/logo-9.png') }}"></a>
                        </div>
                         
                    </div>

            
                </header>
               
            </div>
                <!-- HEADER -->

                <div class="content-push">

                    <div class="breadcrumb-box">
                       
                        <a href="/shopping_catalog">Shop</a>
                        <a href="#">Invoice</a>
                    </div>

                    <div class="information-blocks">
                        <div class="row">
                            <div class="col-sm-9 col-sm-push-3">
                                <div class="wishlist-header hidden-xs">
                                    <div class="title-1">Invoice</div>
                                    
                                </div>
                                <div class="wishlist-box">
                                    <div class="wishlist-entry">


                                       
                                        <div class="column-1">
                                            <div class="traditional-cart-entry">
                                                <a class="image" href="#"><img alt="" src="img/product-minimal-1.jpg" /></a>
                                                <div class="content">
                                                    <div class="cell-view">
                                                        <!-- <a class="tag" href="#">woman clothing</a> -->
                                                            

            
                                                     <div class="title"><h6>Thank you so much {{Auth::user()->name}}</h6></div><br>

                                                      <div class="description"><h6> Your order has been placed.</h6></div><br>

                                                     <div class="description">Click here for your invoice . </h6></div><br>
                                                        

                                                        <div class="inline-description"></div>

                                                        <div class="traditional-cart-entry">
                                                
                                                         <a class="button style-14" href="{{url('/invoice')}}">My Invoice</a> &nbsp; &nbsp;

                                                         <a class="button style-15" href="{{url('/invoice')}}">Back to Home</a>                                  
                                                         </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        


                                    </div>     
                                                   
                                </div>


                            </div>
                            
                            
                        </div>
                    </div>

                    <!-- FOOTER -->
                     @include('layouts.mango.layout.footer')
                     <!-- FOOTER -->

                   
                </div>

            </div>
            <div class="clear"></div>

        </div>

        <div class="overlay-popup" id="image-popup">
            
            <div class="overflow">
                <div class="table-view">
                    <div class="cell-view">
                        <div class="close-layer"></div>
                        <div class="popup-container">
                            <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                            <div class="close-popup"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @include('layouts.mango.layout.searchbox_popup')

       
       @include('layouts.mango.layout.scripts')

       


    </body>
    </html>
