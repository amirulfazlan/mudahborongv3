<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">{{Auth::user()->name}}</a>
                  </li>
                  <li>
                    <a href="#" class="active">Password Management</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="container-fluid">
                    <div class="row">

                 @include('layouts.profileside')



                        <div class="col-md-10 ">
                          <div class="panel panel-default">
                              <div class="panel-heading"><h1><strong>Hi , {{Auth::user()->name}} &#9825</strong></h1></div>

                          <div class="panel-body">
                          This is the page to change your password.<br>
                          <br>


                        

                   		     </div>

                        

                      
                          </div>
                      </div>
                </div>

              </div>

              
            





            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>