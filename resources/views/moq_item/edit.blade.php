
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Moq Item</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Moq Item</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

       <form method="post" action="{{action('Moq_ItemsController@update', $id)}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}
     
      
     <div class="form-group row">
      <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Moq Type</label>
        <div class="col-sm-5">
          <div class="dropdown">
          <select id="moqty_id" name="moqty_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$moq_item->moqty_id}}">{{$moq_item->moqtype->moq_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($moq_type as $post)
                <option value="{{$post['id']}}">{{$post['moq_name']}}</option>
                @endforeach
              </ul>
            </select>
            @if ($errors->has('moqty_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('moqty_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>


       <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit1') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 1 = Min Unit</label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" name="min_unit1" value="{{$moq_item->min_unit1}}"> 
          @if ($errors->has('min_unit1'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit1') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit2') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 2 = Min Unit</label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" name="min_unit2" value="{{$moq_item->min_unit2}}"> 
          @if ($errors->has('min_unit2'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit2') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit3') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 3 = Min Unit</label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" name="min_unit3" value="{{$moq_item->min_unit3}}"> 
          @if ($errors->has('min_unit3'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit3') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>



      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>