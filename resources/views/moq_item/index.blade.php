
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Moq Item</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->


           <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Moq Item<span class="semi-bold"></span></h5>
                    <div class="panel-heading">     

                        </div>
                  </div>
                  <div class="modal-body">

                <form action="/moq_item" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}

      
     <div class="form-group row">
      <div class="form-group {{ $errors->has('moqty_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size Name</label>
        <div class="col-sm-5">
          <div class="dropdown">
          <select id="moqty_id" name="moqty_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Moq Name</option>
                <div class="dropdown-divider"></div>
                @foreach($moq_type as $post2)
                <option value="{{$post2['id']}}">{{$post2['moq_name']}}</option>
                @endforeach
              </ul>
            </select>
            @if ($errors->has('moqty_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('moqty_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit1') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 1 = Min Unit </label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="0" name="min_unit1" required> 
          @if ($errors->has('min_unit1'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit1') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit2') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 2 = Min Unit</label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="0" name="min_unit2" required> 
          @if ($errors->has('min_unit2'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit2') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('min_unit3') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">MOQ 3 = Min Unit</label>
        <div class="col-sm-5">
          <input type="number" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="0" name="min_unit3" required> 
          @if ($errors->has('min_unit3'))
                  <span class="help-block">
                      <strong>{{ $errors->first('min_unit3') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      
                      <div class="row">   
                      <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right">
                        <input type="submit" name="submit" value = "Create Moq Item"  class="btn btn-primary btn-block m-t-5 ">
                       <!--  <button type="button" class="btn btn-primary btn-block m-t-5 ">Pay Now</button> -->
                      </div>
                    </div>


    
    </form>

      
      
                    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel panel-transparent">
              <div class="panel-heading">

                <form method="get" action="/moq_item">

              <div class="form-group row">
              <div class="form-group {{ $errors->has('search_name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Moq Type Name</label>
              <div class="col-sm-5">
              <input type="text" name="search_name" class="form-control">
              @if ($errors->has('search_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('search_name') }}</strong>
                  </span>
              @endif


            </div>

             <button type="submit" class="btn btn-primary">Search</button>
            
            </div>

           

            </div>

          </form>

            </br>
                <div class="panel-title">List Of Moq Item
                </div>
                <div class="btn-group pull-right m-b-10">

               <button class="btn btn-primary" id="btnToggleSlideUpSize">Add New Moq Item</button>
                 
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                 </br>
                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Moq Type</th>
                        <th style="width:20%">MOQ 1 Min Unit</th>
                        <th style="width:20%">MOQ 2 Min Unit</th>
                        <th style="width:20%">MOQ 3 Min Unit</th>
                        
                        <th style="width:15%">Edit</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                       @foreach($moq_item as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{ $post->moqtype->moq_name}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>more than {{$post['min_unit1']}}</p>
                        </td>

                         <td class="v-align-middle">
                          <p>more than {{$post['min_unit2']}}</p>
                        </td>

                        <td class="v-align-middle">
                          <p>more than {{$post['min_unit3']}}</p>
                        </td>
                        
                        
                        
                        <td class="v-align-middle">

                         <a href="{{action('Moq_ItemsController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a>

                        </td>
                        <td class="v-align-middle">
                         <form action="{{action('Moq_ItemsController@destroy', $post['id'])}}" method="post">
                         {{csrf_field()}}
                         <input name="_method" type="hidden" value="DELETE">
                         <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                         </form>
                        </td>
                      </tr>
                      
                         @endforeach

                     </tbody>
                  </table>

                   {{ $moq_item->links() }}
                  
                </div>
              </div>
            </div>


            
                  
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
<
       @include('layouts.footer')

     
    </div>
      @include('layouts.scripts')

  </body>
</html>