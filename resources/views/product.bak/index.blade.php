
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <link href="{{asset('dist/css/bootstrap.min.css')}}" rel="stylesheet">
            
           <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">List Of Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>


  <div class="container">
    <table class="table table-striped">
      <thead>


      <tr>

        <td colspan="12" align="right">
          <a  href="{{route('product.create')}}" class="btn btn-primary" >Add New Product
          </a>
        </td>
      </tr>
      <tr>
        <th>Bil</th>
        <th>Image</th>
        <th>Name</th>
        <th>Release Date</th>
        <th>Brand</th>
        <th>Category</th>
        <th>View</th>
        <th>Edit</th>
        <th>Delete</th>

        
      </tr>
      </thead>
      <tbody>

      <?php $i = 0 ?>
      @foreach($product as $post)
      <?php $i++ ?>
       <tr>

            <td>{{ $i}}</td>
            <td>
            <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="{{ $post['main_image'] }}" height="80px"/>
            </td>
            <td>{{$post['p_name']}}  </td>
            <td>{{$post['release_date']}}</td>
            <td>{{ $post->product_brand->brand_name}} </td>  
            <td>{{ $post->product_category->category_product}}</td>
           
  
            <td><a href="{{action('ProductController@show', $post['id'])}}" class="btn btn-info">View <span class="glyphicon glyphicon-eye-open"></span></a></td>
            

            <td><a href="{{action('ProductController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a></td>

            <td>
                <form action="{{action('ProductController@destroy', $post['id'])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete <span class="glyphicon glyphicon-trash"></span></button>
              </form>
            </td>
          </tr>
            @endforeach



      </tbody>
    </table>

    {{$product->links() }}

  </div>
  </div>
  </div>
  </div>
         
             
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>