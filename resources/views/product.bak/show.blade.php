
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">View Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">View Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >


       <div class="form-group row">
                    
                      <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/main_product/'.$product->main_image) }}" alt="{{$product->brand_image}}" class="img-responsive"/> 
                        </div>
                  
                    </div>


      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" name="p_name" value="{{$product->p_name}}">
          @if ($errors->has('p_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_name') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Release Date</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="release_date" name="release_date" value="{{$product->release_date}}">
           @if ($errors->has('release_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('release_date') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      
      <div class="form-group row">
       <div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Brand Name</label>
        <div class="col-sm-5">

          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="brand_id" name="brand_id" value="{{$product->product_brand->brand_name}}">
            @if ($errors->has('brand_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_id') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>

       <div class="form-group row">
       <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product Category</label>
        <div class="col-sm-5">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="category_id" name="category_id" value="{{$product->product_category->category_product}}">

            @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
        
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Gender</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="gender" name="gender" value="{{$product->gender}}">

            @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>


      <div class="form-group row">
       <div class="form-group {{ $errors->has('mat_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Material Name</label>
        <div class="col-sm-5">
           <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="mat_id" name="mat_id" value="{{$product->product_mat->material_name}}">

            @if ($errors->has('mat_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mat_id') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('escription') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
        <div class="col-sm-5">
          <textarea name="description" rows="8" cols="80" readonly>{{$product->description}}</textarea>
          @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
        <div class="col-md-2"></div>
        <a href="{{action('ProductController@edit', $product['id'])}}" class="btn btn-primary">Edit <span class="glyphicon glyphicon-pencil"></span></a>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>

      


  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>