
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Product</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

    <form method="post" action="{{action('ProductController@update', $id)}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}

     <div class="form-group row">
                    
                      <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/main_product/'.$product->main_image) }}" alt="{{$product->brand_image}}" class="img-responsive"/> 
                        </div>
                  
                    </div>
     

      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" name="p_name" value="{{$product->p_name}}">
          @if ($errors->has('p_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_name') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Release Date</label>
        <div class="col-sm-5">
          <input type="date" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="release_date" name="release_date" value="{{$product->release_date}}">
           @if ($errors->has('release_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('release_date') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      
      <div class="form-group row">
       <div class="form-group {{ $errors->has('brand_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Brand Name</label>
        <div class="col-sm-5">
          <div class="dropdown">
            <select id="brand_id" name="brand_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->brand_id}}">{{$product->product_brand->brand_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($brand as $post)
                <option value="{{$post['id']}}">{{$post['brand_name']}}</option>
                @endforeach      
              </ul>
            </select>
            @if ($errors->has('brand_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>

       <div class="form-group row">
       <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product Category</label>
        <div class="col-sm-5">
          <div class="dropdown">
            <select id="category_id" name="category_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->category_id}}">{{$product->product_category->category_product}}</option>
                <div class="dropdown-divider"></div>
                @foreach($category as $post2)
                <option value="{{$post2['id']}}">{{$post2['category_product']}}</option>
                @endforeach      
              </ul>
            </select>
            @if ($errors->has('category_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>

      <div class="form-group row">
      <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Gender</label>
        <div class="col-sm-5">
          <div class="dropdown">
            <select id="brand_id" name="gender" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->gender}}">{{$product->gender}}</option>
                <div class="dropdown-divider"></div>
                <option value="1">Male</option>
                <option value="2">Female</option>
              </ul>
            </select>
            @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif

          </div>
        </div>
      </div>
      </div>


      <div class="form-group row">
       <div class="form-group {{ $errors->has('mat_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Material Name</label>
        <div class="col-sm-5">
          <div class="dropdown">
            <select id="mat_id" name="mat_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$product->mat_id}}">{{$product->product_mat->material_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($mat as $post4)
                <option value="{{$post4['id']}}">{{$post4['material_name']}}</option>
                @endforeach      
              </ul>
            </select>
            @if ($errors->has('mat_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mat_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('escription') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Description</label>
        <div class="col-sm-5">
          <textarea name="description" rows="8" cols="80">{{$product->description}}</textarea>
          @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                @endif
        </div>
      </div>
      </div>


      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>