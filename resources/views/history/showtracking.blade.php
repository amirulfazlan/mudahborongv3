<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="/shopping_catalog">Home</a>
                    <a href="/orderhistory">Order History</a>
                    <a href="#">Tracking Detail</a> 
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 col-sm-push-3">
                            <div class="wishlist-header hidden-xs">
                                <div class="title-1">Product</div>
                                <div class="title-2">Tracking</div>
                            </div>

                            <div class="wishlist-box">

                               
                                <div class="wishlist-entry">

                                    <div class="column-1">

                                        <div class="traditional-cart-entry">
                                        
                                        @foreach($tracking as $post)
                                         
                                         @foreach($json_decode as $product) 
                                         @if($product->rowId == $post->item_id )
                                        
                                         @foreach ($pro_var as $var)
                                         @if ( $var['p_id'] == $product->id)
                                         @if ( $var['p_color'] == $product->options->color)
                                            <a class="image" href="{{action('ShoppingCatalog_Controller@show', $var['p_id'])}}"><img alt="" src="{{ asset ('images/product/'.$var['main_image']) }}" /></a>
                                         @endif 
                                         @endif
                                         @endforeach 
                                        
                                            <div class="content">
                                                <div class="cell-view">
                                                   
                                                    <a class="title" href="#">{{$product->name }}</a>
                                                    <div class="inline-description">{{$product->options->size }}</div>
                                                    <div class="inline-description">{{$product->options->color }}</div>
                                                </div>
                                            </div>

                                        

                                        </div>

                                       
                                    </div>

                                    <div class="column-2 col-sm-pull-9">

                                         <a class="title size-1 pull-left" href="#" style="color: black"><b>Tracking No: {{$post->tracking_no }}</b></a></br></br>
                                       <div class="inline-description pull-left"> <b>Weight : {{$post->weight }}</b></div></br></br>
                                        <div class="inline-description pull-left"><b>Shipping Price : {{$post->ship_price }}</b></div>

                                        
                                    </div>

                                    @endif
                                    @endforeach

                                    @endforeach
                                    
                                </div>
                                


                            </div>

                            

                        </div>

                        </br>
                            </br>

                          <div class="col-sm-3 col-sm-pull-9 blog-sidebar">
                                <div class="information-blocks">
                                    <div class="categories-list account-links">
                                        <div class="block-title size-3">Tracking Detail</div> 
                                            <div class="block-title size-1">Order No : {{  $order['id'] }} </div>
                    
                                        
                                    </div>
                                    <div class="article-container">
                                        
                                    </div>
                                </div>
                            </div>

                        
                    </div>
                </div>
               
                 

                 <!-- FOOTER -->
                 @include('layouts.mango.layout.footer')
                 <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">  
                        <img class="gallery-image" src="{{ asset ('mango/img/portfolio-1.jpg') }}" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      @include('layouts.mango.layout.searchbox_popup')


    @include('layouts.mango.layout.cart_popup_item')

   
   @include('layouts.mango.layout.scripts')

   
</body>
</html>
