<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="/orderhistory">Order History </a>
                    
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 col-sm-push-3">
                            <div class="wishlist-header hidden-xs">
                                <div class="title-1">Date</div>
                                <div class="title-2">Product</div>
                            </div>

                            <div class="wishlist-box">

                               

                                @foreach($order as $history)


                                <div class="wishlist-entry">

                                    <div class="column-1">

                                        <div class="traditional-cart-entry">
                                            
                                            <div class="content">
                                                <div class="cell-view">
                                                    <a class="title" href="#">Order No : {{  $history['id'] }}</a>
                                                     <a class="title" href="#">{{  $history['created_at'] }}</a>
                                                     <!-- <div class="inline-description">Status Shipping : {{  $history['status'] }}</div> -->
                                                     <div class="inline-description">Payment : RM {{  $history['grand_total'] }}</div></br>
                                                     
                                                     
                                                   
                                                </div>
                                            </div>
                                        </div>

                                       
                                    </div>

                                    <div class="column-2">
                                        <div class="traditional-cart-entry">
                                                
                                           <a class="button style-14" href="{{action('HistoryController@show', $history['id'])}}">View Order</a>  

                                           <a class="button style-14" href="{{action('HistoryController@showtracking', $history['id'])}}">View Tracking</a>  

                                        </div>
                                    </div>
                                    
                                </div>


                               
                                @endforeach
                               
                                


                            </div>

                            

                        </div>

                        </br>
                            </br>

                          <div class="col-sm-3 col-sm-pull-9 blog-sidebar">
                                <div class="information-blocks">
                                    <div class="categories-list account-links">
                                        <div class="block-title size-3">Order History</div>
                                        
                                    </div>
                                    <div class="article-container">
                                        
                                    </div>
                                </div>
                            </div>

                        
                    </div>
                </div>
               
                 

                 <!-- FOOTER -->
                 @include('layouts.mango.layout.footer')
                 <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">  
                        <img class="gallery-image" src="{{ asset ('mango/img/portfolio-1.jpg') }}" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      @include('layouts.mango.layout.searchbox_popup')


    @include('layouts.mango.layout.cart_popup_item')

   
   @include('layouts.mango.layout.scripts')

   
</body>
</html>
