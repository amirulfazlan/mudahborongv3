<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

 <link href="{{ asset ('bundle/demo/html/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset ('bundle/demo/html/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
   
    
    <link href="{{ asset ('bundle/demo/html/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
   
    <link href="{{ asset ('bundle/demo/html/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    
    <link class="main-stylesheet" href="{{ asset ('bundle/demo/html/pages/css/pages.css')}}" rel="stylesheet" type="text/css" /> 



<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
        
             <!-- HEADER -->

            @include('layouts.mango.layout.header')

            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                     <a href="/shopping_catalog">Home</a>
                        <a href="/orderhistory">Order History</a>
                        <a href="#">View Order</a></br></br></br>

                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="invoice padding-50 sm-padding-20">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{ asset ('mango/img/logo-9.png') }}" data-src="{{ asset ('mango/img/logo-9.png') }}" src="{{ asset ('mango/img/logo-9.png') }}" style="width: 195px; height: 30px">
                      <address class="m-t-10">
                                      MudahBorong Online
                                      <br>(03) XXXXXXXXX.
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Order History</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="container-sm-height">
                    <div class="row-sm-height">
                      <div class="col-md-9 col-sm-height sm-no-padding">
                        <!-- <p class="small no-margin">Tracking Details : </p> -->

                            <p class="small no-margin">Shipping Address : </p>
                        </br>

                        <form method="post" action="{{url('/bayarshipping')}}">
                        {{ csrf_field() }}

                        <div>


                         <!--  <div class="pull-left font-montserrat bold all-caps">Tracking ID :</div>
                          <div>  &nbsp; {{  $order->track_num['id'] }}</div>
                          <input type="hidden" name="track_id"  value="{{  $order->track_num['id'] }}">
                          <div class="clearfix"></div>
                        </div>
                        </br>


                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Tracking Number :</div>
                          <div>  &nbsp; {{  $order->track_num['tracking_num'] }}</div>
                          <div class="clearfix"></div>
                        </div>
                        </br>


                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Parsel Weight :</div>
                          <div>  &nbsp; {{  $order->track_num['berat'] }} kg</div>
                          <div class="clearfix"></div>
                        </div>
                        </br>


                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Shipping Price :</div>
                          <div>  &nbsp; RM {{  $order->track_num['harga'] }}</div>
                          <div class="clearfix"></div>
                        </div>
 -->


                         
                    
                                  <!--  <div class="pull-right">
                                    <a href="{{url('/print')}}" type ="submit" class="btn btn-primary"><i class="fa fa-chevron-right"></i> Proceed to Checkout</a>
                                </div> -->

                      </div>
                      <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                        <br>

                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Order Number : </div>
                          <div class="pull-left">&nbsp;{{$order['id']}}</div>
                          <input type="hidden" name="order_id"  value="{{$order['id']}}">
                          <div class="clearfix"></div>
                        </div>

                        </br>


                         <div>
                          <div class="pull-left font-montserrat bold all-caps">Order Dates : </div>
                          <div class="pull-left">&nbsp; {{  $order['created_at'] }}</div>
                          <div class="clearfix"></div>
                        </div>

                        </br>


                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Status Shipping :  </div>
                          <div class="pull-left">&nbsp; {{  $order['status'] }}</div>
                          <div class="clearfix"></div>
                        </div>

                        </br>


                       <!--  <div>     
                          <div class="pull-left"><button type="submit" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>  &nbsp; Bayar </button></div>
                          <div class="clearfix"></div>
                        </div>
 -->
                         </form>

                        
                      </div>
                    </div>
                  </div>
                  </br>


                   


                  </div>

                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="">Product Image</th>
                          <th class="">Product Name</th>
                          <th class="text-center">Size</th>
                          <th class="text-center">Color</th>
                          <th class="text-center">Quantity</th>
                         
                          
                        
                        </tr>

                      </thead>
                      <tbody>

                       <?php $i = 0 ?>
                        @foreach($json_decode as $content)
                       <?php $i++ ?>

                        

                        <tr>
                          <td class="text-center">{{ $i}}</td>

                          @foreach ($pro_var as $var)
                          @if ( $var['p_id'] == $content->id)
                          @if ( $var['p_color'] == $content->options->color)
                          
                          <td class="v-align-middle">

                           <div class="row" id="thumbs">

                              <img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" height="100" width="100">

                          </div> 
                            
                          </td>

                          @endif 
                          @endif
                          @endforeach 

                          <td class="">
                            <p class="text-black">{{  $content->name }}</p>
                          </td>
                          <td class="text-center">{{  $content->options->size }}</td>
                          <td class="text-center">{{  $content->options->color }}</td>
                          <td class="text-center">{{  $content->qty }}</td>
                          
                        </tr>

                         @endforeach

                       

                      </tbody>
                    </table>
                  </div>
                  <br>
                  <br>
                  

                  <br>
                
                 
                  <div class="container-sm-height">
                    <div class="row row-sm-height b-a b-grey">
                      <div class="col-sm-2 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 col-sm-height col-middle clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 text-right text-sm-center bg-menu col-sm-height padding-15 sm-p-b-15 ">
                        <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold col-xs-20 col-md-6">Payment : </h5>
                        <h1 class="no-margin text-white col-xs-20 col-md-6"> RM {{  $order['grand_total'] }}</h1>
                      </div>
                    </div>
                  </div>



                
                  <br>
                  <hr>
                  <div class="sm-padding-20">
                    <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png')}}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png')}}" width="78" height="22">

                    <span class="m-l-70 text-black sm-pull-right">(03) XXXXXXXX</span>
                    <span class="m-l-40 text-black sm-pull-right">mudahborong@gmail.com</span>

                  </div>
                </div>
                         
                         
                    </div>
                </div>

                <!-- FOOTER -->
                 @include('layouts.mango.layout.footer')
                 <!-- FOOTER -->

               
            </div>

        </div>
        <div class="clear"></div> 

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('layouts.mango.layout.searchbox_popup')


    @include('layouts.mango.layout.cart_popup_item')


   
   @include('layouts.mango.layout.scripts')

   


</body>
</html>
