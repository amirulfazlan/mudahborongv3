<!DOCTYPE html>
<html>

  @include('layouts.head')

  <!-- Product Catalog -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset ('p_catalog/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset ('p_catalog/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset ('p_catalog/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset ('p_catalog/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel-heading">     

                        </div>

            <div id="all">

        <div id="content">
            <div class="container-fluid">

                <div class="col-md-9">
                    <ul class="breadcrumb">
                        <li><p><a href="{{url('/')}}">MudahBorong</a></p>
                        </li>
                        <!-- <li><a href="#">Ladies</a>
                        </li>
                        <li><a href="#">Tops</a>
                        </li> -->
                        <li>{{ $pro_var->product_name['p_name'] }}</li>
                    </ul>

                </div>

               

                <div class="col-md-9">

                    <div class="row" id="productMain">
                        <div class="col-sm-6">
                            <div id="mainImage">
                                <img src="{{ asset ('images/product/'.$pro_var['main_image']) }}" alt="{{ $pro_var['main_image'] }}" class="img-responsive">
                            </div>

                            <!-- <div class="ribbon sale">
                                <div class="theribbon">SALE</div>
                                <div class="ribbon-background"></div>
                            </div> -->
                            <!-- /.ribbon -->

                            <!-- <div class="ribbon new">
                                <div class="theribbon">NEW</div>
                                <div class="ribbon-background"></div>
                            </div> -->
                            <!-- /.ribbon -->

                        </div>
                        <div class="col-sm-6">
                            <div class="box">
                                <h1 class="text-center">{{ $pro_var->product_name['p_name'] }}</h1>
                                <h1 class="text-center">Color : {{ $pro_var['p_color'] }}</h1>
                                <p class="goToDescription"><a href="#details" class="scroll-to">Scroll to product details, material & care and sizing</a>
                                </p>
                                 <p class="price">Starting From RM 




                                 </p>





                                 <div class="panel-heading"></div>  

                                 <h4 class="text">Type of Size : {{ $pro_var->sz_type['size_name'] }}</h4>

                                 <form action="/addProduct" method="POST" >
                                    {{ csrf_field() }}

                                  <input type="hidden" name="id"  value="<?php echo $id; ?>">
                                
                                 <div class="form-group row">
                                 <div class="form-group {{ $errors->has('cart_size') ? ' has-error' : '' }}">
                                 <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size</label>
                                 <div class="col-sm-5">
                                 <div class="dropdown">
                                 <select id="cart_size" name="cart_size" class="form-control" required>
                                 <span class="caret"></span>
                                 <ul class="dropdown-menu">
                                 <option value="">Choose Size </option>
                                 <div class="dropdown-divider"></div>

                                 @foreach($size_item as $post2)
                                 @if( $post2['st_id'] == $pro_var['st_id'] ) 
                                 <option value="{{$post2['st_item']}}">{{$post2['st_item']}}</option>
                                 @endif
                                 @endforeach
                                 </ul>
                                 </select>
                                 @if ($errors->has('cart_size'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('cart_size') }}</strong>
                                 </span>
                                 @endif
                                 </div>
                                 </div>
                                 </div>
                                 </div>

                                <div class="form-group row">
                                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Quantity</label>
                                <div class="col-sm-5">
                                <input type="number" name="cart_quantity" class="form-control" min="5">
                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif
                                </div>
                                </div>
                                </div>



                                

                                 <p class="text-center buttons">
                                   
                                    <button type="submit" class="btn btn-primary ><i class="fa fa-shopping-cart"></i> Add to cart</button> 
                                    
                                </p>
                                

                              </form>
                                 






                            </div>

                            <div class="row" id="thumbs">
                                <div class="col-xs-4">
                                    <a href="{{ asset ('images/product/'.$pro_var['main_image'])}}" class="thumb">
                                        <img src="{{ asset ('images/product/'.$pro_var['main_image'])}}" alt="{{ $pro_var['main_image'] }}" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="{{ asset ('images/product/'.$pro_var['back_image'])}}" class="thumb">
                                        <img src="{{ asset ('images/product/'.$pro_var['back_image'])}}" alt="{{ $pro_var['back_image'] }}" class="img-responsive">
                                    </a>
                                </div>
                                
                            </div>
                        </div>

                    </div>


                    <div class="box" id="details">
                        <p>
                            <h4>Product SKU</h4>
                            <p>{{ $pro_var['sku'] }}</p>
                            <h4>Product details</h4>
                            <p>{{ $pro_var->product_name['description'] }}</p>
                            
                            <h4>Size & Fit</h4>
                            <ul>
                                <li>Type of Size: {{ $pro_var->sz_type['size_name'] }}</li> 

                                <li> Size Item:  @foreach ($size_item as $item)</li>


                                    @if( $item['st_id'] == $pro_var['st_id'] ) 

                                    {{ $item['st_item'] }} 
                                    @endif 
                                    @endforeach
                                    

                                   



                                


                            </ul>


                            <h4>Product Price details</h4>
                            <ul>
                                Based on Moq: {{ $pro_var->moqtype['moq_name'] }} 

                                Price Based On Quantity:   
                                   </li>    

                            </ul>

                            <div class="pull-left">
                            <a href="category.html" class="btn btn-primary btn-lg"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                            </div>

                            <hr>

                           <div class="panel-heading">     

                        </div>
 
                            
                    </div>

                   

                    
                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>




            <!-- END PLACE PAGE CONTENT HERE -->
        
          <!-- END CONTAINER FLUID -->
           </div>  
  

        <!-- END PAGE CONTENT -->

        <div class="panel-heading">    </div>
       
        

                       

       @include('layouts.footer')

      @include('layouts.leftsidebar')
  
</div>
      @include('layouts.scripts')

    <script src="{{ asset ('p_catalog/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/waypoints.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/modernizr.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/front.js') }}"></script>


  </body>
</html>
