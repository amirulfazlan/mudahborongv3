
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Insert Product Variation</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Insert New Product Variation</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

              <form action="/add_pvariation" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}



             <div class="form-group row">
             <div class="form-group {{ $errors->has('p_id') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Product Name</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="brand_id" name="p_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Product</option>
                <option value="1">Bar Logo Tee</option>
                <option value="2">Daisy Roll Sleeve Blouse</option>
               
              </ul>
            </select>
            </div>
             @if ($errors->has('p_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


            <div class="form-group row">
                    <div class="form-group {{ $errors->has('p_mainimage') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Main image</label>
                        <div class="col-sm-5">
                          <input type="file" name="p_mainimage" class="form-control">
                          @if ($errors->has('p_mainimage'))
                          <span class="help-block">
                          <strong>{{ $errors->first('p_mainimage') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>


                     <div class="form-group row">
                    <div class="form-group {{ $errors->has('p_backimage') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Back image</label>
                        <div class="col-sm-5">
                          <input type="file" name="p_backimage" class="form-control">
                          @if ($errors->has('p_backimage'))
                          <span class="help-block">
                          <strong>{{ $errors->first('p_backimage') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>


            <div class="form-group row">
             <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Product Color</label>
             <div class="col-sm-5">
             <div class="dropdown">
             <select id="color" name="color" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Color</option>
                <option value="1">Red</option>
                <option value="2">Blue</option>
              </ul>
            </select>
            </div>
             @if ($errors->has('color'))
                  <span class="help-block">
                      <strong>{{ $errors->first('color') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


            <div class="form-group row">
              <div class="form-group {{ $errors->has('sku') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">SKU</label>
              <div class="col-sm-2">
              <input type="text" name="sku" class="form-control">
              @if ($errors->has('sku'))
                  <span class="help-block">
                      <strong>{{ $errors->first('sku') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

            <div class="form-group row">
            <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
            <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Price</label>
            <div class="col-sm-5">
            <div class="input-group">
            <span class="input-group-addon">RM</span>
            <input name="price" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Price" required>
            <span class="input-group-addon">.00</span>
            </div>
            @if ($errors->has('price'))
                  <span class="help-block">
                      <strong>{{ $errors->first('price') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


             


      <center><input type="submit" name="submit" value = "Create Product"  class="btn btn-primary"></center>
      </form>

    <div class="panel-heading"> </div>

  </div>
  </div>
  </div>
  </div>
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>