<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<body class="style-21">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="wide-center fixed-header-margin">
           
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->

            <div class="content-push">

              <div class="navigation-banner-swiper">
                    <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide active" data-val="0"> 
                                <div class="navigation-banner-wrapper light-text align-3" style="background-image: url(mango/img/main1.jpg); background-position: center center;">
                                    <div class="navigation-banner-content">
                                        <div class="cell-view">
                                            <h2 class="subtitle">Most Wanted Footware</h2>
                                            <h1 class="title">Men & Women Shoes</h1>
                                            <div class="description">Mudahborong your shopping destination for fashion online. We offer fashion and quality at the best price in a more sustainable way.</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="swiper-slide" data-val="1"> 
                                <div class="navigation-banner-wrapper light-text align-3" style="background-image: url(mango/img/main3.jpg); background-position: center center;">
                                    <div class="navigation-banner-content">
                                        <div class="cell-view">
                                            <h2 class="subtitle">Latest Fashion</h2>
                                            <h1 class="title">Men & Women Clothing</h1>
                                            <div class="description">Offering an extensive collection of top international brands.</div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="pagination"></div>
                    </div>
                </div>

                  <div class="mozaic-banners-wrapper type-2">
                    <div class="row">
                        <div class="banner-column col-sm-6">
                            <a style="background-image: url(mango/img/forhim.jpg); background-size: cover; background-position: right top;" class="mozaic-banner-entry type-3" href="/shopping_catalog?gender_id=1">
                                <span class="mozaic-banner-content">
                                    <span class="subtitle">Collection</span>
                                    <span class="title">For Him</span>
                                    <span class="view">Show More</span>
                                </span>
                            </a>
                        </div>
                        <div class="banner-column col-sm-6">
                            <a style="background-image: url(mango/img/forher.jpg); background-size: cover; background-position: right top;" class="mozaic-banner-entry type-3" href="/shopping_catalog?gender_id=2">
                                <span class="mozaic-banner-content">
                                    <span class="subtitle">Collection</span>
                                    <span class="title">For Her</span>
                                    <span class="view">Show More</span>
                                </span>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>



                <div class="products-slider-banner">

                            <a style="background-image: url(mango/img/menshoes.jpg); background-color: #235290;" class="promo-banner-box" href="/shopping_catalog?category_id=5">
                                <span class="promo-text">
                                    <span class="title" style="color: black">Men Shoes</span>
                                    <span class="description" style="color: black">Sneaker/ <br/> Sandal </span>
                                    <span class="detail-link" style="color: black">view products</span>
                                </span>
                            </a>

                            <div class="products-swiper">
                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="5">
                                    <div class="swiper-wrapper">

                                         <?php  $count = 0;  ?>

                                          @foreach ($shoes as $men_shoes)
                                          <?php 
                                          if($count == 8) break; 
                                          ?>                             
                                       
                                        <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="{{ asset ('images/main_product/'.$men_shoes->main_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <img src="{{ asset ('images/main_product/'.$men_shoes->back_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <a href="{{action('ShoppingCatalog_Controller@show', $men_shoes->id)}}" class="top-line-a right"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                            
                                                        </div>
                                                        <a class="tag" href="#">
                                                        @if ($men_shoes->gender_id == 1)
                                                          Men
                                                        @else
                                                          Women
                                                        @endif

                                                        @foreach ($cat as $cats)

                                                        @if ($men_shoes->category_id == $cats['id'])
                                                        {{ $cats['category_product']}}

                                                        @endif

                                                        @endforeach

                                                       
                                                        </a>



                                                        <a class="title" href="{{action('ShoppingCatalog_Controller@show', $men_shoes->id)}}">{{ $men_shoes->p_name}} </a>
                                                       
                                                        <div class="price">
                                                        @foreach ($cur as $yuan)

                                                        @foreach ($margin as $mar)
 
                                                        @if( $mar['id'] == $men_shoes->margin_id )

                                                        @foreach ($moq_item as $moq)

                                                        @if( $moq['moqty_id'] == $men_shoes->moqty_id )
       
                                                        <?php $admin_price = ( $men_shoes->base_price2 +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                                         $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                                         $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                                         $bundar = ceil($whole_price * 2 ) / 2
                                                         ?>
                                                            <div class="current">RM {{ number_format($bundar, 2) }}</div>

                                                         @endif 
                                                         @endforeach 
       
                                                         @endif 
       
                                                         @endforeach
     
                                                         @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                         <?php $count++; ?>

                                         @endforeach
                                            
                                    </div>
                                    <div class="pagination"></div>
                                </div>
                            </div>
                   
                </div>

                <div class="products-slider-banner">

                            <a style="background-image: url(mango/img/mencloth.jpg); background-color: #383c43;" class="promo-banner-box" href="/shopping_catalog?gender_id=1">
                                <span class="promo-text">
                                    <span class="title">Men Clothes</span>
                                    <span class="description" >Tshirt <br/> Pants</span>
                                    <span class="detail-link" style="color: black">view products</span>
                                </span>
                            </a>

                            <div class="products-swiper">
                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="5">
                                    <div class="swiper-wrapper">
                                       
                                     <?php  $count2 = 0;  ?>

                                          @foreach ($men_cloth as $men_all)
                                          
                                          <?php 
                                          if($count2 == 8) break; 
                                          ?>                             
                                       
                                        <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="{{ asset ('images/main_product/'.$men_all->main_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <img src="{{ asset ('images/main_product/'.$men_all->back_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <a href="{{action('ShoppingCatalog_Controller@show', $men_all->id)}}" class="top-line-a right"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                          
                                                        </div>
                                                        <a class="tag" href="#">
                                                        @if ($men_all->gender_id == 1)
                                                          Men
                                                        @else
                                                          Women
                                                        @endif

                                                        @foreach ($cat as $cats)

                                                        @if ($men_all->category_id == $cats['id'])
                                                        {{ $cats['category_product']}}

                                                        @endif

                                                        @endforeach

                                                       
                                                        </a>



                                                        <a class="title" href="{{action('ShoppingCatalog_Controller@show', $men_all->id)}}">{{ $men_all->p_name}} </a>
                                                       
                                                        <div class="price">
                                                        @foreach ($cur as $yuan)

                                                        @foreach ($margin as $mar)
 
                                                        @if( $mar['id'] == $men_all->margin_id )

                                                        @foreach ($moq_item as $moq)

                                                        @if( $moq['moqty_id'] == $men_all->moqty_id )
       
                                                        <?php $admin_price = ( $men_all->base_price2 +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                                         $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                                         $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                                         $bundar = ceil($whole_price * 2 ) / 2
                                                         ?>
                                                            <div class="current">RM {{ number_format($bundar, 2) }}</div>

                                                         @endif 
                                                         @endforeach 
       
                                                         @endif 
       
                                                         @endforeach
     
                                                         @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                         <?php $count2++; ?>

                                         @endforeach
                                            
                                        
                                     
                                    </div>
                                    <div class="pagination"></div>
                                </div>
                            </div>
                   
                </div>

                <div class="products-slider-banner">

                            <a style="background-image: url(mango/img/womencloth.jpg); background-color: #eb2969;" class="promo-banner-box" href="/shopping_catalog?gender_id=2">
                                <span class="promo-text">
                                    <span class="title" style="color: black">Women Clothes</span>
                                    <span class="description" style="color: black">Tshirt <br/> Pants</span>
                                    <span class="detail-link">view products</span>
                                </span>
                            </a>

                            <div class="products-swiper">
                                <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="5">
                                    <div class="swiper-wrapper">
                                        
                                        <?php  $count3 = 0;  ?>

                                          @foreach ($women_cloth as $women_all)
                                          
                                          <?php 
                                          if($count3 == 8) break; 
                                          ?>                             
                                       
                                        <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="{{ asset ('images/main_product/'.$women_all->main_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <img src="{{ asset ('images/main_product/'.$women_all->back_image) }}" alt="" style="width: 160px; height: 160px" />
                                                            <a href="{{action('ShoppingCatalog_Controller@show', $women_all->id)}}" class="top-line-a right"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                          
                                                        </div>
                                                        <a class="tag" href="#">
                                                        @if ($women_all->gender_id == 1)
                                                          Men
                                                        @else
                                                          Women
                                                        @endif

                                                        @foreach ($cat as $cats)

                                                        @if ($women_all->category_id == $cats['id'])
                                                        {{ $cats['category_product']}}

                                                        @endif

                                                        @endforeach

                                                       
                                                        </a>



                                                        <a class="title" href="{{action('ShoppingCatalog_Controller@show', $women_all->id)}}">{{ $women_all->p_name}} </a>
                                                       
                                                        <div class="price">
                                                        @foreach ($cur as $yuan)

                                                        @foreach ($margin as $mar)
 
                                                        @if( $mar['id'] == $women_all->margin_id )

                                                        @foreach ($moq_item as $moq)

                                                        @if( $moq['moqty_id'] == $women_all->moqty_id )
       
                                                        <?php $admin_price = ( $women_all->base_price2 +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                                         $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                                         $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                                         $bundar = ceil($whole_price * 2 ) / 2
                                                         ?>
                                                            <div class="current">RM {{ number_format($bundar, 2) }}</div>

                                                         @endif 
                                                         @endforeach 
       
                                                         @endif 
       
                                                         @endforeach
     
                                                         @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                         <?php $count3++; ?>

                                         @endforeach
                                       
                                    
                                        
                                    </div>
                                    <div class="pagination"></div>
                                </div>
                            </div>
                   
                </div>


                <div class="clear"></div>

            </div>

        </div>
         <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

         <!-- FOOTER -->
        

    </div>

    @include('layouts.mango.layout.searchbox_popup')


     @include('layouts.mango.layout.cart_popup_item')

     @include('layouts.mango.layout.scripts')

</body>
</html>
