
<!DOCTYPE html> 
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="/admin-orderhistory" class="active">Add Tracking</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Add Tracking</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

     <form action="/tracking" method="POST"  enctype="multipart/form-data">

      {!! csrf_field() !!}

   
      <div class="form-group row">
              <div class="form-group {{ $errors->has('order_id') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Order Id</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="order_id" name="order_id" value="{{$pesanan['id']}}" > 
              @if ($errors->has('order_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('order_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">user Name</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput"  value="{{$pesanan->wh_name['name']}}" > 
              @if ($errors->has('user_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('user_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 


            <div class="form-group row">
              <div class="form-group {{ $errors->has('tracking_num') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Tracking Number</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="tracking_num" name="tracking_num" > 
              @if ($errors->has('tracking_num'))
                  <span class="help-block">
                      <strong>{{ $errors->first('tracking_num') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 


            <div class="form-group row">
              <div class="form-group {{ $errors->has('berat') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Weight Parsel</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="berat" name="berat" > 
              @if ($errors->has('berat'))
                  <span class="help-block">
                      <strong>{{ $errors->first('berat') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('harga') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Price Shipping</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="harga" name="harga" > 
              @if ($errors->has('harga'))
                  <span class="help-block">
                      <strong>{{ $errors->first('harga') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 



      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Add</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
    
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>