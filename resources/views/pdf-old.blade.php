<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong>ORDER LIST</strong></p>
<p style="text-align: left;"><strong>Order Number : {{  $order->id }} </strong></p>
<p style="text-align: left;">Customer Name : <span style="color: #000080;"><strong>{{  $address->firstname }} {{  $address->lastname }}</strong></span></p>
<p style="text-align: left;">Phone Number : <span style="color: #000080;"><strong>{{  $address->phone }} </strong></span></p>
<p style="text-align: left;">Preferred Postage Services : <span style="color: #000080;"><strong>{{  $address->delivery_method }} </strong></span></p>
<p style="text-align: left;">Postage Address :&nbsp; <span style="color: #000080;"><br><strong>
                                              {{$address->street}} , <br>
                                              {{$address->postcode}} , {{$address->city}}<br>
                                              {{$address->state}}, <br> 
                                              {{$address->country}}</strong></span></p>
<hr />
<p style="text-align: left;">&nbsp;</p>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Product Image</th>
      <th scope="col">Product Name</th>
      <th scope="col">Size</th>
      <th scope="col">Color</th>
      <th scope="col">Quantity</th>
    </tr>
  </thead>

  <tbody>
    <?php $i = 0 ?>
    @foreach($json_decode as $content)
    <?php $i++ ?>

    <tr>
      <th scope="row">{{ $i}}</th>
      @foreach ($pro_var as $var)
      @if ( $var['p_id'] == $content->id)
         @if ( $var['p_color'] == $content->options->color)
            <td>                           
              <div class="row" id="thumbs">
                <img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" height="100" width="100">
              </div>
            </td>
          @endif 
        @endif
      @endforeach 


      <td>{{  $content->name }}</td>
      <td>{{  $content->options->size }}</td>
      <td>{{  $content->options->color }}</td>
      <td>{{  $content->qty }}</td>
    </tr>

    @endforeach


  </tbody>
</table>
<hr />

<p style="text-align: center;"><strong>Price Breakdown</strong></p>

<table class = "table">
  <thead class="thead-dark">
    <tr>

      <th scope="col">No</th>
      <th scope="col">Product Name</th>
      <th scope="col">Quantity</th>
      <th scope="col">Subprice</th>
      <th scope="col">Total</th>

    </tr>

  </thead>

  <tbody>

    <?php $x = 0;
    $totalharga = 0; 

    ?> 
    @foreach($all_new_output as $group)
                       
     <?php $x++ ;

     $admin_round= round($group['admin'] ,1,PHP_ROUND_HALF_UP) ;

     $sub = $admin_round * $group['total_qt'] ;

     $totalharga += $sub;

     ?>

    <tr>

      <td>{{ $x}}</td>
      <td>{{  $group['name_item'] }}</td>
      <td>{{  $group['total_qt'] }}</td>
      <td>RM  {{ number_format($admin_round, 2) }}</td>
      <td>RM  {{ number_format($sub, 2) }}</td>

    </tr>
    @endforeach

      <td><strong>TOTAL AMOUNT FOR THIS ORDER : RM  {{ number_format($totalharga, 2) }}</strong></td>

  </tbody>
</table>





<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>