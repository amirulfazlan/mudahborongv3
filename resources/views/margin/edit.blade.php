
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Margin</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Margin</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

       <form method="post" action="{{action('MarginsController@update', $id)}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}
     
      <div class="form-group row">
      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Margin Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Name" name="name" value="{{$margin->name}}"> 
         
        </div>
      </div>
      </div>

       <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_admin1') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Admin MOQ 1</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Admin1" name="mar_admin1" value="{{$margin->mar_admin1}}">
            </div>
            </div>
            </div> 

            <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_admin2') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Admin MOQ 2</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Admin2" name="mar_admin2" value="{{$margin->mar_admin2}}">
            </div>
            </div>
            </div> 

            <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_admin3') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Admin MOQ 3</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Admin3" name="mar_admin3" value="{{$margin->mar_admin3}}">
            </div>
            </div>
            </div> 

       <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_whole1') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Wholesaler MOQ 1</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Wholesaler1" name="mar_whole1" value="{{$margin->mar_whole1}}">
            </div>
            </div>
            </div> 

            <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_whole2') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Wholesaler MOQ 2</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Wholesaler2" name="mar_whole2" value="{{$margin->mar_whole2}}">
            </div>
            </div>
            </div> 



            <div class="form-group row">
       <div class="form-group {{ $errors->has('mar_whole3') ? ' has-error' : '' }}">
          <label class="col-sm-2 control-label">Wholesaler MOQ 3</label>
          <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Wholesaler3" name="mar_whole3" value="{{$margin->mar_whole3}}">
            </div>
            </div>
            </div> 

            

      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>