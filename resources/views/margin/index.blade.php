
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Margin</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
           

           <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Margin <span class="semi-bold"></span></h5>

                    <div class="panel-heading">     

                        </div>
                    
                  </div>
                  <div class="modal-body">

               <form action="/margin" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}


              <div class="form-group row">
              <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Margin Name</label>
              <div class="col-sm-5">
              <input type="text" name="name" class="form-control">
            </div>
            </div>
            </div> 


            <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_admin1') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Admin MOQ 1</label>
              <div class="col-sm-5">
              <input type="text" name="mar_admin1" class="form-control">
            </div>
            </div>
            </div> 

             <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_admin2') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Admin MOQ 2</label>
              <div class="col-sm-5">
              <input type="text" name="mar_admin2" class="form-control">
            </div>
            </div>
            </div> 

             <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_admin3') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Admin MOQ 3</label>
              <div class="col-sm-5">
              <input type="text" name="mar_admin3" class="form-control">
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_whole1') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Wholesaler MOQ 1</label>
              <div class="col-sm-5">
              <input type="text" name="mar_whole1" class="form-control">
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_whole2') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Wholesaler MOQ 2</label>
              <div class="col-sm-5">
              <input type="text" name="mar_whole2" class="form-control">
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('mar_whole3') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Wholesaler MOQ 3</label>
              <div class="col-sm-5">
              <input type="text" name="mar_whole3" class="form-control">
            </div>
            </div>
            </div> 

            <div class="form-group row">
            <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right"> 
            <input type="submit" name="submit" value = "Create Margin"  class="btn btn-primary btn-block m-t-5 ">
            </div>
            </div>      

             </form>
    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

             <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">List Of Margin
                </div>
                <div class="btn-group pull-right m-b-10">
                <button class="btn btn-primary" id="btnToggleSlideUpSize" >Add New Margin</button>
                    
                 
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Margin</th>
                        <th style="width:15%">MOQ Admin1</th>
                        <th style="width:15%">MOQ Admin2</th>
                        <th style="width:15%">MOQ Admin3</th>
                        <th style="width:15%">MOQ Wsaler1</th>
                        <th style="width:15%">MOQ Wsaler2</th>
                        <th style="width:15%">MOQ Wsaler3</th>
                        
                        <th style="width:15%">Edit</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                       @foreach($margin as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{$post['name']}}</p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{$post['mar_admin1']}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{$post['mar_admin2']}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{$post['mar_admin3']}}</p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{$post['mar_whole1']}}</p>
                        </td>

                         <td class="v-align-middle">
                          <p>{{$post['mar_whole2']}}</p>
                        </td>

                         <td class="v-align-middle">
                          <p>{{$post['mar_whole3']}}</p>
                        </td>
                        
                        <td class="v-align-middle">

                         <a href="{{action('MarginsController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a>

                        </td>
                        <td class="v-align-middle">
                          <form action="{{action('MarginsController@destroy', $post['id'])}}" method="post">
                          {{csrf_field()}}
                           <input name="_method" type="hidden" value="DELETE">
                           <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                           </form>
                        </td>
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>
                </div>
              </div>
            </div>


          
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

     
    </div>
      @include('layouts.scripts')

  </body>
</html>