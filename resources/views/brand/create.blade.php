<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Brand</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


     <div class="col-lg-12">
                    <h1 class="page-header">Insert New Brand</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >
      

                

                {!! csrf_field() !!}


                <div class="form-group row">
                <div class="form-group {{ $errors->has('brand_name') ? ' has-error' : '' }}">
                  <label class="col-sm-2 control-label">Brand</label>
                    <div class="col-sm-5">
                
                    <input type="text" name="brand_name" class="form-control">

                    @if ($errors->has('brand_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('brand_name') }}</strong>
                    </span>
                    @endif

                  </div>
                </div> 
                </div>

                      <div class="form-group row">
                    <div class="form-group {{ $errors->has('brand_image') ? ' has-error' : '' }}">
                      <label class="col-sm-2 control-label">Brand Logo</label>
                        <div class="col-sm-5">
                          <input type="file" name="brand_image" class="form-control">
                          @if ($errors->has('brand_image'))
                          <span class="help-block">
                          <strong>{{ $errors->first('brand_image') }}</strong>
                          </span>
                          @endif
                        </div>
                    </div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </div>

                    <div class="panel-heading"> </div>

                  </form>

              </div>

            </div>

          </div>
      </div>


            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>