<!DOCTYPE html>
<html>

   @include('layouts.mango.layout.head')

    <link href="{{ asset ('bundle/demo/html/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset ('bundle/demo/html/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
   
    
    <link href="{{ asset ('bundle/demo/html/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
   
    <link href="{{ asset ('bundle/demo/html/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    
    <link class="main-stylesheet" href="{{ asset ('bundle/demo/html/pages/css/pages.css')}}" rel="stylesheet" type="text/css" /> 

    <style>
    @media print {
  #printPageButton {
    display: none;
  }
}


  </style>

  <script>
function myFunction() {
    window.print();
}
</script>


  <body class="style-10">

    <div id="loader-wrapper">
            <div class="bubbles">
                <div class="title">loading</div>
                <span></span>
                <span id="bubble2"></span>
                <span id="bubble3"></span>
            </div>
        </div>



  

  <div class="page-container ">



      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->


          <div id="printPageButton" class="padding-50 detail-info-entry">
            <a  href="/destroyItem" class="button style-10"><i class="fa fa-shopping-cart"></i> Back to Home</a> 
            <a class="button style-10" onclick="myFunction()"><i class="fa fa-print"></i> Print</a>                 
                                </div>
    
          <!-- END JUMBOTRON -->
          

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

             <div class="panel panel-default">
              <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{ asset ('mango/img/logo-9.png') }}" data-src="{{ asset ('mango/img/logo-9.png') }}" src="{{ asset ('mango/img/logo-9.png') }}" style="width: 195px; height: 30px">
                      <address class="m-t-10">
                                      MudahBorong Online
                                      <br>(03) XXXXXXXXX.
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Invoice</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="container-sm-height">
                    <div class="row-sm-height">
                      <div class="col-md-9 col-sm-height sm-no-padding">
                        <p class="small no-margin">Invoice to</p>

                     

                        <h5 class="semi-bold m-t-0">{{$address['firstname']}} {{$address['lastname']}}</h5>

                                          <address>
                                              <strong>{{$address->company}}</strong><br>
                                              {{$address->street}} , <br>
                                              {{$address->postcode}} , {{$address->city}}<br>
                                              {{$address->state}}, <br> 
                                              {{$address->country}}
                                          </address>

                     
                                   <!-- <div class="pull-right">
                                    <a href="{{url('/print')}}" type ="submit" class="btn btn-primary"><i class="fa fa-chevron-right"></i> Proceed to Checkout</a>
                                </div> -->

                      </div>
                      <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                        <br>
                       <!--  <div>
                          <div class="pull-left font-montserrat bold all-caps">Invoice No :</div>
                          <div class="pull-right">0047</div>
                          <div class="clearfix"></div>
                        </div> -->
                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Invoice date :</div>
                          <div class="pull-right">{{$date}}</div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br><br>  

                  <h5 class="font-montserrat all-caps small no-margin hint-text text-black bold">List of Product</h5>


                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          
                          <th class="">Product</th>
                          <th class="text-center">Size</th>
                          <th class="text-center">Color</th>
                          <th class="text-center">Quantity</th>
                        
                        </tr>

                      </thead>
                      <tbody>

                        <?php $total = 0; 

                         $i=0;

                          ?>
                       @foreach($cart as $cartItems) 

                        <?php $total += $cartItems->subtotal ; 
                              $sub = $cartItems->price * $cartItems->qty ;
                              $i++;

                        ?>

                        <tr>
                          <td class="text-center">{{ $i}}</td>




                          <td class="">
                            <p class="text-black">{{$cartItems->name}}</p>
                            
                          </td>

                          <td class="text-center">{{$cartItems->options->size}}</td>
                          <td class="text-center">{{$cartItems->options->color}}</td>
                          <td class="text-center">{{$cartItems->qty}}</td>
                          <!-- <td class="text-center">RM 0.00</td>
                          <td class="text-right">RM {{$cartItems->price}}</td>
                           <td class="text-right">RM <?php echo $sub; ?> </td> -->
                        </tr>

                        @endforeach


                      </tbody>
                    </table>
                  </div>

                  <br>
                  <br>

                  <h5 class="font-montserrat all-caps small no-margin hint-text text-black bold">Combine Product To Calculate Moq Price</h5>
                  

                  <br>

                   <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Product Name</th>
                          <th class="text-center">Unit Price</th>
                          <th class="text-center">Quantity</th>
                          <!-- <th class="text-center">Min Moq</th> -->
                          <th class="text-center">Subtotal</th>
                        </tr>

                      </thead>
                      <tbody>

                         <?php 
                                  $i2 = 0;
                                  $totalharga = 0;
                                         
                            ?>
                                     
                             @foreach($all_new_array as $selipar)
                             
                             <?php 

                             $i2++;    

                             $bundar = ceil($selipar['unit_price'] * 2 ) / 2;

                             $sub2 = $bundar * $selipar['total_qt'] ;

                             $totalharga += $sub2;

                             ?>



                        <tr>
                          <td class="text-center">{{ $i2}}</td>
                          <td class="">
                            <p class="text-black">{{$selipar['name_item']}}</p>
                          </td>
                          <td class="text-center">{{ number_format($bundar, 2) }}</td>
                          <td class="text-center">{{$selipar['total_qt']}}</td>
                          <!-- <td class="text-center">{{$selipar['unit_min']}}</td> -->
                          <td class="text-center"><div class="subtotal">{{number_format($sub2, 2)}}</div></td>

                          <!-- <td class="text-center">RM 0.00</td>
                          <td class="text-right">RM {{$cartItems->price}}</td>
                           <td class="text-right">RM <?php echo $sub; ?> </td> -->
                        </tr>

                        @endforeach


                      </tbody>
                    </table>
                  </div>
                
                 
                  <div class="container-sm-height">
                    <div class="row row-sm-height b-a b-grey">
                      <div class="col-sm-2 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 col-sm-height col-middle clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
                        <h1 class="no-margin text-white">RM <?php echo $total; ?></h1>
                      </div>
                    </div>
                  </div>

                
                  <br>
                  <hr>
                  <div>
                    <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png')}}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png')}}" width="78" height="22">

                    <span class="m-l-70 text-black sm-pull-right">(03) XXXXXXXX</span>
                    <span class="m-l-40 text-black sm-pull-right">mudahborong@gmail.com</span>

                  </div>
                </div>
              </div>
            </div>


           
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

        

      
    </div>
      @include('layouts.mango.layout.scripts')
       <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
       
       <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>

       <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-bez/jquery.bez.min.js') }}"></script>

       <script src="{{ asset ('bundle/demo/html/assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
   
       <script src="{{ asset ('bundle/demo/html/assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
       
       <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    

  </body>
</html>