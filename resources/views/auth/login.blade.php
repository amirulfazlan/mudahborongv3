<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->

            @include('layouts.mango.layout.header_checkout')

            <!-- HEADER -->
            
            

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Login</a>
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>Login User</h3>
                                    <p>Welcome to Mudahborong. Already a user please login to your account.</p>
                                </div>
                                  <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                  {{ csrf_field() }}
                                    
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email Address</label>
                                    <input class="simple-field" type="email" placeholder="Enter Email Address"  id="email" name="email" value="{{ old('email') }}" required autofocus/>
                                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                   </div>

                                    

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input class="simple-field" type="password" placeholder="Enter Password" id="password" name="password" required />
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                   @endif
                                  </div>


                                  <div class="checkbox-entry-inline">
                                  <label class="checkbox-entry">

                                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                  <span class="check"></span>

                                  </label>
                                  </div>

                                   <div class="article-container style-1">
                                    <p></p>
                                </div>

                                    <div class="button style-10">Login Page<input type="submit" /></div>

                                      <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>

                                </form>
                            </div>
                        </div>
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>New Customers</h3>
                                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                </div>
                                <a href="/register" class="button style-12">Register Account</a>
                            </div>
                        </div>
                    </div>
                </div>              

                <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

   @include('layouts.mango.layout.searchbox_popup')


   @include('layouts.mango.layout.cart_popup')

   
   @include('layouts.mango.layout.scripts')

    
    

    
</body>
</html>
