<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->

             @include('layouts.mango.layout.header_checkout')

            <!-- HEADER -->
            
            

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Register Form</a>
                </div>
            <div class="content-center">
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>New Customers</h3>
                                    <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}


                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Name</label>
                                    <input class="simple-field" type="text" placeholder="Enter Name" value="" id="name" name="name" value="{{ old('name') }}" required autofocus/>
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                     </div>


                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label>Email Address</label>
                                    <input class="simple-field" type="email" placeholder="Enter Email Address" id="email" name="email" value="{{ old('email') }}" required />
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                     @endif         
                                     </div>

                                    
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label>Password</label>
                                    <input class="simple-field" type="password" placeholder="Enter Password" id="password" name="password" required />
                                     @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                    </div>




                                    <label>Confirm Password</label>
                                    <input class="simple-field" type="password" placeholder="Enter Confirm Password" id="password-confirm" name="password_confirmation" required />

                                    

                                    <div class="button style-12">Register Account<input type="submit"/></div>
                                </form>
                            </div>
                        </div>


                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>Already Registered Customers</h3>
                                    
                                    <p>By login to your account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                </div>
                                <a href="/login" class="button style-10">Login Account</a>





                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
           
                <div class="clear"></div>
                <div class="clear"></div>
               

                <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

   @include('layouts.mango.layout.searchbox_popup')


   @include('layouts.mango.layout.cart_popup')

   
   @include('layouts.mango.layout.scripts')
    
</body>
</html>
