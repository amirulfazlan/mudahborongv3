<!DOCTYPE html>
<html>

  @include('layouts.head')


  


  <body class="fixed-header menu-pin">

  @if(Auth::guest())
  @include('layouts.sidebarWholesaler')
  @else
  @if ( Auth::user()->is_Admin() ) 
   


  @include('layouts.sidebar')
  @else
  @include('layouts.sidebarWholesaler')
  @endif
  @endif

    <div class="page-container">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">Dashboard</a>
                  </li>
                  <li><a href="#" class="active">Order History</a>
                  </li>
                  <li><a href="#" class="active">Order Number : {{$order['id']}} </a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel panel-default">
              <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{ asset ('mango/img/logo-9.png') }}" data-src="{{ asset ('mango/img/logo-9.png') }}" src="{{ asset ('mango/img/logo-9.png') }}" style="width: 195px; height: 30px">
                      <address class="m-t-10">
                                      MudahBorong Online
                                      <br>(03) XXXXXXXXX.
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Order List</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="container-sm-height">
                    <div class="row-sm-height">
                      <div class="col-md-9 col-sm-height sm-no-padding">
                        <p class="small no-margin">Shipped to </p>

                     

                        <h5 class="semi-bold m-t-0">{{$nama}} </h5>

                                          <address>
                                              <strong>{{$address->company}}</strong><br>
                                              {{$address->street}} , <br>
                                              {{$address->postcode}} , {{$address->city}}<br>
                                              {{$address->state}}, <br> 
                                              {{$address->country}}
                                          </address>


                    
                                 
                      </div>
                      <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                        <br>
                       <!--  <div>
                          <div class="pull-left font-montserrat bold all-caps">Invoice No :</div>
                          <div class="pull-right">0047</div>
                          <div class="clearfix"></div>
                        </div> -->
                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Order date :</div>
                          <div class="pull-right"> {{$order['created_at']->format('d-m-Y')}}</div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br><br>  

                  <h5 class="font-montserrat all-caps small no-margin hint-text text-black bold">List of Product</h5>


                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="">Image</th>     
                          <th class="">Product</th>
                          <th class="text-center">Size</th>
                          <th class="text-center">Color</th>
                          <th class="text-center">Quantity</th>
                        
                        </tr>

                      </thead>
                      <tbody>

                      <?php $i = 0 ?>
                        @foreach($json_decode as $content)
                       <?php $i++ ?>  

                        <tr>

                          <td class="text-center">{{ $i}}</td>

                         @foreach ($pro_var as $var)
                         @if ( $var['p_id'] == $content->id)
                         @if ( $var['p_color'] == $content->options->color)
                          <td class="">
                            <div class="row" id="thumbs">
                              <img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" height="100" width="100">
                            </div>      
                          </td>
                          @endif 
                          @endif
                          @endforeach 

                          <td class="text">{{  $content->name }}</td>
                          <td class="text-center">{{  $content->options->size }}</td>
                          <td class="text-center">{{  $content->options->color }}</td>
                          <td class="text-center">{{  $content->qty }}</td>
                         
                        </tr>
                        @endforeach
                     


                      </tbody>
                    </table>
                  </div>

                  <br>
                  <br>

                  <h5 class="font-montserrat all-caps small no-margin hint-text text-black bold">Combine Product To Calculate Moq Price</h5>
                  

                  <br>

                   <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text">Product Name</th>
                          <th class="text-center">Quantity</th>
                          <th class="text-center">Unit Price</th>
                          <th class="text-center">Subtotal</th>
                        </tr>

                      </thead>
                      <tbody>

                         <?php $x = 0;
                             $totalharga = 0; ?> 

                        @foreach($all_new_output as $group)
                       
                        <?php $x++ ;
                         $admin_round= round($group['admin'] ,1,PHP_ROUND_HALF_UP) ;
                         $sub = $admin_round * $group['total_qt'] ;
                         $totalharga += $sub; ?>

                        
                        <tr>
                          <td class="text-center">{{ $x}}</td>
                          <td class="">
                            <p class="text-black">{{  $group['name_item'] }}</p>
                          </td>
                          <td class="text-center">{{  $group['total_qt'] }}</td>
                          <td class="text-center">  {{ number_format($admin_round, 2) }}</td>
                          
                          <td class="text-center"><div class="subtotal"> {{ number_format($sub, 2) }}</div></td>
                        
                        </tr>
                        @endforeach
                      
                      </tbody>
                    </table>
                  </div>
                
                 
                  <div class="container-sm-height">
                    <div class="row row-sm-height b-a b-grey">
                      <div class="col-sm-2 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 col-sm-height col-middle clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold"></h5>
                        <h3 class="no-margin"></h3>
                      </div>
                      <div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
                        <h1 class="no-margin text-white">RM {{ number_format($totalharga, 2) }}</h1>
                      </div>
                    </div>
                  </div>

                
                  <br>
                  <hr>
                  <div>
                    <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png')}}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png')}}" width="78" height="22">

                    <span class="m-l-70 text-black sm-pull-right">(03) XXXXXXXX</span>
                    <span class="m-l-40 text-black sm-pull-right">mudahborong@gmail.com</span>

                  </div>
                </div>
              </div>
            </div>




            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      {{--@include('layouts.leftsidebar')--}}

      </div>
      @include('layouts.scripts')

  </body>
</html>