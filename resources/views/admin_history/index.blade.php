
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>


  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Product</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

                <div class="panel panel-transparent">
              <div class="panel-heading">

              

                <div class="panel-title">List Of History Order
                </div>
                </br>

                <div class="clearfix"></div>
              </div>
              <div class="panel-body">

                  </br>

                  <form method="get" action="/admin-orderhistory">

                 <div class="form-group row">
                 <div class="form-group {{ $errors->has('search_date') ? ' has-error' : '' }}">
                 <label class="col-sm-2 control-label">Order Date</label>
                 <div class="col-sm-4">
                 <input type="date" name="search_date" class="form-control">
                 @if ($errors->has('search_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('search_date') }}</strong>
                  </span>
                 @endif


                 </div>

                <button type="submit" class="btn btn-primary">Search</button>
            
            </div>

           

            </div>

          </form>

            </br>


                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Date</th>
                        <th style="width:20%">User Id</th>
                        <th style="width:20%">Grand Total (RM)</th>
                        <th style="width:20%">Status</th>
                         <th style="width:20%">Tracking No</th>
                        <th style="width:20%">Tracking Details</th>


                        <th style="width:15%">View</th>                       
                        <th style="width:15%">Delete</th>
                        <th style="width:15%">Print</th>

                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                       @foreach($order as $history)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i }}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{  $history['created_at']->todatestring() }} </p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $history->wh_name['name'] }} </p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{  $history['grand_total'] }} </p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{  $history['status'] }} </p>
                        </td>

                         <td class="v-align-middle ">
                           @foreach($tracking as $track)
                           @if($history['id'] == $track['order_id'])
                           <?php $count_track = count($track['order_id']);?>
                           <p> Insert :  {{$count_track}} </p>
                           @else
                           <p> Insert :  0 </p>

                          @endif
                          @endforeach
                        </td>

                        
                        <td class="v-align-middle">
                           <a href="{{action('TrackingNewController@show', $history['id'])}}" class="btn btn-warning">Show <span class="glyphicon glyphicon-eye-open"></span></a>
                        </td>
                       

                        <td class="v-align-middle">

                         <a href="{{action('AdminHistoryController@show', $history['id'])}}" class="btn btn-info">Order <span class="glyphicon glyphicon-eye-open"></span></a>

                        </td>
                        
                      
                        <td class="v-align-middle">
                          <form action="{{action('AdminHistoryController@destroy', $history['id'])}}" method="post">
                          {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                           </form>
                        </td>           

                              <td><a href="{{action('AdminHistoryController@downloadPDF', $history['id'])}}">PDF</a></td>
                      </tr>

                      @endforeach
                      
                        

                        
                    </tbody>
                  </table>

                   {{ $order->appends(request()->input())->links() }}

               

                 
                  
                </div>
              </div>
            </div>

    
         
             
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

    </div>
      @include('layouts.scripts')

  </body>
</html>