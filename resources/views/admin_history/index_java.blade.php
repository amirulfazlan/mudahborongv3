<!DOCTYPE html>
<html>

  @include('layouts.head')

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

   window.onload=function(){
    var total = 0;
    var sum = document.getElementsByClassName('inputtosum');
    for (var i = 0; i < sum.length; ++i) {
    if (!isNaN(parseFloat(sum[i].value)) )
    total += parseFloat(sum[i].value);  
    }
    document.getElementById('total').value=total;
}


 
</script>


  


  <body class="fixed-header menu-pin">

  @if(Auth::guest())
  @include('layouts.sidebarWholesaler')
  @else
  @if ( Auth::user()->is_Admin() ) 
   


  @include('layouts.sidebar')
  @else
  @include('layouts.sidebarWholesaler')
  @endif
  @endif

    <div class="page-container">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">Dashboard</a>
                  </li>
                  <li><a href="#" class="active">Order History</a>
                  </li>
                  <li><a href="#" class="active">Order Number : {{$order['id']}} </a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <h1><b>This is the order detail for order number : {{$order['id']}} </b></h1>

            <div class="table-responsive">
                         <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:2%">ID</th>
                        <th style="width:2%">Product Image</th>
                        <th style="width:20%">Product Name</th>
                        <th style="width:20%">Size</th>
                        <th style="width:20%">Color</th>
                        <th style="width:20%">Quantity</th>
                        
                        
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                        @foreach($json_decode as $content)
                       <?php $i++ ?>  
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>

                        <td class="v-align-middle">
                            <p>{{  $content->id }}</p>
                        </td>


                        @foreach ($pro_var as $var)
                        @if ( $var['p_id'] == $content->id)
                        @if ( $var['p_color'] == $content->options->color)
                        <td class="v-align-middle">

                           <div class="row" id="thumbs">

                              <img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" height="100" width="100">

                          </div> 
                            
                        </td>
                        @endif 
                        @endif
                        @endforeach 


                        <td class="v-align-middle">
                            <p>{{  $content->name }}</p>

                        </td>
                        <td class="v-align-middle ">

                            <p>{{  $content->options->size }}</p>

                        </td>
                        <td class="v-align-middle ">
                          <p>{{  $content->options->color }}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{  $content->qty }}</p>
                        </td>

              
                      </tr>
                      
                    @endforeach

                        
                    </tbody>
                  </table>
                </div>


                <h1><b> Payment Table</b></h1>



                <div class="table-responsive">
                         <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:2%">ID</th>
                        
                        <th style="width:20%">Product Name</th>
                        
                        <th style="width:20%">Quantity</th>

                        <th style="width:20%">Moq</th>

                        <th style="width:20%">Subprice</th>

                        <th style="width:20%">Total Price</th>
                        
                        
                      </tr>
                    </thead>
                    <tbody>

                       <?php $x = 0 ?>
                        @foreach($output as $group)
                       <?php $x++ ?>  
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $x}}</p>
                        </td>

                        <td class="v-align-middle">
                            <p>{{  $group->id }}</p>
                        </td>


                        <td class="v-align-middle">
                            <p>{{  $group->name }}</p>

                        </td>
                        <td class="v-align-middle ">

                            <p>{{  $group->qty }}</p>

                        </td>

                       @foreach ($cur as $yuan) 
                       @foreach ($margin as $mar)
                       @foreach ($products as $product)
                        @if($group->id == $product['id'])   
                        @if( $mar['id'] == $product['margin_id']) 


                          @foreach ($moq_item as $moq)

                             <?php $admin_price = 0;  
                                   $total_price = 0;
                                   $grand_total = 0;

                             ?>

                             @if($moq['moqty_id'] == $product['moqty_id'])

                             @if( $group->qty >= $moq['min_unit1']  && $group->qty < $moq['min_unit2'])

                              <?php $admin_price = ( $product['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                                    $total_price =  number_format($admin_price, 2) * $group->qty ;
                              ?>
                              

                              @elseif( $group->qty >= $moq['min_unit2']  && $group->qty < $moq['min_unit3'])

                              <?php $admin_price = ( $product['base_price2'] +  $mar['mar_admin2']) /  $yuan['cur_value'];
                                    $total_price =  number_format($admin_price, 2) * $group->qty ;

                                   
                              ?>

                              @elseif( $group->qty >= $moq['min_unit3'] )

                              <?php $admin_price = ( $product['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                    $total_price =  number_format($admin_price, 2) * $group->qty ; 
                              ?>
                            @endif

                            <?php 

                            $round= round($total_price ,1,PHP_ROUND_HALF_UP) ;
                            $grand_total += $round;
                            
                            ?>

                             <td class="v-align-middle ">

                              <p>moq = {{ $moq['min_unit3'] }}</p>

                              </td>

                              <td class="v-align-middle ">

                              <p>RM  {{ number_format($admin_price, 2) }}</p>

                              </td>

                              <td class="v-align-middle ">

                              <p>RM  {{ number_format($round, 2) }}</p>

                              </td>

                               <td class="v-align-middle ">

                                <input  name="unittotal[]" class="inputtosum" type="text" onblur="sumofunittotal();" onkeydown="return onlynumber(event);" size="3" value="<?php echo number_format($round, 2); ?>" /><br>

                            

                              </td>



                              
                            @endif


                           @endforeach

                       
                        @endif
                        @endif

                        @endforeach
                        @endforeach
                        @endforeach

                      </tr>

                      
                    @endforeach

                    <tr>

                      <td class="v-align-middle" colspan="5" align="right" >

                              <b><p>Grand Total :</p></b>

                              </td>

                     

                      <td class="v-align-middle" colspan="2" align="center" >

                              <b><p>RM <input type="text" id ="grandtotal" name="garandtotal">
                                <input name="total" type="text" id="total" value="">

                              </p></b>

                              </td>

                      </tr>

                        
                    </tbody>
                  </table>
                </div>


            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      {{--@include('layouts.leftsidebar')--}}

      </div>
      @include('layouts.scripts')

  </body>
</html>