<!DOCTYPE html>
<html>


@include('layouts.mango.layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Checkout</a>
                </div>

                <div class="container">
        <div class="row form-group">
            <div class="col-xs-12">
                <ul class="nav nav-pills nav-justified thumbnail setup-panel" id="myNav">
                    <li id="navStep1" class="li-nav active" step="#step-1">
                        <a>
                            <h4 class="list-group-item-heading">Step 1</h4>
                            <p class="list-group-item-text">First step description</p>
                        </a>
                    </li>
                    <li id="navStep2" class="li-nav disabled" step="#step-2">
                        <a>
                            <h4 class="list-group-item-heading">Step 2</h4>
                            <p class="list-group-item-text">Second step description</p>
                        </a>
                    </li>
                    <li id="navStep3" class="li-nav disabled" step="#step-3">
                        <a>
                            <h4 class="list-group-item-heading">Step 3</h4>
                            <p class="list-group-item-text">Third step description</p>
                        </a>
                    </li>
                    <li id="navStep4" class="li-nav disabled" step="#step-4">
                        <a>
                            <h4 class="list-group-item-heading">Step 4</h4>
                            <p class="list-group-item-text">Second step description</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <form class="container">
        <div class="row setup-content" id="step-1">
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <h1> STEP 1</h1>
                    <!-- <form> -->

                    <div class="container col-xs-12">
                        <input />
                    </div>
                    <input onclick="step1Next()" class="btn btn-md btn-info" value="Next">

                    <!-- </form> -->
                </div>
            </div>
        </div>
    </form>
    <form class="container">
        <div class="row setup-content" id="step-2">
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <h1 class="text-center"> STEP 2</h1>

                    <!--<form>-->
                    <div class="container col-xs-12">
                        <input />
                        <input />
                    </div>
                    <!--</form> -->

                    <input onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                    <input onclick="step2Next()" class="btn btn-md btn-info" value="Next">

                </div>
            </div>
        </div>
    </form>
    <form class="container">
        <div class="row setup-content" id="step-3">
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <h1 class="text-center"> STEP 3</h1>

                    <!--<form></form> -->

                    <input onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                    <input onclick="step3Next()" class="btn btn-md btn-info" value="Next">

                </div>
            </div>
        </div>
    </form>
    <form class="container">

        <div class="row setup-content" id="step-4">
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <h1 class="text-center"> STEP 4</h1>

                    <!--<form></form> -->
                    <input onclick="prevStep()" class="btn btn-md btn-info" value="Prev">
                    <input class="btn btn-md btn-primary" value="Send">
                </div>
            </div>
        </div>
    </form>



                @include('layouts.mango.layout.product_features_inline')               

               <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->

            </div>


        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  @include('layouts.mango.layout.searchbox_popup')
  

   @include('layouts.mango.layout.cart_popup')

   @include('layouts.mango.layout.scripts')

</body>
</html>
