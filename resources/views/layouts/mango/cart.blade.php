<!DOCTYPE html>
<html>

@include('layout.head')


<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layout.header')
            <!-- HEADER -->
         

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Shop</a>
                    <a href="#">Shopping Cart</a>
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 information-entry">
                            <h3 class="cart-column-title size-1">Products</h3>
                            <div class="traditional-cart-entry style-1"> 
                                <a class="image" href="#"><img alt="" src="{{ asset ('mango/img/product-minimal-1.jpg') }}"></a>
                                <div class="content">
                                    <div class="cell-view">
                                        <a class="tag" href="#">woman clothing</a>
                                        <a class="title" href="#">Pullover Batwing Sleeve Zigzag</a>
                                        <div class="inline-description">S / Dirty Pink</div>
                                        <div class="inline-description">Zigzag Clothing</div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title">Quantity</div>
                                            <div class="entry number-minus">&nbsp;</div>
                                            <div class="entry number">10</div>
                                            <div class="entry number-plus">&nbsp;</div>
                                            <a class="button style-17">remove</a>
                                            <a class="button style-15">Update Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="traditional-cart-entry style-1">
                                <a class="image" href="#"><img alt="" src="{{ asset ('mango/img/product-minimal-1.jpg') }}"></a>
                                <div class="content">
                                    <div class="cell-view">
                                        <a class="tag" href="#">woman clothing</a>
                                        <a class="title" href="#">Pullover Batwing Sleeve Zigzag</a>
                                        <div class="inline-description">S / Dirty Pink</div>
                                        <div class="inline-description">Zigzag Clothing</div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title">Quantity</div>
                                            <div class="entry number-minus">&nbsp;</div>
                                            <div class="entry number">10</div>
                                            <div class="entry number-plus">&nbsp;</div>
                                            <a class="button style-17">remove</a>
                                            <a class="button style-15">Update Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="traditional-cart-entry style-1">
                                <a class="image" href="#"><img alt="" src="{{ asset ('mango/img/product-minimal-1.jpg') }}"></a>
                                <div class="content">
                                    <div class="cell-view">
                                        <a class="tag" href="#">woman clothing</a>
                                        <a class="title" href="#">Pullover Batwing Sleeve Zigzag</a>
                                        <div class="inline-description">S / Dirty Pink</div>
                                        <div class="inline-description">Zigzag Clothing</div>
                                        <div class="price">
                                            <div class="prev">$199,99</div>
                                            <div class="current">$119,99</div>
                                        </div>
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title">Quantity</div>
                                            <div class="entry number-minus">&nbsp;</div>
                                            <div class="entry number">10</div>
                                            <div class="entry number-plus">&nbsp;</div>
                                            <a class="button style-17">remove</a>
                                            <a class="button style-15">Update Cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="information-entry col-md-6">
                                    <div class="sale-entry">
                                        <div class="hot-mark red">hot</div>
                                        <div class="sale-price"><span>-40%</span> winter Sale</div>
                                        <div class="sale-description">Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do</div>
                                    </div>
                                </div>
                                <div class="information-entry col-md-6">
                                    <div class="sale-entry">
                                        <div class="hot-mark red">hot</div>
                                        <div class="sale-price"><span>FREE</span> UK delivery</div>
                                        <div class="sale-description">Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3 information-entry">
                            <h3 class="cart-column-title size-1" style="text-align: center;">Subtotal</h3>
                            <div class="sidebar-subtotal">
                                <div class="price-data">
                                    <div class="main">$129.99</div>
                                    <div class="title">Excluding tax &amp; shipping</div>
                                    <div class="subtitle">ORDERS WILL BE PROCESSED IN USD</div>
                                </div>
                                <div class="additional-data">
                                    <div class="title"><span class="inline-label red">Promotion</span> Additional Notes</div>
                                    <textarea class="simple-field size-1"></textarea>
                                    <a class="button style-10">Checkout</a>
                                </div>
                            </div>
                            <div class="block-title size-1">Get shipping estimates</div>
                            <form>
                                <label>Country</label>
                                <div class="simple-drop-down simple-field size-1">
                                    <select>
                                        <option>United States</option>
                                        <option>Great Britain</option>
                                        <option>Canada</option>
                                    </select>
                                </div>
                                <label>State</label>
                                <div class="simple-drop-down simple-field size-1">
                                    <select>
                                        <option>Alabama</option>
                                        <option>Alaska</option>
                                        <option>Idaho</option>
                                    </select>
                                </div>
                                <label>Zip Code</label>
                                <input type="text" value="" placeholder="Zip Code" class="simple-field size-1">
                                <div class="button style-16" style="display: block; margin-top: 10px;">calculate shipping<input type="submit"/></div>
                            </form>
                        </div>
                    </div>
                </div>

               @include('layout.product_features_inline')               

                <!-- FOOTER -->

                @include('layout.footer')

                <!-- FOOTER -->
               
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layout.searchbox_popup')

    @include('layout.cart_popup')
    

     @include('layout.scripts')

   
</body>
</html>
