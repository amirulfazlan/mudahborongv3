<!DOCTYPE html>
<html>

@include('layout.head')


<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layout.header')
            <!-- HEADER -->
           

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Shop</a>
                    <a href="#">T-shirts</a>
                    <a href="#">Careers</a>
                    <a href="#">T-shirt Stampata</a>                         
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="product-preview-box">
                                <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image"> 
                                                <img src="{{ asset ('mango/img/product-main-1.jpg') }}" alt="" data-zoom="{{ asset ('mango/img/product-main-1-zoom.jpg') }}" />
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('mango/img/product-main-2.jpg') }}" alt="" data-zoom="{{ asset ('mango/img/product-main-2-zoom.jpg') }}" />
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('mango/img/product-main-3.jpg') }}" alt="" data-zoom="{{ asset ('mango/img/product-main-3-zoom.jpg') }}" />
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('mango/img/product-main-4.jpg') }}" alt="" data-zoom="{{ asset ('mango/img/product-main-4-zoom.jpg') }}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pagination"></div>
                                    <div class="product-zoom-container">
                                        <div class="move-box"> 
                                            <img class="default-image" src="{{ asset ('mango/img/product-main-1.jpg') }}" alt="" />
                                            <img class="zoomed-image" src="{{ asset ('mango/img/product-main-1-zoom.jpg') }}" alt="" />
                                        </div>
                                        <div class="zoom-area"></div>
                                    </div>
                                </div>
                                <div class="swiper-hidden-edges">
                                    <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide selected">
                                                <div class="paddings-container"> 
                                                    <img src="{{ asset ('mango/img/product-main-1.jpg') }}" alt="" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('mango/img/product-main-2.jpg') }}" alt="" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('mango/img/product-main-3.jpg') }}" alt="" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('mango/img/product-main-4.jpg') }}" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pagination"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 information-entry">
                            <div class="product-detail-box">
                                <h1 class="product-title">T-shirt Basic Stampata</h1>
                                <h3 class="product-subtitle">Loremous Clothing</h3>
                                <div class="rating-box">
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star"></i></div>
                                    <div class="star"><i class="fa fa-star-o"></i></div>
                                    <div class="star"><i class="fa fa-star-o"></i></div>
                                    <div class="rating-number">25 Reviews</div>
                                </div>
                                <div class="product-description detail-info-entry">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                <div class="price detail-info-entry">
                                    <div class="prev">$90,00</div>
                                    <div class="current">$70,00</div>
                                </div>

                                <div class="size-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Size</div>
                                    <div class="entry active">xs</div>
                                    <div class="entry">s</div>
                                    <div class="entry">m</div>
                                    <div class="entry">l</div>
                                    <div class="entry">xl</div>
                                    <div class="spacer"></div>
                                </div>

                                <div class="color-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Color</div>
                                    <div class="entry active" style="background-color: #d23118;">&nbsp;</div>
                                    <div class="entry" style="background-color: #2a84c9;">&nbsp;</div>
                                    <div class="entry" style="background-color: #000;">&nbsp;</div>
                                    <div class="entry" style="background-color: #d1d1d1;">&nbsp;</div>
                                    <div class="spacer"></div>
                                </div>

                                <div class="quantity-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Quantity</div>
                                    <div class="entry number-minus">&nbsp;</div>
                                    <div class="entry number">10</div>
                                    <div class="entry number-plus">&nbsp;</div>
                                </div>
                                <div class="detail-info-entry">
                                    <a class="button style-10">Add to cart</a>
                                    <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="tags-selector detail-info-entry">
                                    <div class="detail-info-entry-title">Tags:</div>
                                    <a href="#">bootstrap</a>/
                                    <a href="#">collections</a>/
                                    <a href="#">color/</a>
                                    <a href="#">responsive</a>
                                </div>
                                <div class="share-box detail-info-entry">
                                    <div class="title">Share in social media</div>
                                    <div class="socials-box">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                        <a href="#"><i class="fa fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="information-blocks">
                    <div class="tabs-container style-1">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title">Product info</div>
                            <div class="list">
                                <a class="tab-switcher active">Product description</a>
                                <a class="tab-switcher">SHIPPING &amp; RETURNS</a>
                                <a class="tab-switcher">Reviews (25)</a>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                            <h4>RETURNS POLICY</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 information-entry">
                                            <h4>SHIPPING</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                            <h4>RETURNS POLICY</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 information-entry">
                                            <h4>SHIPPING</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                            <h4>RETURNS POLICY</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 information-entry">
                                            <h4>SHIPPING</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</p>
                                            <ul>
                                                <li>Any Product types that You want - Simple, Configurable</li>
                                                <li>Downloadable/Digital Products, Virtual Products</li>
                                                <li>Inventory Management with Backordered items</li>
                                                <li>Customer Personal Products - upload text for embroidery, monogramming</li>
                                                <li>Create Store-specific attributes on the fly</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layout.product_features_tab')

                @include('layout.product_features_inline')


                 <!-- FOOTER -->

                @include('layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

   @include('layout.searchbox_popup')

   @include('layout.cart_popup')

   @include('layout.scripts')

</body>
</html>
