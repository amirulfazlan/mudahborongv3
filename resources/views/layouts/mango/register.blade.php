<!DOCTYPE html>
<html>

@include('layout.head')

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layout.header')
            <!-- HEADER -->
            
            

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Register Form</a>
                </div>
            <div class="content-center">
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-6 information-entry">
                            <div class="login-box">
                                <div class="article-container style-1">
                                    <h3>New Customers</h3>
                                    <p>Lorem ipsum dolor amet, conse adipiscing, eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <form>
                                    <label>Name</label>
                                    <input class="simple-field" type="text" placeholder="Enter Name" value="" />
                                    <label>Email Address</label>
                                    <input class="simple-field" type="text" placeholder="Enter Email Address" value="" />
                                    <label>Password</label>
                                    <input class="simple-field" type="password" placeholder="Enter Password" value="" />
                                    <label>Confirm Password</label>
                                    <input class="simple-field" type="password" placeholder="Enter Confirm Password" value="" />
                                    <div class="button style-12">Register Account<input type="submit" value="" /></div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

                
                <div class="clear"></div>
                <div class="clear"></div>
               

                <!-- FOOTER -->

                @include('layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

   @include('layout.searchbox_popup')


   @include('layout.cart_popup')

   
   @include('layout.scripts')

    
    

    
</body>
</html>
