<!DOCTYPE html>
<html>

@include('layout.head')


<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
        
             <!-- HEADER -->
            @include('layout.header')
            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Account</a>
                    <a href="#">Dashboard</a>
                </div>

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 col-sm-push-3">
                            <div class="wishlist-header hidden-xs">
                                <div class="title-1">Dashboard</div>
                                
                            </div>
                            <div class="wishlist-box">
                                <div class="wishlist-entry">
                                    <div class="column-1">
                                        <div class="traditional-cart-entry">
                                            <a class="image" href="#"><img alt="" src="img/product-minimal-1.jpg" /></a>
                                            <div class="content">
                                                <div class="cell-view">
                                                    <!-- <a class="tag" href="#">woman clothing</a> -->
                                                    <a class="title" href="#">Welcome "XXXXXXXX" , you are wholesaler</a>
                                                    <div class="inline-description">This is the page for new catalogue meow meow meow</div>

                                                    <div class="inline-description"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                                
                               
                                
                                
                            </div>
                        </div>
                        
                         @include('layout.profile_sidebar')
                         
                    </div>
                </div>

                <!-- FOOTER -->
                 @include('layout.footer')
                 <!-- FOOTER -->

               
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('layout.searchbox_popup')


    @include('layout.cart_popup')

   
   @include('layout.scripts')

   


</body>
</html>
