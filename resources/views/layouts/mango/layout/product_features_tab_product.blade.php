
                <div class="information-blocks">
                    <div class="tabs-container">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title">Products</div>
                            <div class="list">
                                <a class="block-title tab-switcher active">Featured Products</a>
        
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            <div class="tabs-entry">
                                <div class="products-swiper">
                                    <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                                        <div class="swiper-wrapper">  

                                         <?php $count = 0; 
                                          shuffle($pro);
                                          ?>

                                          @foreach ($pro as $product)
                                          <?php 
 
                                          if($count == 4) break; 
                                          ?>                                                                                                                      
                                            <div class="swiper-slide"> 
                                                <div class="paddings-container">
                                                    <div class="product-slide-entry shift-image">
                                                        <div class="product-image">
                                                            <img src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" style="width: 160px; height: 160px" />
                                                            <img src="{{ asset ('images/main_product/'.$product['back_image']) }}" alt="" style="width: 160px; height: 160px" />
                                                            <a href="{{action('ShoppingCatalog_Controller@show', $product['id'])}}" class="top-line-a right"><i class="fa fa-expand"></i> <span>Quick View</span></a>
                                                            
                                                        </div>
                                                        <a class="tag" href="#">
                                                        @if ($product['gender_id'] == 1)
                                                          Men
                                                        @else
                                                          Women
                                                        @endif

                                                        @foreach ($cat as $cats)

                                                        @if ($product['category_id'] == $cats['id'])
                                                        {{ $cats['category_product']}}

                                                        @endif

                                                        @endforeach

                                                       
                                                        </a>



                                                        <a class="title" href="{{action('ShoppingCatalog_Controller@show', $product['id'])}}">{{ $product['p_name']}} </a>
                                                       
                                                        <div class="price">
                                                        @foreach ($cur as $yuan)

                                                        @foreach ($margin as $mar)
 
                                                        @if( $mar['id'] == $product['margin_id'] )

                                                        @foreach ($moq_item as $moq)

                                                        @if( $moq['moqty_id'] == $product['moqty_id'] )
       
                                                        <?php $admin_price = ( $product['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                                         $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                                         $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                                         $bundar = ceil($whole_price * 2 ) / 2
                                                         ?>
                                                            <div class="current">RM {{ number_format($bundar, 2) }}</div>

                                                         @endif 
                                                         @endforeach 
       
                                                         @endif 
       
                                                         @endforeach
     
                                                         @endforeach


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php $count++; ?>

                                             @endforeach

                                        </div>
                                        <div class="pagination"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
              