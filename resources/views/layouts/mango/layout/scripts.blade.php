   
<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="{{ asset ('mango/js/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ asset ('mango/js/idangerous.swiper.min.js') }}"></script>
    <script src="{{ asset ('mango/js/global.js') }}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>

    <!-- custom scrollbar -->
    <script src="{{ asset ('mango/js/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset ('mango/js/jquery.jscrollpane.min.js') }}"></script>

