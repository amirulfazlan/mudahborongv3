 <div style="height: 120px;"></div>

        <div class="footer-wrapper style-3">
            <footer class="type-2">
                <div class="position-center">
                    <img alt="" src="{{ asset ('mango/img/logo-21.png') }}" class="footer-logo">
                    <div class="footer-links">
                        <a href="#">Site Map</a>
                        <a href="#">Search</a>
                        <a href="#">Terms</a>
                        <a href="#">Advanced Search</a>
                        <a href="#">Orders and Returns</a>
                        <a href="#">Contact Us</a>
                    </div>
                    <div class="copyright">Created with &#9829; by <a href="#">Teratak Pujangga Creative Media</a>. All right reserved</div>
                </div>
            </footer>
        </div>
