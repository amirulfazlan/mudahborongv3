 <style type="text/css">

 .ScrollStyle
{
    max-height: 650px;
    overflow-y: auto; 
}
     
 </style>
 <div class="cart-box popup">
        <div class="popup-container ScrollStyle">

            <?php 
             $total = 0; 
             $i = 0;
             ?>

        @foreach($cart as $cartItems)

         <?php 
         $total += $cartItems->subtotal ; 
          $sub = $cartItems->price * $cartItems->qty ;
                             $i++;
                             ?>

          
            
            <div class="cart-entry">

           @foreach ($pro_var as $var)
               @if ( $var['p_id'] == $cartItems->id)
               @if ( $var['p_color'] == $cartItems->options->color)
                <a class="image"><img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" /></a>

                @endif
               @endif
             @endforeach 



            @foreach ($pro as $products)
            @if ( $products['id'] == $cartItems->id)  

                <div class="content">
                    <a class="title" href="#">{{$cartItems->name}}</a>
                    <div class="quantity">Quantity: {{$cartItems->qty}}</div>
                    <div class="price">RM {{number_format($cartItems->price, 2)}} (unit) </div>  
                </div>
                
            </div>

            @endif
            @endforeach
              
         @endforeach
       

           <!--  <div class="summary">
                <div class="subtotal">Subtotal: $990,00</div>
                <div class="grandtotal">Grand Total <span>$1029,79</span></div>
            </div> -->

            <div class="cart-buttons">
                <div class="column">
                    <a class="button style-3" href="/cart" >View Basket</a>
                    <div class="clear"></div>
                </div>
                <div class="column">
                    <a class="button style-4" href="/basket" h>Checkout</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>

        </div>
    </div>