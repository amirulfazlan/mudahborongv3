 <div class="information-blocks">
            <h3 class="block-title inline-product-column-title">Featured products</h3>
                    <div class="row">
                        <div class="information-entry"> 

                            <?php $count = 0; 
                            shuffle($pro);
                            ?>

                            @foreach ($pro as $product)
                             <?php 

                             if($count == 3) break; 
                            ?>

                              <div class="col-md-4 inline-product-entry">  

                                <a href="{{action('ShoppingCatalog_Controller@show', $product['id'])}}" class="image"><img alt="" src="{{ asset ('images/main_product/'.$product['main_image']) }}"></a>



                                <div class="content">
                                    <div class="cell-view">
                                        <a href="{{action('ShoppingCatalog_Controller@show', $product['id'])}}" class="title">{{ $product['p_name']}}</a>
                                        <div class="price">
                                            
                                            <div class="current"> 

                                                @foreach ($cur as $yuan)

                                         @foreach ($margin as $mar)
 
                                         @if( $mar['id'] == $product['margin_id'] )

                                         @foreach ($moq_item as $moq)

                                         @if( $moq['moqty_id'] == $product['moqty_id'] )
       
                                         <?php $admin_price = ( $product['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                               $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                               $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                                $bundar = ceil($whole_price * 2 ) / 2
                                         ?>

                                          RM {{ number_format($bundar, 2) }}

                                         
                                         @endif 
                                         @endforeach 
       
                                         @endif 
       
                                          @endforeach
     
                                          @endforeach

                                            

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                             <?php $count++; ?>
                            @endforeach
                            

                        </div>
                        
            </div>
 </div>             