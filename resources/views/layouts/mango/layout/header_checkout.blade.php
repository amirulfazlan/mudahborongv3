<div class="header-wrapper style-10">
                <header class="type-1">

                    <div class="header-product">
                        <div class="logo-wrapper">
                            <a href="#" id="logo"><img alt="" src="{{ asset ('mango/img/logo-9.png') }}"></a>
                        </div>
                        <div class="product-header-message">
                            
                        </div>
                        <div class="product-header-content">
                            <div class="line-entry">
                                <div class="menu-button responsive-menu-toggle-class"><i class="fa fa-reorder"></i></div>
                                <div class="header-top-entry increase-icon-responsive open-search-popup">
                                   
                                </div>
                                <div class="header-top-entry increase-icon-responsive">
                                    <div class="title"><i class="fa fa-user"></i> <a href="/profile"><span>My Account</span></a></div>
                                </div>
                               
                            </div>
                            <div class="middle-line"></div>
                            
                        </div>
                    </div>

                    <div class="close-header-layer"></div>
                    <div class="navigation">
                        <div class="navigation-header responsive-menu-toggle-class">
                            <div class="title">Navigation</div>
                            <div class="close-menu"></div>
                        </div>
                        <div class="nav-overflow">
                            <nav>
                                 
                                <ul>
                                                 
                                       <li><a href="/main">Shop</a></li>

                                       <li class="simple-list">
                                        <a href="/shopping_catalog?gender_id=1">Men</a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">

                                                <li><a href="/shopping_catalog?category_id=3"><i class="fa fa-angle-right"></i>Accessories</a></li>

                                                <li><a href="/shopping_catalog?category_id=12"><i class="fa fa-angle-right"></i>Bags</a></li>

                                                <li><a href="/shopping_catalog?category_id=1"><i class="fa fa-angle-right"></i>Shirts</a></li>

                                                <li><a href="/shopping_catalog?category_id=5"><i class="fa fa-angle-right"></i>Shoes</a></li>

                                                 <li><a href="/shopping_catalog?category_id=2"><i class="fa fa-angle-right"></i>Pants</a></li>
                                          
                                                
                                            </ul>
                                        </div>
                                    </li>

                                    <li class="simple-list">
                                        <a href="/shopping_catalog?gender_id=2">Women</a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">


                                                <li><a href="/shopping_catalog?category_id=14"><i class="fa fa-angle-right"></i>Accessories</a></li>

                                                <li><a href="/shopping_catalog?category_id=13"><i class="fa fa-angle-right"></i>Bags</a></li>

                                                <li><a href="/shopping_catalog?category_id=4"><i class="fa fa-angle-right"></i>Shirts</a></li>

                                                <li><a href="/shopping_catalog?category_id=7"><i class="fa fa-angle-right"></i>Shoes</a></li>

                                                 <li><a href="/shopping_catalog?category_id=6"><i class="fa fa-angle-right"></i>Pants</a></li>
                                          
                                                
                                                
                                            </ul>
                                        </div>
                                    </li>


                                       
                                       
                                       <li><a href="/shopping_catalog?cat_id[]=1&cat_id[]=2&cat_id[]=4&cat_id[]=6">Clothes</a></li>
                                       <li><a href="/shopping_catalog?cat_id[]=3&cat_id[]=11&cat_id[]=14">Accessories</a></li>
                                       <li><a href="/shopping_catalog?cat_id[]=5&cat_id[]=7">Shoes</a></li>


                                     <li class="simple-list">
                                        <a href="#">Top Brands</a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">

                                                 <li><a href="/shopping_catalog?t_brand_id[]=6"><i class="fa fa-angle-right"></i>Adidas</a></li>

                                                            <li><a href="/shopping_catalog?t_brand_id[]=7"><i class="fa fa-angle-right"></i>Nike</a></li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=8"><i class="fa fa-angle-right"></i>Supreme</a></li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=27"><i class="fa fa-angle-right"></i>Lacoste</a></li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=9"><i class="fa fa-angle-right"></i>Bape</a></li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=18"><i class="fa fa-angle-right"></i>Herschel</a></li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=10"><i class="fa fa-angle-right"></i>Vans</a></li>
                                                            </li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=24"><i class="fa fa-angle-right"></i>Champion</a></li>
                                                            </li>
                                                            <li><a href="/shopping_catalog?t_brand_id[]=19"><i class="fa fa-angle-right"></i>Under Armour</a></li>
                                                            </li>                                          
                                                
                                            </ul>
                                        </div>
                                    </li>
                                  
                                </ul>
                                
                                <ul>
                                    @if (Auth::guest())
                                     <li><a href="/login">Login/Register</a></li>


                                      @elseif ( Auth::user()->is_admin()) 

                                     <li class="simple-list">
                                        <a>{{Auth::user()->name}}</a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">
                                                <li><a href="/welcome"><i class="fa fa-angle-right"></i>ADMIN PANEL</a></li>  

                                                <li><a href="/profile"><i class="fa fa-angle-right"></i>MY ACCOUNT</a></li>  
                                                <li><a href="/orderhistory"><i class="fa fa-angle-right"></i>ORDER HISTORY</a></li>  
                                                <li><a href="/logout"><i class="fa fa-angle-right"></i>LOGOUT</a></li>  
                                            </ul>
                                        </div>
                                    </li>   




                                     @else 

                                     <li class="simple-list">
                                        <a>{{Auth::user()->name}}</a><i class="fa fa-chevron-down"></i>
                                        <div class="submenu">
                                            <ul class="simple-menu-list-column">
                                                <li><a href="/profile"><i class="fa fa-angle-right"></i>MY ACCOUNT</a></li>
                                                <li><a href="/orderhistory"><i class="fa fa-angle-right"></i>ORDER HISTORY</a></li>  
                                                <li><a href="/logout"><i class="fa fa-angle-right"></i>LOGOUT</a></li>  
                                            </ul>
                                        </div>
                                    </li>     
  
                                    @endif 

                                    
                                </ul>
                                <div class="clear"></div>  

                                <a class="fixed-header-visible additional-header-logo"><img src="{{ asset ('mango/img/logo-9.png') }}" alt=""/></a>
                            </nav>
                            
                        </div>
                    </div>
                </header>
                <div class="clear"></div>
            </div>