<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    

    <link href="{{ asset ('mango/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset ('mango/css/idangerous.swiper.css') }}" rel="stylesheet" type="text/css" />  
    <link href="{{ asset ('mango/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700%7CDancing+Script%7CMontserrat:400,700%7CMerriweather:400,300italic%7CLato:400,700,900' rel='stylesheet' type='text/css' />
    
    <link href="{{ asset ('mango/css/style.css') }}" rel="stylesheet" type="text/css" />
    <!--[if IE 9]>
        <link href="css/ie9.css" rel="stylesheet" type="text/css" />
    <![endif]-->
   
    <link rel="shortcut icon" href=" {{ asset ('mango/img/favicon-6.ico') }}" />
    <title>Mango - Shop</title>
</head>