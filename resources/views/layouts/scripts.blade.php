<!-- BEGIN VENDOR JS -->

    <script src=" {{URL::asset('bundle/demo/html/assets/plugins/pace/pace.min.js')}}" type="text/javascript')}}"></script>

   
    <script src="{{URL::asset('bundle/demo/html/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>

    <script src="{{ asset ('bundle/demo/html/assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('bundle/demo/html/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript')}}"></script>

    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-bez/jquery.bez.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/classie/classie.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script> 
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/jquery-autonumeric/autoNumeric.js')}}"></script>
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/dropzone/dropzone.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset ('bundle/demo/html/assets/plugins/jquery-inputmask/jquery.inputmask.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/summernote/js/summernote.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/moment/moment.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-typehead/typeahead.bundle.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-typehead/typeahead.jquery.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/plugins/handlebars/handlebars-v4.0.5.js')}}"></script> 
    
    <script src="{{URL::asset('assets/bundle/demo/html/assets/js/form_elements.js')}}" type="text/javascript"></script>
   
    <script src="{{ asset('bundle/demo/html/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
   
    <script src="{{ asset ('bundle/demo/html/pages/js/pages.min.js')}}"></script>
    <script src="{{ asset ('bundle/demo/html/assets/js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/js/gallery.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/js/scripts.js')}}" type="text/javascript"></script>
    
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="{{ asset ('bundle/demo/html/assets/js/demo.js')}}" type="text/javascript"></script>
    <script src="{{ asset ('bundle/demo/html/assets/js/scripts.js')}}" type="text/javascript"></script>

    <script src="{{ asset('js/chosen/chosen.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/chosen/chosen.jquery.min.js')}}"  type="text/javascript"></script>
    <script src="{{ asset('js/chosen/chosen.proto.js')}}"  type="text/javascript"></script>
    <script src="{{ asset('js/chosen/chosen.proto.min.js')}}"  type="text/javascript"></script>

    <link href="{{ asset('css/chosen/chosen.css')}}" rel="stylesheet">
    <link href="{{ asset('css/chosen/chosen.min.css')}}" rel="stylesheet">
   
    




