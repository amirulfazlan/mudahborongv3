
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->

          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline">
                <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png') }}" width="78" height="22">
              </div>
            </div>
          </div>

          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->

          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
              <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png') }}" width="78" height="22">
            </div>
            <!-- START NOTIFICATION LIST -->
            
            <!-- END NOTIFICATIONS LIST -->
            <a href="#" class="search-link" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> </div>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->

          
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
               @if (Auth::guest())
               <span class="semi-bold">GUEST</span>
               @else  
              <span class="semi-bold">{{Auth::user()->name}}</span>
              @endif 
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="{{ asset ('bundle/demo/html/assets/img/profiles/avatar.jpg') }}" alt="" data-src="{{ asset ('bundle/demo/html/assets/img/profiles/avatar.jpg') }}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/profiles/avatar_small2x.jpg') }}" width="32" height="32">
            </span>
              </button>
              @if (Auth::guest())
              
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="{{ route('login') }}"><i class="pg-settings_small"></i>Login</a>
                </li>
                <li><a href="{{ route('register') }}"><i class="pg-outdent"></i> Register As Wholesaler</a>
                </li>
                <li><a href="#"><i class="pg-signals"></i> Help</a>
                </li>

                

              </ul>  
              
              @else
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="{{ url('/profile') }}"><i class="pg-settings_small"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="pg-outdent"></i> Feedback</a>
                </li>
                <li><a href="#"><i class="pg-signals"></i> Help</a>
                </li>

                <li class="bg-master-lighter">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form> 
                  </a>
                </li>

              </ul>
              @endif
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <!-- START PAGE CONTENT WRAPPER -->