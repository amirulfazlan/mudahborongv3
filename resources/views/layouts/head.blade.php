<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>MudahBorong Online</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <link rel="apple-touch-icon" href="{{URL::asset('assets/bundle/demo/html/pages/ico/60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/bundle/demo/html/pages/ico/76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::asset('assets/bundle/demo/html/pages/ico/120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::asset('assets/bundle/demo/html/pages/ico/152.png')}}">
    <link rel="icon" type="image/x-icon" href="{{URL::asset('assets/bundle/demo/html/favicon.ico')}}" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />


    
    <link href="{{URL::asset('bundle/demo/html/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset ('bundle/demo/html/assets/plugins/bootstrapv3/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />

      <link href="{{ asset('bundle\demo\html\assets\plugins\bootstrap3-wysihtml5\bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-tag/bootstrap-tagsinput.css')}}" rel="stylesheet" type="text/css" />
    
    <link href="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ asset ('bundle/demo/html/assets/plugins/summernote/css/summernote.css')}}" rel="stylesheet" type="text/css" media="screen">
    
    <link href="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" media="screen">
    <link href="{{ asset ('bundle/demo/html/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" media="screen">

    <link href="{{ asset ('bundle/demo/html/assets/plugins/owl-carousel/assets/owl.carousel.css')}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/jquery-nouislider/jquery.nouislider.css')}}" rel="stylesheet" type="text/css" media="screen" />
    

    <link href="{{ asset ('bundle/demo/html/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset ('bundle/demo/html/pages/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('bundle/demo/html/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}" rel="stylesheet" type="text/css" media="screen" />

    <link href="{{ asset ('bundle/demo/html/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset ('bundle/demo/html/pages/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
    <![endif]-->
 
    
   

    

  </head>