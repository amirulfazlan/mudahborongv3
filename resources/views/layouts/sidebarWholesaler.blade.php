<!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-40"><img src="{{ asset ('bundle/demo/html/assets/img/demo/social_app.svg') }}" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-10"><img src="{{ asset ('bundle/demo/html/assets/img/demo/email_app.svg') }}" alt="socail">
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-40"><img src="{{ asset ('bundle/demo/html/assets/img/demo/calendar_app.svg') }}" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-10"><img src="{{ asset ('bundle/demo/html/assets/img/demo/add_more.svg') }}" alt="socail">
            </a>
          </div>
        </div>
      </div>
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="assets/img/logo_white.png" alt="logo" class="brand" data-src="{{ asset ('bundle/demo/html/assets/img/logo_white.png') }}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_white_2x.png') }}" width="78" height="22">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
          </button>
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{url('/')}}" class="detailed">
              <span class="title">Home</span>
              
            </a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>

          <li class="">
            @if (Auth::check())
            <a href="javascript:;"><span class="title">{{Auth::user()->name}}</span>
            <span class="arrow"></span></a>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/profile')}}">User Profile</a>
                <span class="icon-thumbnail">P</span>
              </li>
              <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="clearfix"><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>Logout</a>
                <span class="icon-thumbnail">LC</span>
              </li>              
              
            </ul>
            @else
            <a href="javascript:;"><span class="title">Guest</span>
            <span class="arrow"></span></a>
            
            <span class="icon-thumbnail"><i class="pg-grid"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/login')}}">Login</a>
                <span class="icon-thumbnail">P</span>
              </li>
              <li>
                <a href="{{url('/register')}}">Register</a>
                <span class="icon-thumbnail">LC</span>
              </li>              
              
            </ul>
            @endif
          </li>






      <li class="">
            <a href="javascript:;"><span class="title">Catalog</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-bag"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/shopping_catalog')}}">Shopping Catalog</a>
                <span class="icon-thumbnail">&#9794</span>
              </li>


      
             

             

            </ul>
          </li>

          <li class="">
            <a href="javascript:;"><span class="title">Shopping Cart ( Temporary nanti tukar tau boboy)</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-shopping_cart"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/cart')}}">Shopping Cart</a>
                <span class="icon-thumbnail">CA</span>
              </li>


        
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->