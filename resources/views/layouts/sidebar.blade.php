  <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">


        <img src="{{URL::asset('bundle/demo/html/assets/img/logo_white.png')}}" alt="logo" class="brand" data-src="{{ asset ('bundle/demo/html/assets/img/logo_white.png') }}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_white_2x.png') }}" width="78" height="22">
        <div class="sidebar-header-controls">

          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 ">
            <a href="{{url('/')}}" class="detailed">
              <span class="title">Home</span>
              
            </a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>

          <li class="">
            @if (Auth::check())
            <a href="javascript:;"><span class="title">{{Auth::user()->name}}</span>
            <span class="arrow"></span></a>
            @else
            <a href="javascript:;"><span class="title">Guest</span>
            <span class="arrow"></span></a>
            @endif
            <span class="icon-thumbnail"><i class="pg-grid"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/profile')}}">User Profile</a>
                <span class="icon-thumbnail">P</span>
              </li>
              <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="clearfix"><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>Logout</a>
                <span class="icon-thumbnail">LC</span>
              </li>              
              
            </ul>
          </li>

          <li class="">
            <a href="javascript:;"><span class="title">Customer</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-grid"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/customer')}}">List Customer</a>
                <span class="icon-thumbnail">LC</span>
              </li>
              
            </ul>
          </li>

          <li class="">
            <a href="javascript:;"><span class="title">Order History</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-grid"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/admin-orderhistory')}}">View Order History</a>
                <span class="icon-thumbnail">O</span>
              </li>
              
            </ul>
          </li>

 

          <li class="">
            <a href="javascript:;"><span class="title">Product</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-menu_lv"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/product')}}">Add Product</a>
                <span class="icon-thumbnail">P</span>
              </li>

              <li>
                <a href="{{url('/product_price')}}">Add Product Color</a>
                <span class="icon-thumbnail">PP</span>
              </li>

              <li>
                <a href="{{url('/product_photo')}}">Add Product Image</a>
                <span class="icon-thumbnail">PI</span>
              </li>

             

            </ul>
          </li>


           <li class="">
            <a href="javascript:;"><span class="title">Variation</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-form"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/brand')}}">Add Brand</a>
                <span class="icon-thumbnail">B</span>
              </li>

              <li>
                <a href="{{url('/category')}}">Add Category</a>
                <span class="icon-thumbnail">C</span>
              </li>

              <li>
                <a href="{{url('/size_type')}}">Add Size Type</a>
                <span class="icon-thumbnail">ST</span>
              </li>

              <li>
                <a href="{{url('/size_item')}}">Add Size Item</a>
                <span class="icon-thumbnail">SI</span>
              </li>

              <li>
                <a href="{{url('/moq_type')}}">Add Moq Type</a>
                <span class="icon-thumbnail">MT</span>
              </li>

               <li>
                <a href="{{url('/moq_item')}}">Add Moq Item</a>
                <span class="icon-thumbnail">MI</span>
              </li>


            </ul>
          </li>


           <li class="">
            <a href="javascript:;"><span class="title">Service</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="fa fa-star-o"></i></span>
            <ul class="sub-menu">

              <li>
                <a href="{{url('/newtracking')}}">Tracking</a>
                <span class="icon-thumbnail">T</span>
              </li>  

              <li>
                <a href="{{url('/prine')}}">Prime</a>
                <span class="icon-thumbnail">P</span>
              </li>   

              <li>
                <a href="{{url('/currency')}}">Currency</a>
                <span class="icon-thumbnail">C</span>
              </li> 

               <li>
                <a href="{{url('/margin')}}">Margin </a>
                <span class="icon-thumbnail">MG</span>
              </li>    

            </ul>
          </li>

      <li class="">
            <a href="javascript:;"><span class="title">Catalog</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-bag"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/shopping_catalog')}}">Shop</a>
                <span class="icon-thumbnail">&#9794</span>
              </li>

            </ul>
          </li>

          <li class="">
            <a href="javascript:;"><span class="title">Shopping Cart ( Temporary nanti tukar tau boboy)</span>
            <span class="arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-shopping_cart"></i></span>
            <ul class="sub-menu">
              <li>
                <a href="{{url('/cart')}}">Shopping Cart</a>
                <span class="icon-thumbnail">CA</span>
              </li>


        
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->