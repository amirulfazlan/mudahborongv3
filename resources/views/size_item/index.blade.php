
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Size Item</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->


           <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Size Item<span class="semi-bold"></span></h5>
                    <div class="panel-heading">     

                        </div>
                  </div>
                  <div class="modal-body">

                <form action="/size_item" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}

      
     <div class="form-group row">
      <div class="form-group {{ $errors->has('st_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size Name</label>
        <div class="col-sm-5">
          <div class="dropdown">
          <select id="st_id" name="st_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Size Name</option>
                <div class="dropdown-divider"></div>
                @foreach($size_type as $post2)
                <option value="{{$post2['id']}}">{{$post2['size_name']}}</option>
                @endforeach
              </ul>
            </select>
            @if ($errors->has('st_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('st_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('st_item') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Item Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="st_item" name="st_item" required> 
          @if ($errors->has('st_item'))
                  <span class="help-block">
                      <strong>{{ $errors->first('st_item') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="row">
                      
                      <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right">

                        <input type="submit" name="submit" value = "Create Size Item"  class="btn btn-primary btn-block m-t-5 ">

                       <!--  <button type="button" class="btn btn-primary btn-block m-t-5 ">Pay Now</button> -->

                      </div>
                    </div>


    
    </form>

      
      
                    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel panel-transparent">
              <div class="panel-heading">
                <form method="get" action="/size_item">

              <div class="form-group row">
              <div class="form-group {{ $errors->has('search_name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Size Type Name</label>
              <div class="col-sm-5">
              <input type="text" name="search_name" class="form-control">
              @if ($errors->has('search_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('search_name') }}</strong>
                  </span>
              @endif


            </div>

             <button type="submit" class="btn btn-primary">Search</button>
            
            </div>

           

            </div>

          </form>

            </br>

                <div class="panel-title">List Of Size Type
                </div>
                <div class="btn-group pull-right m-b-10">
                <button class="btn btn-primary" id="btnToggleSlideUpSize" >Add New Size Type</button>
                    
                 
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                
                 </br>

                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Size Type</th>
                        <th style="width:20%">Item Name</th>
                        
                        <th style="width:15%">Edit</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                       @foreach($size_item as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        <td class="v-align-middle">
                          <p>{{ $post->stype->size_name}}</p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{$post['st_item']}}</p>
                        </td>
                        
                        <td class="v-align-middle">

                         <a href="{{action('Size_ItemsController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a>

                        </td>
                        <td class="v-align-middle">
                          <form action="{{action('Size_ItemsController@destroy', $post['id'])}}" method="post">
                          {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                          </form>
                        </td>
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>
                  
                  {{$size_item->links() }}

                </div>
              </div>
            </div>


            
                  
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>