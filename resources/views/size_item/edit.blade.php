
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Size Item</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Size Item</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

       <form method="post" action="{{action('Size_ItemsController@update', $id)}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}
     
      
     <div class="form-group row">
      <div class="form-group {{ $errors->has('st_id') ? ' has-error' : '' }}">
        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size Type</label>
        <div class="col-sm-5">
          <div class="dropdown">
          <select id="st_id" name="st_id" class="form-control">
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="{{$size_item->st_id}}">{{$size_item->stype->size_name}}</option>
                <div class="dropdown-divider"></div>
                @foreach($size_type as $post)
                <option value="{{$post['id']}}">{{$post['size_name']}}</option>
                @endforeach
              </ul>
            </select>
            @if ($errors->has('st_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('st_id') }}</strong>
                    </span>
                @endif
          </div>
        </div>
      </div>
      </div>



      <div class="form-group row">
      <div class="form-group {{ $errors->has('st_item') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Size Item</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="st_item" name="st_item" value="{{$size_item->st_item}}"> 
          @if ($errors->has('st_item'))
                  <span class="help-block">
                      <strong>{{ $errors->first('st_item') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>


      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>