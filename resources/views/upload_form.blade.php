<form action="/upload" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    Product name:
    <br />
                 <div class="dropdown">
             <select id="brand_id" name="brand_id" class="form-control" required>
              <span class="caret"></span>
              <ul class="dropdown-menu">
                <option value="">Choose Brand</option>
                @foreach($brand as $post2)
                <option value="{{$post2['id']}}">{{$post2['brand_name']}}</option>
                @endforeach    
              </ul>
            </select>
            </div>
    <br /><br />
    Product photos (can attach more than one):
    <br />
    <input type="file" name="photos[]" multiple />
    <br /><br />
    <input type="submit" value="Upload" />
</form>