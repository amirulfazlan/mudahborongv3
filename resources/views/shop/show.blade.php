<!DOCTYPE html>
<html>

  @include('layouts.head')

  <!-- Product Catalog -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset ('p_catalog/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset ('p_catalog/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset ('p_catalog/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset ('p_catalog/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">

     <script type="text/javascript">

    function sum() {         
            var qty = document.getElementById('cart_quantity').value;
            var moq1 = document.getElementById('moq1').value;
            var moq2 = document.getElementById('moq2').value;
            var moq3 = document.getElementById('moq3').value;
            var price1 = document.getElementById('price1').value;
            var price2 = document.getElementById('price2').value;
            var price3 = document.getElementById('price2').value;
            
            if ( qty >= moq1  ){

                var result = parseInt(qty) * parseFloat(price1)
            }

            else if ( qty >= moq2 ) {
                var result = parseInt(qty) * parseFloat(price2)
            }

            else if ( qty >= moq3 ) {
                var result = parseInt(qty) * parseFloat(price3)
            }
            
            if (!isNaN(result)) {
                document.getElementById('txt3').value = result.toFixed(2);
                $(".name2").html(result.toFixed(2));
            }
        }

                function updateTotal() {
    var total = 0;//
    var list = document.getElementsByClassName("input");
    var values = [];
    for(var i = 0; i < list.length; ++i) {
        values.push(parseFloat(list[i].value));
    }
    total = values.reduce(function(previousValue, currentValue, index, array){
        return previousValue + currentValue;
    });
    document.getElementById("grandtotal").value = total;    
}

</script>


  


  <body class="fixed-header menu-pin">


  @include('layouts.sidebar')

  
  <div class="page-container ">


     @include('layouts.topbar')

      
       <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel-heading">     

                        </div>

    <div id="all">

        <div id="content">
            <div class="container-fluid">

                <div class="col-md-11">
                    <ul class="breadcrumb">
                        <li><p><a href="{{url('/')}}">MudahBorong</a></p>
                        </li>
                        <!-- <li><a href="#">Ladies</a>
                        </li>
                        <li><a href="#">Tops</a>
                        </li> -->
                        <li>{{ $product['p_name'] }}</li>
                    </ul>

                </div>


                <div class="col-md-11">

                   <div class="row" id="productMain">
                  
                  <div class="col-sm-6">
                     <div id="mainImage">               
                        <img src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" class="img-responsive">
                      </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="box">

                       <h1 class="text-center">{{ $product['p_name'] }}</h1>

                        @foreach ($cur as $yuan)
                                    @foreach ($margin as $mar)
                                    @if( $mar['id'] == $product['margin_id'] )
                                    @foreach ($moq_item as $moq)
                                    @if( $moq['moqty_id'] == $product['moqty_id'] ) 
                                            
                                    <?php $admin_price = ( $product['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                                          $whole_price1 =  $admin_price + $mar['mar_whole1'] ;



                                          $admin_price = ( $product['base_price2'] + $mar['mar_admin2'] ) /  $yuan['cur_value'];
                                          $whole_price2 =  $admin_price + $mar['mar_whole2'] ;

                                          $admin_price = ( $product['base_price2'] + $mar['mar_admin3'] ) /  $yuan['cur_value'];
                                          $whole_price3 =  $admin_price + $mar['mar_whole3'] ;

                                           $round1= $whole_price1;
                                           $round2= $whole_price2;
                                           $round3= $whole_price3;

                                    ?>

                                    <center><p style ="color:red">MoQ {{$moq['min_unit1']}} more  =RM {{ number_format($round1, 2) }}</p></center>

                                     <input type="hidden" id="moq1" name="moq1" class="form-control" value="{{$moq['min_unit1']}}" />
                                     <input type="hidden" id="price1" name="price1" class="form-control" value="{{ number_format ($round1, 2) }}" />


                                     <center><p style ="color:red"> MoQ {{$moq['min_unit2']}} more  =RM {{ number_format($round2, 2) }}</p></center>

                                      <input type="hidden" id="moq2" name="moq2" class="form-control" value="{{$moq['min_unit2']}}" />
                                      <input type="hidden" id="price2" name="price2" class="form-control" value="{{ number_format($round2, 2) }}" />

                                      <center><p style ="color:red"> MoQ {{$moq['min_unit3']}} more  =RM {{ number_format($round3, 2) }}</p></center>

                                     <input type="hidden" id="moq3" name="moq3" class="form-control" value="{{$moq['min_unit3']}}" />
                                     <input type="hidden" id="price3" name="price3" class="form-control" value="{{ number_format($round3, 2) }}" />

                                       <p class="price" style ="color:red"> RM <span class="name2">0.00</span><br /></p> 


                                               
                                      @endif 
                                      @endforeach 
                                      @endif 
                                      @endforeach
                                      @endforeach

                                       <p class="goToDescription"><a href="#details" class="scroll-to">Scroll to product details, material & care and sizing</a></p>

                                    <form action="/addProduct" method="POST" >
                                    {{ csrf_field() }}

                                    <input type="hidden" name="id"  value="<?php echo $id; ?>">

                                    <div class="form-group row">
                                    <div class="form-group {{ $errors->has('cart_color') ? ' has-error' : '' }}">
                                    <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Color</label>
                                    <div class="col-sm-5">
                                    <div class="dropdown">
                                    <select id="cart_color" name="cart_color" class="form-control" required>
                                    <span class="caret"></span>
                                    <ul class="dropdown-menu">
                                    <option value="">Choose Color </option>
                                    <div class="dropdown-divider"></div>

                                     @foreach($pro_var as $color)
                                     @if( $color['p_id'] == $product['id'] ) 
                                     <option value="{{$color['id']}}">{{$color['p_color']}}</option>
                                     @endif
                                     @endforeach
                                     </ul>
                                     </select>
                                     @if ($errors->has('cart_color'))
                                     <span class="help-block">
                                     <strong>{{ $errors->first('cart_color') }}</strong>
                                     </span>
                                     @endif
                                     </div>
                                     </div>
                                     </div>
                                     </div>

                                 <div class="form-group row">
                                 <div class="form-group {{ $errors->has('cart_size') ? ' has-error' : '' }}">
                                 <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size</label>
                                 <div class="col-sm-5">
                                 <div class="dropdown">
                                 <select id="cart_size"  name="cart_size" class="form-control" required>
                                 <span class="caret"></span>
                                 <ul class="dropdown-menu">
                                 <option value="">Choose Size </option>
                                 <div class="dropdown-divider"></div>

                                 @foreach($size_item as $post2)
                                 @if( $post2['st_id'] == $product['st_id'] ) 
                                 <option value="{{$post2['st_item']}}">{{$post2['st_item']}}</option>
                                 @endif
                                 @endforeach
                                 </ul>
                                 </select>
                                 @if ($errors->has('cart_size'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('cart_size') }}</strong>
                                 </span>
                                 @endif
                                 </div>
                                 </div>
                                 </div>
                                 </div>

                                 <div class="form-group row">
                                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Quantity </label>
                                <div class="col-sm-5">
                                <input type="text" id="cart_quantity" name="cart_quantity" class="form-control" min="5" onchange="sum();"/>                            
                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif
                                </div>
                                </div>
                                </div>



                                  <p class="text-center buttons">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Add to cart</button> 
                                  </p>



                                    </form>
                                
                    </div>

                    <div class="row" id="thumbs">

                      <div class="col-xs-4">
                                    <a href="{{ asset ('images/main_product/'.$product['back_image']) }}" class="thumb">
                                        <img src="{{ asset ('images/main_product/'.$product['back_image']) }}" alt="" class="img-responsive">
                                    </a>
                                </div>

                      @foreach($product->variation as $colors)

                      <div class="col-xs-4">
                          <a href="{{ asset ('images/product/'.$colors['main_image'])}}" class="thumb">
                              <img src="{{ asset ('images/product/'.$colors['main_image'])}}" alt="" class="img-responsive">
                          </a>
                      </div>
                      @endforeach 


                   
                    </div>

                  </div>
                  <!-- /.col-sm-6 -->
                </div>
                <!-- /.productMain -->

                 <div class="box" id="details">

                  <p>
                            <h4>Product details</h4>
                            <p>{{ $product['description'] }}</p>
                            <h4>Material</h4>
                            <ul>
                                <li>{{ $product->product_mat['material_name'] }}</li>
                               
                            </ul>

                             <h4>Color Available</h4>
                             <ul>
                                 
                                 @foreach($pro_var as $color)  
                                 @if( $color['p_id'] == $product['id'] ) 
                                <li>{{$color['p_color']}}</li>
                                
                                 @endif
                                 @endforeach
                            </ul>


                    
                            <h4>Size & Fit</h4>
                            <ul>
                                <li>Type of Size:  @foreach ($size_type as $name)
                                    @if( $name['id'] == $product['st_id'] ) 
                                    {{ $name['size_name'] }}
                                    @endif 
                                    @endforeach
                                    </li>

                                 <li> Size Available:  @foreach ($size_item as $item)

                                    @if( $item['st_id'] == $product->product_sitem['st_id'] ) 
                                    {{ $item['st_item'] }}
                                    @endif 
                                    @endforeach
                                    
                                   </li>

                            </ul>


                             <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/shop_men')}}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                                </div>
                            
                            </div>
       
                 </div>
                  <!-- /.details -->  

                </div>
                <!-- /.col-md-9 -->

               

                    
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->
    </div>




            <!-- END PLACE PAGE CONTENT HERE -->
        
          <!-- END CONTAINER FLUID -->
           </div>  
  

        <!-- END PAGE CONTENT --> 
        <div class="panel-heading">     

                        </div>

       @include('layouts.footer')

      @include('layouts.leftsidebar')
  </div>
      @include('layouts.scripts')

    <script src="{{ asset ('p_catalog/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/waypoints.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/modernizr.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/front.js') }}"></script>


  </body>
</html>
