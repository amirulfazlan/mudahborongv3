<!DOCTYPE html>
<html>

  @include('layouts.head')

  <!-- Product Catalog -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset ('p_catalog/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset ('p_catalog/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset ('p_catalog/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset ('p_catalog/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">

  <body class="fixed-header menu-pin">
   

  @if (Auth::check()) 
    @if ( Auth::user()->is_Admin() )
        @include('layouts.sidebar')
        @else
        @include('layouts.sidebarWholesaler')
    @endif
  @else
    @include('layouts.sidebarWholesaler')
  @endif



  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


              <!-- *** NAVBAR END *** -->

    <div id="all">

        <div id="content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}">MudahBorong</a>
                        </li>
                        <li>Product Catalog</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                <li>
                                    <a href="category.html">Women <span class="badge pull-right">42</span></a>
                                    <ul>

                                        <li><a href="category.html">View All</a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li>
                                    <a href="category.html">Shop By Category <span class="badge pull-right">11</span></a>
                                    <ul>
                                       
                                        @foreach ($cat as $cats)
                                        @if ($cats['gender_id'] == 2)
                                        <li><a href="/shop_women?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                        </li>
                                        @endif
                                        @endforeach

                                    </ul>
                                </li>
                               
                                

                            </ul>

                        </div>
                    </div>

                    <!-- *** MENUS AND FILTERS END *** -->
                </div>

                <div class="col-md-9">
                    <div class="box">
                        <h1>Women</h1>
                        <p>In our Women department we offer wide selection of the best products we have found and carefully selected worldwide.</p>
                    </div>

                    <div class="box info-bar">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 products-showing">
                                Showing <strong>12</strong> of <strong>25</strong> products
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-number">
                                                <strong>Show</strong>  <a href="#" class="btn btn-default btn-sm btn-primary">12</a>  <a href="#" class="btn btn-default btn-sm">24</a>  <a href="#" class="btn btn-default btn-sm">All</a> products
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong>Sort by</strong>
                                                <select name="sort-by" class="form-control">
                                                    <option>Price</option>
                                                    <option>Name</option>
                                                    <option>Sales first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                     <div class="row products">

                     @foreach($product as $post)

                        <div class="col-md-3 col-sm-4">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="{{action('ShopController@show', $post['id'])}}">
                                                <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="{{ $post['main_image'] }}" class="img-responsive"/></a>
                                        </div>
                                        <div class="back">
                                            <a href="{{action('ShopController@show', $post['id']) }}">
                                               <img src="{{ 'images/main_product/'.$post['back_image'] }}" alt="{{ $post['back_image'] }}" class="img-responsive"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{action('ShopController@show', $post['id'])}}" class="invisible">
                                    <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="{{ $post['main_image'] }}" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="detail.html">{{ $post['p_name']}}</a></h3>
                                         @foreach ($cur as $yuan)

                                         @foreach ($margin as $mar)
 
                                         @if( $mar['id'] == $post['margin_id'] )

                                         @foreach ($moq_item as $moq)

                                         @if( $moq['moqty_id'] == $post['moqty_id'] )
       
      
                                  


                                         <?php $admin_price = ( $post['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                               $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                               $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;
                                         ?>

                                          <sup class="text"> FROM </sup> <p class="price" style ="color:red">  RM {{ number_format($rounding, 2) }}</p>

                                         
                                         @endif 
                                         @endforeach 
       
                                         @endif 
       
                                          @endforeach
     
                                          @endforeach
                                    
                                    <p class="buttons">
                                       <a href="{{action('ShopController@show', $post['id'])}}" class="btn btn-default">View detail</a>
                                        
                                    </p>
                                </div>
                                <!-- /.text -->
                            </div>
                            <!-- /.product -->
                        </div> 

                        @endforeach

                    </div>
                    <!-- /.products -->

                 <!--  {{ $product->render() }} -->

                   

                    <!-- /.products -->

                    <div class="pages">

                        <p class="loadMore">
                            <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>

                        <ul class="pagination">
                            <li><a href="#">&laquo;</a>
                            </li>
                            <li class="active"><a href="#">1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li><a href="#">4</a>
                            </li>
                            <li><a href="#">5</a>
                            </li>
                            <li><a href="#">&raquo;</a>
                            </li>
                        </ul>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


             <!-- END PLACE PAGE CONTENT HERE -->
            <div class="panel-heading">     

                        </div>
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
      </div>
        <div class="panel-heading">     

                        </div>

       @include('layouts.footer')

      @include('layouts.leftsidebar')
  </div>
      @include('layouts.scripts')

    <script src="{{ asset ('p_catalog/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/waypoints.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/modernizr.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/front.js') }}"></script>


  </body>
</html>
