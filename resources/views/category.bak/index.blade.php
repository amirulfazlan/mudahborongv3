
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Category</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
           

           <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Category <span class="semi-bold"></span></h5>

                    <div class="panel-heading">     

                        </div>
                    
                  </div>
                  <div class="modal-body">

               <form action="/category" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}


              <div class="form-group row">
              <div class="form-group {{ $errors->has('category_product') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Category</label>
              <div class="col-sm-5">
              <input type="text" name="category_product" class="form-control">
              @if ($errors->has('category_product'))
                  <span class="help-block">
                      <strong>{{ $errors->first('category_product') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

            <div class="form-group row">
            <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right"> 
            <input type="submit" name="submit" value = "Create Category"  class="btn btn-primary btn-block m-t-5 ">
            </div>
            </div>      

             </form>
    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->







          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <link href="{{asset('dist/css/bootstrap.min.css')}}" rel="stylesheet">

                <div class="col-lg-12">
                    <h1 class="page-header">List Of Category</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>


  <div class="container">
    <table class="table table-striped">
      <thead>

       <tr><td colspan="12" align="right"> 

      <button class="btn btn-primary" id="btnToggleSlideUpSize" >Add New Category</button>

      </td></tr>

      <tr>
    
        <th>Bil</th>
        <th>Name</th>
        
        <th colspan="1">Action</th>
      </tr>
      </thead>
      <tbody>

      <?php $i = 0 ?>
      @foreach($cat as $post)
      <?php $i++ ?>
       <tr>

            <td>{{ $i}}</td>
            <td>{{$post['category_product']}}</td>
            
             <td><a href="{{action('CategoryController@edit', $post['id'])}}" class="btn btn-warning">Edit <span class="glyphicon glyphicon-pencil"></span></a></td>

              <td align="text-left">
                <form action="{{action('CategoryController@destroy', $post['id'])}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete <span class="glyphicon glyphicon-trash"></span></button>
              </form>
            </td>

          </tr>
            @endforeach

      </tbody>
    </table>

  </div>
  </div>
  </div>
  </div>
                  
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>