@include('layouts.obaju.modal')
@include('layouts.obaju.nav')
@include('layouts.obaju.head')

<div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li><a href="#">Ladies</a>
                        </li>
                        <li><a href="#">Tops</a>
                        </li>
                        <li>White Blouse Armani</li>
                    </ul>

                </div>

                <div class="col-md-3">
                    <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                <li>
                                    <a href="{{ url('/men') }}">Men <span class="badge pull-right">42</span></a>
                                    <ul>
                                       @foreach ($cat as $cats)
                                        @if ($cats['gender_id'] == 1)
                                        <li><a href="/men?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/womwn') }}">Women <span class="badge pull-right">123</span></a>
                                    <ul>
                                        @foreach ($cat as $cats)
                                        @if ($cats['gender_id'] == 2)
                                        <li><a href="/women?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                              

                            </ul>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Brands <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    
                                   @foreach ($brand as $a)
                                  <div class="checkbox">
                                        <label>
                                            <input type="checkbox">{{$a['brand_name']}}
                                        </label>
                                    </div>
                                     @endforeach
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Colours <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour white"></span> White (14)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour blue"></span> Blue (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour green"></span> Green (20)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour yellow"></span> Yellow (13)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour red"></span> Red (10)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <!-- *** MENUS AND FILTERS END *** -->

                    
                </div>

                <div class="col-md-9">

                    <div class="row" id="productMain">
                        <div class="col-sm-6">
                            <div id="mainImage">
                                <img src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" class="img-responsive">
                            </div>


                        </div>
                        <div class="col-sm-6">
                            <div class="box">
                                <h1 class="text-center">{{ $product['p_name'] }}</h1>

                                 @foreach ($cur as $yuan)
                                    @foreach ($margin as $mar)
                                    @if( $mar['id'] == $product['margin_id'] )
                                    @foreach ($moq_item as $moq)
                                    @if( $moq['moqty_id'] == $product['moqty_id'] ) 
                                            
                                    <?php $admin_price = ( $product['base_price1'] +  $mar['mar_admin1']) /  $yuan['cur_value'];
                                          $whole_price1 =  $admin_price + $mar['mar_whole1'] ;



                                          $admin_price = ( $product['base_price2'] + $mar['mar_admin2'] ) /  $yuan['cur_value'];
                                          $whole_price2 =  $admin_price + $mar['mar_whole2'] ;

                                          $admin_price = ( $product['base_price2'] + $mar['mar_admin3'] ) /  $yuan['cur_value'];
                                          $whole_price3 =  $admin_price + $mar['mar_whole3'] ;

                                           $round1= $whole_price1;
                                           $round2= $whole_price2;
                                           $round3= $whole_price3;

                                    ?>

                                    <center><p style ="color:red">MoQ {{$moq['min_unit1']}} more  =RM {{ number_format($round1, 2) }}</p></center>

                                     <input type="hidden" id="moq1" name="moq1" class="form-control" value="{{$moq['min_unit1']}}" />
                                     <input type="hidden" id="price1" name="price1" class="form-control" value="{{ number_format ($round1, 2) }}" />


                                     <center><p style ="color:red"> MoQ {{$moq['min_unit2']}} more  =RM {{ number_format($round2, 2) }}</p></center>

                                      <input type="hidden" id="moq2" name="moq2" class="form-control" value="{{$moq['min_unit2']}}" />
                                      <input type="hidden" id="price2" name="price2" class="form-control" value="{{ number_format($round2, 2) }}" />

                                      <center><p style ="color:red"> MoQ {{$moq['min_unit3']}} more  =RM {{ number_format($round3, 2) }}</p></center>

                                     <input type="hidden" id="moq3" name="moq3" class="form-control" value="{{$moq['min_unit3']}}" />
                                     <input type="hidden" id="price3" name="price3" class="form-control" value="{{ number_format($round3, 2) }}" />

                                      {{-- <p class="price" style ="color:red"> RM <span class="name2">0.00</span><br /></p> --}}


                                               
                                      @endif 
                                      @endforeach 
                                      @endif 
                                      @endforeach
                                      @endforeach

                                <p class="goToDescription"><a href="#details" class="scroll-to">Scroll to product details, material & care and sizing</a>
                                </p>
                                
                                <p class="text">

                                  <form action="/addProduct" method="POST" >
                                    {{ csrf_field() }}

                                    <input type="hidden" name="id"  value="<?php echo $id; ?>">

                                    <div class="form-group row">
                                    <div class="form-group {{ $errors->has('cart_color') ? ' has-error' : '' }}">
                                    <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Color</label>
                                    <div class="col-sm-5">
                                    <div class="dropdown">
                                    <select id="cart_color" name="cart_color" class="form-control" required>
                                    <span class="caret"></span>
                                    <ul class="dropdown-menu">
                                    <option value="">Choose Color </option>
                                    <div class="dropdown-divider"></div>

                                     @foreach($pro_var as $color)
                                     @if( $color['p_id'] == $product['id'] ) 
                                     <option value="{{$color['id']}}">{{$color['p_color']}}</option>
                                     @endif
                                     @endforeach
                                     </ul>
                                     </select>
                                     @if ($errors->has('cart_color'))
                                     <span class="help-block">
                                     <strong>{{ $errors->first('cart_color') }}</strong>
                                     </span>
                                     @endif
                                     </div>
                                     </div>
                                     </div>
                                     </div>

                                     <div class="form-group row">
                                 <div class="form-group {{ $errors->has('cart_size') ? ' has-error' : '' }}">
                                 <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size</label>
                                 <div class="col-sm-5">
                                 <div class="dropdown">
                                 <select id="cart_size"  name="cart_size" class="form-control" required>
                                 <span class="caret"></span>
                                 <ul class="dropdown-menu">
                                 <option value="">Choose Size </option>
                                 <div class="dropdown-divider"></div>

                                 @foreach($size_item as $post2)
                                 @if( $post2['st_id'] == $product['st_id'] ) 
                                 <option value="{{$post2['st_item']}}">{{$post2['st_item']}}</option>
                                 @endif
                                 @endforeach
                                 </ul>
                                 </select>
                                 @if ($errors->has('cart_size'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('cart_size') }}</strong>
                                 </span>
                                 @endif
                                 </div>
                                 </div>
                                 </div>
                                 </div>

                                 <div class="form-group row">
                                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Quantity </label>
                                <div class="col-sm-5">
                                <input type="text" id="cart_quantity" name="cart_quantity" class="form-control" min="5" onchange="sum();"/>                            
                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif
                                </div>
                                </div>
                                </div>

                                </p>

                               <p class="text-center buttons">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Add to cart</button> 
                                  </p>

                                    </form>

                            </div>

                            <div class="row" id="thumbs">
                                 <div class="col-xs-4">
                                    <a href="{{ asset ('images/main_product/'.$product['back_image']) }}" class="thumb">
                                        <img src="{{ asset ('images/main_product/'.$product['back_image']) }}" alt="" class="img-responsive">
                                    </a>
                                </div>

                      @foreach($product->variation as $colors)

                      <div class="col-xs-4">
                          <a href="{{ asset ('images/product/'.$colors['main_image'])}}" class="thumb">
                              <img src="{{ asset ('images/product/'.$colors['main_image'])}}" alt="" class="img-responsive">
                          </a>
                      </div>
                      @endforeach 

                            </div>
                        </div>

                    </div>


                    <div class="box" id="details">
                        <p>
                            <h4>Product details</h4>
                            <p>{{ $product['description'] }}</p>
                            <h4>Material </h4>
                            <ul>
                                <li>{{ $product->product_mat['material_name'] }}</li>
                                
                            </ul>
                            <h4>Color</h4>
                            <ul>
                                @foreach($pro_var as $color)
                                @if( $color['p_id'] == $product['id'] ) 
                                <li>{{$color['p_color']}}</li>
                                 @endif
                                 @endforeach
                            </ul>
                            
                            <h4>Size </h4>
                            <ul>
                               @foreach ($size_item as $item)
                               @if( $item['st_id'] == $product->product_sitem['st_id'] )  
                               <li>{{ $item['st_item'] }}</li>
                                @endif 
                                @endforeach
                
                                
                            </ul>

                           

                            <hr>
                            <div class="social">
                                <h4>Show it to your friends</h4>
                                <p>
                                    <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                                    <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                                </p>
                            </div>

                             <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/shop_men')}}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                                </div>
                            
                            </div>
                    </div>

              

                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


    


@include('layouts.obaju.footer')
@include('layouts.obaju.scripts')
