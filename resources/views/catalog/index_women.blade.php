@include('layouts.obaju.modal')
@include('layouts.obaju.nav_women')
@include('layouts.obaju.head')

 <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Men</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                <li>
                                    <a href="{{ url('/men') }}">Men <span class="badge pull-right">42</span></a>
                                    <ul>
                                        @foreach ($cat as $cats)
                                        @if ($cats['gender_id'] == 1)
                                        <li><a href="/men?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="active">
                                    <a href="{{ url('/women') }}">Women  <span class="badge pull-right">123</span></a>
                                    <ul>
                                        @foreach ($cat as $cats)
                                        @if ($cats['gender_id'] == 2)
                                        <li><a href="/women?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                </li>
                                
                            </ul>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Brands <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">

                                   @foreach ($brand as $a)
                                  <div class="checkbox">
                                        <label>
                                            <input type="checkbox">{{$a['brand_name']}}
                                        </label>
                                    </div>
                                     @endforeach
                                                                       
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Colours <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour white"></span> White (14)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour blue"></span> Blue (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour green"></span> Green (20)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour yellow"></span> Yellow (13)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour red"></span> Red (10)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <!-- *** MENUS AND FILTERS END *** -->

                    
                </div>

                <div class="col-md-9">
                    <div class="box">
                        <h1>Women</h1>
                        <p>In our women department we offer wide selection of the best products we have found and carefully selected worldwide.</p>
                    </div>

                    <div class="box info-bar">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 products-showing">
                                Showing <strong>12</strong> of <strong>25</strong> products
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-number">
                                                <strong>Show</strong>  <a href="#" class="btn btn-default btn-sm btn-primary">12</a>  <a href="#" class="btn btn-default btn-sm">24</a>  <a href="#" class="btn btn-default btn-sm">All</a> products
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong>Sort by</strong>
                                                <select name="sort-by" class="form-control">
                                                    <option>Price</option>
                                                    <option>Name</option>
                                                    <option>Sales first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row products">

                      @foreach($product as $post)

                        <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="{{action('CatalogController@show', $post['id'])}}">
                                                <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="{{action('CatalogController@show', $post['id'])}}">
                                                <img src="{{ 'images/main_product/'.$post['back_image'] }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{action('CatalogController@show', $post['id'])}}" class="invisible">
                                    <img src="{{ 'images/main_product/'.$post['main_image'] }}" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="{{action('CatalogController@show', $post['id'])}}">{{ $post['p_name']}}</a></h3>
                                    <p class="price"> 

                                        @foreach ($cur as $yuan)

                                         @foreach ($margin as $mar)
 
                                         @if( $mar['id'] == $post['margin_id'] )

                                         @foreach ($moq_item as $moq)

                                         @if( $moq['moqty_id'] == $post['moqty_id'] )
       
      
                                  


                                         <?php $admin_price = ( $post['base_price2'] +  $mar['mar_admin3']) /  $yuan['cur_value'];
                                               $whole_price =  $admin_price + $mar['mar_whole3'] ;
                                               $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;
                                         ?>

                                          <sup class="text"> FROM </sup> <p class="price" style ="color:red">  RM {{ number_format($rounding, 2) }}</p>

                                         
                                         @endif 
                                         @endforeach 
       
                                         @endif 
       
                                          @endforeach
     
                                          @endforeach</p>

                                    <p class="buttons">
                                        <a href="{{action('CatalogController@show', $post['id'])}}" class="btn btn-default">View detail</a>
                                        
                                    </p>
                                </div>
                                <!-- /.text -->
                            </div>
                            <!-- /.product -->
                        </div>

                         @endforeach
                      
                    </div>
                    <!-- /.products -->

                    <div class="pages">

                        <p class="loadMore">
                            <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>

                        <ul class="pagination">
                            <li><a href="#">&laquo;</a>
                            </li>
                            <li class="active"><a href="#">1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li><a href="#">4</a>
                            </li>
                            <li><a href="#">5</a>
                            </li>
                            <li><a href="#">&raquo;</a>
                            </li>
                        </ul>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->






@include('layouts.obaju.footer')
@include('layouts.obaju.scripts')
