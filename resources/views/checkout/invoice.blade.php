<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">Checkout - Delivery Method</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

         <div class = "all"> 
          <div id="content">
            <div class="container-fluid">



                <div class="col-md-9" id="checkout">

                    <div class="box">
                        <form method="post" action="checkout4.html">
                            <h1>Checkout - Payment method</h1>
                            <ul class="nav nav-pills nav-justified">
                                <li><a href="checkout1.html"><i class="fa fa-map-marker"></i><br>Address</a>
                                </li>
                                <li><a href="checkout2.html"><i class="fa fa-truck"></i><br>Delivery Method</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Payment Method</a>
                                </li>
                                <li class="active"><a href="checkout4.html"><i class="fa fa-eye"></i><br>Order Review</a>
                                </li>
                            </ul>

                            

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/cart')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Cart</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{url('/checkout-invoice')}}" type ="submit" class="btn btn-primary"><i class="fa fa-chevron-right"></i> Submit Order</a>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->



            </div>
            <!-- /.container -->
        </div>

            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>

      
      @include('layouts.scripts')

  </body>
</html>