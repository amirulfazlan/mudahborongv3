<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">Checkout - Address</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

         <div class = "all"> 
          <div id="content">
            <div class="container-fluid">



                <div class="col-md-9" id="checkout">

                    <div class="box">

                        <form method="post" action="{{url('/formvalidate')}}">
                            {!! csrf_field() !!}

                            <h1>Checkout</h1>
                            <ul class="nav nav-pills nav-justified">
                                <li class="active"><a href="#"><i class="fa fa-map-marker"></i><br>Address</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-truck"></i><br>Delivery Method</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Payment Method</a>
                                </li>
                                <li class="disabled"><a href="#"><i class="fa fa-eye"></i><br>Order Review</a>
                                </li>
                            </ul>

                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Firstname</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname">
                                            <span style ="color:red">{{$errors->first('firstname')}}</span>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Lastname</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname">
                                            <span style ="color:red">{{$errors->first('lastname')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Company</label>
                                            <input type="text" class="form-control" id="company" name="company">
                                            <span style ="color:red">{{$errors->first('company')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" id="street" name="street">
                                            <span style ="color:red">{{$errors->first('street')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">Company</label>
                                            <input type="text" class="form-control" id="city" name="city">
                                            <span style ="color:red">{{$errors->first('city')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="postcode">Postcode</label>
                                            <input type="text" class="form-control" id="postcode" name="postcode">
                                            <span style ="color:red">{{$errors->first('postcode')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state" name="state">
                                            <option value="">Choose State</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Kuala Lumpur/Putrajaya">Kuala Lumpur/Putrajaya</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Terengganu">Terengganu</option>
                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah/Labuan">Sabah/Labuan</option>
                                            <option value="Sarawak">Sarawak</option>

                                            </select>
                                            <span style ="color:red">{{$errors->first('state')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" id="country" name="country">
                                            <option value="Malaysia">Malaysia</option>
                                            </select>
                                            <span style ="color:red">{{$errors->first('country')}}</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Telephone</label>
                                            <input type="text" class="form-control" id="phone" name="phone">
                                            <span style ="color:red">{{$errors->first('phone')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email">
                                            <span style ="color:red">{{$errors->first('email')}}</span>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/cart')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Cart</a>
                                </div>
                                <div class="pull-right">
                                    <input type="submit" name="submit" value = "Proceed to Checkout"  class="btn btn-primary"><i class="fa fa-chevron-right"></i> 
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->



            </div>
            <!-- /.container -->
        </div>

            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
  </div>
      @include('layouts.scripts')

  </body>
</html>