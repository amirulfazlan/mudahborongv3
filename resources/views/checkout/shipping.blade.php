<!DOCTYPE html>
<html>


@include('layouts.mango.layout.head')

<link href="{{ asset('angularstep/style.css') }}" rel="stylesheet">

<link href="{{ asset('angularstep/selector.css') }}" rel="stylesheet">





<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            <!-- HEADER -->
            @include('layouts.mango.layout.headercheckout')
            <!-- HEADER -->

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="#">Checkout</a>
                </div>

                <div class="container">
    <div class="row">
        <section>
        <div class="wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-folder-open"></i>
                            </span>
                        </a>
                    </li>


                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-picture"></i>
                            </span>
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                            <span class="round-tab">
                                <i class="glyphicon glyphicon-ok"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

<form method="post" action="{{url('/formvalidate')}}">
                            {!! csrf_field() !!}
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <div class = "step1">

            <!--start if check--> 

            @if(is_null($address))

                            <h1>Shipping Details</h1>
                            
                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Firstname</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" value="">
                                            <span style ="color:red">{{$errors->first('firstname')}}</span>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Lastname</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" value="">
                                            <span style ="color:red">{{$errors->first('lastname')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Unit No/Block No/House Number</label>
                                            <input type="text" class="form-control" id="company" name="company" value="">
                                            <span style ="color:red">{{$errors->first('company')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" id="street" name="street" value="">
                                            <span style ="color:red">{{$errors->first('street')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">Company</label>
                                            <input type="text" class="form-control" id="city" name="city" value="">
                                            <span style ="color:red">{{$errors->first('city')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="postcode">Postcode</label>
                                            <input type="text" class="form-control" id="postcode" name="postcode" value="">
                                            <span style ="color:red">{{$errors->first('postcode')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state" name="state">
                                            <option value="">Choose State</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Kuala Lumpur/Putrajaya">Kuala Lumpur/Putrajaya</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Terengganu">Terengganu</option>
                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah/Labuan">Sabah/Labuan</option>
                                            <option value="Sarawak">Sarawak</option>

                                            </select>
                                            <span style ="color:red">{{$errors->first('state')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" id="country" name="country">
                                            <option value="Malaysia">Malaysia</option>
                                            </select>
                                            <span style ="color:red">{{$errors->first('country')}}</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Telephone</label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="">
                                            <span style ="color:red">{{$errors->first('phone')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" value="">
                                            <span style ="color:red">{{$errors->first('email')}}</span>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>

            @else
                
                <h1>Shipping Details</h1>
                            
                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="firstname">Firstname</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" value="{{$address->firstname}}">
                                            <span style ="color:red">{{$errors->first('firstname')}}</span>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Lastname</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" value="{{$address->lastname}}">
                                            <span style ="color:red">{{$errors->first('lastname')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company">Unit No/Block No/House Number</label>
                                            <input type="text" class="form-control" id="company" name="company" value="{{$address->company}}">
                                            <span style ="color:red">{{$errors->first('company')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="street">Street</label>
                                            <input type="text" class="form-control" id="street" name="street" value="{{$address->street}}">
                                            <span style ="color:red">{{$errors->first('street')}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="city">Company</label>
                                            <input type="text" class="form-control" id="city" name="city" value="{{$address->city}}">
                                            <span style ="color:red">{{$errors->first('city')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="postcode">Postcode</label>
                                            <input type="text" class="form-control" id="postcode" name="postcode" value="{{$address->postcode}}">
                                            <span style ="color:red">{{$errors->first('postcode')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state" name="state">
                                            <option value="">Choose State</option>
                                            <option value="Johor">Johor</option>
                                            <option value="Melaka">Melaka</option>
                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                            <option value="Selangor">Selangor</option>
                                            <option value="Kuala Lumpur/Putrajaya">Kuala Lumpur/Putrajaya</option>
                                            <option value="Perak">Perak</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Kelantan">Kelantan</option>
                                            <option value="Terengganu">Terengganu</option>
                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                            <option value="Kedah">Kedah</option>
                                            <option value="Perlis">Perlis</option>
                                            <option value="Sabah/Labuan">Sabah/Labuan</option>
                                            <option value="Sarawak">Sarawak</option>

                                            </select>
                                            <span style ="color:red">{{$errors->first('state')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" id="country" name="country">
                                            <option value="Malaysia">Malaysia</option>
                                            </select>
                                            <span style ="color:red">{{$errors->first('country')}}</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">Telephone</label>
                                            <input type="text" class="form-control" id="phone" name="phone" value="{{$address->phone}}">
                                            <span style ="color:red">{{$errors->first('phone')}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id="email" name="email" value="{{$address->email}}">
                                            <span style ="color:red">{{$errors->first('email')}}</span>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.row -->
                            </div>

                            @endif

  <!--habis if check-->                          
                       
                            
                        </div>

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    
                    <div class="tab-pane" role="tabpanel" id="step3">

                        <div class = "step33">


                            <h1>Delivery Option</h1>
                            
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-5">
                                        
                                            <div class="frb-group">
                                                <div class="frb frb-default">
                                                    <input type="radio" id="radio-button-0" name="delivery" value="poslaju">
                                                        <label for="radio-button-0">
                                                            <span class="frb-title">PosLaju</span>
                                                           
                                                        </label>
                                                </div>
                                                <div class="frb frb-primary">
                                                    <input type="radio" id="radio-button-1" name="delivery" value="gdex">
                                                        <label for="radio-button-1">
                                                            <span class="frb-title">GDex</span>
                                                            
                                                        </label>
                                                </div>
                                                <div class="frb frb-success">
                                                    <input type="radio" id="radio-button-2" name="delivery" value="skynet">
                                                        <label for="radio-button-2">
                                                            <span class="frb-title">Skynet</span>
                                                            
                                                        </label>
                                                </div>

                                            </div>
                                </div>
                            </div>
                                <!-- /.row -->

                            </div>

                            
                        
                        </div>

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            
                            <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <div class="step44">
                            <h5>Congratulations, your order has been completed. Click Complete for your invoice.</h5>

                            <div class="pull-right">
                                    <input type="submit" name="submit" value = "Complete"  class="btn btn-primary"><i class="fa fa-chevron-right"></i> 
                                </div>
                            
                          
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </section>
   </div>
</div>  






               <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->

            </div>


        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  @include('layouts.mango.layout.searchbox_popup')
  

   @include('layouts.mango.layout.cart_popup')

   @include('layouts.mango.layout.scripts')

</body>
</html>
