<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header ">
   

  @include('layouts.sidebar')

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Pages</p>
                  </li>
                  <li><a href="#" class="active">Dashboard</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <!-- START PANEL -->
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="invoice padding-50 sm-padding-10">
                  <div>
                    <div class="pull-left">
                      <img width="235" height="47" alt="" class="invoice-logo" data-src-retina="{{ asset ('bundle/demo/html/assets/img/invoice/squarespace2x.png')}}" data-src="{{ asset ('bundle/demo/html/assets/img/invoice/squarespace.png')}}" src="{{ asset ('bundle/demo/html/assets/img/invoice/squarespace2x.png')}}">
                      <address class="m-t-10">
                                      Apple Enterprise Sales
                                      <br>(877) 412-7753.
                                      <br>
                                  </address>
                    </div>
                    <div class="pull-right sm-m-t-20">
                      <h2 class="font-montserrat all-caps hint-text">Invoice</h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <br>
                  <br>
                  <div class="container-sm-height">
                    <div class="row-sm-height">
                      <div class="col-md-9 col-sm-height sm-no-padding">
                        <p class="small no-margin">Invoice to</p>

                     

                        <h5 class="semi-bold m-t-0">{{$address['firstname']}} {{$address['lastname']}}</h5>
                        <address>
                                              <strong>Pages Incoperated.</strong>
                                              <br>page.inc
                                              <br>
                                              1600 Amphitheatre Pkwy, Mountain View,<br>
                                              CA 94043, United States
                                          </address>

                    


                      </div>
                      <div class="col-md-3 col-sm-height col-bottom sm-no-padding sm-p-b-20">
                        <br>
                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Invoice No :</div>
                          <div class="pull-right">0047</div>
                          <div class="clearfix"></div>
                        </div>
                        <div>
                          <div class="pull-left font-montserrat bold all-caps">Invoice date :</div>
                          <div class="pull-right">{{$date}}</div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <table class="table m-t-50">
                      <thead>
                        <tr>
                          <th class="text-center">Bil</th>
                          <th class="">Product</th>
                          <th class="text-center">Size</th>
                          <th class="text-center">Color</th>
                          <th class="text-center">Quantity</th>
                          <th class="text-center">Prime</th>

                          <th class="text-right">Unit Price</th>
                          <th class="text-right">Sub Price</th>
                        
                        </tr>

                      </thead>
                      <tbody>

                        <?php $total = 0; 

                         $i=0;

                          ?>
                       @foreach($cart as $cartItems) 

                        <?php $total += $cartItems->subtotal ; 
                              $sub = $cartItems->price * $cartItems->qty ;
                              $i++;

                        ?>

                        <tr>
                          <td class="text-center">{{ $i}}</td>
                          <td class="">
                            <p class="text-black">{{$cartItems->name}}</p>
                            <p class="small hint-text">
                              Sku / apa2
                            </p>
                          </td>
                          <td class="text-center">{{$cartItems->options->size}}</td>
                          <td class="text-center">{{$cartItems->options->color}}</td>
                          <td class="text-center">{{$cartItems->qty}}</td>
                          <td class="text-center">RM 0.00</td>
                          <td class="text-right">RM {{$cartItems->price}}</td>
                           <td class="text-right">RM <?php echo $sub; ?> </td>
                        </tr>

                        @endforeach


                      </tbody>
                    </table>
                  </div>
                  <br>
                  <br>
                  <br>
                  <br>
                  <br>
                  <div>
                    <img width="150" height="58" alt="" class="invoice-signature" data-src-retina="{{ asset ('bundle/demo/html/assets/img/invoice/signature2x.png')}}" data-src="{{ asset ('bundle/demo/html/assets/img/invoice/signature.png')}}" src="{{ asset ('bundle/demo/html/assets/img/invoice/signature2x.png')}}">
                    <p>Designer’s Identity</p>
                  </div>
                  <br>
                  <br>
                  <div class="container-sm-height">
                    <div class="row row-sm-height b-a b-grey">
                      <div class="col-sm-2 col-sm-height col-middle p-l-25 sm-p-t-15 sm-p-l-15 clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold">Advance</h5>
                        <h3 class="no-margin">$21,000.00</h3>
                      </div>
                      <div class="col-sm-5 col-sm-height col-middle clearfix sm-p-b-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text bold">Discount (10%)</h5>
                        <h3 class="no-margin">$645.00</h3>
                      </div>
                      <div class="col-sm-5 text-right bg-menu col-sm-height padding-15">
                        <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
                        <h1 class="no-margin text-white">RM <?php echo $total; ?></h1>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <p class="small hint-text">Services will be invoiced in accordance with the Service Description. You must pay all undisputed invoices in full within 30 days of the invoice date, unless otherwise specified under the Special Terms and Conditions. All payments must reference the invoice number. Unless otherwise specified, all invoices shall be paid in the currency of the invoice</p>
                  <p class="small hint-text">Insight retains the right to decline to extend credit and to require that the applicable purchase price be paid prior to performance of Services based on changes in insight's credit policies or your financial condition and/or payment record. Insight reserves the right to charge interest of 1.5% per month or the maximum allowable by applicable law, whichever is less, for any undisputed past due invoices. You are responsible for all costs of collection, including reasonable attorneys' fees, for any payment default on undisputed invoices. In addition, Insight may terminate all further work if payment is not received in a timely manner.</p>
                  <br>
                  <hr>
                  <div>
                    <img src="{{ asset ('bundle/demo/html/assets/img/logo.png') }}" alt="logo" data-src="{{ asset ('bundle/demo/html/assets/img/logo.png')}}" data-src-retina="{{ asset ('bundle/demo/html/assets/img/logo_2x.png')}}" width="78" height="22">
                    <span class="m-l-70 text-black sm-pull-right">+34 346 4546 445</span>
                    <span class="m-l-40 text-black sm-pull-right">support@revox.io</span>
                  </div>
                </div>
              </div>
            </div>
            <!-- END PANEL -->





            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
      @include('layouts.scripts')

  </body>
</html>