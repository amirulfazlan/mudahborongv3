
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Service : Currency</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Currency Value</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

               @foreach($curr as $value)

                <form method="post" action="{{action('CurrenciesController@update', $value['id'])}}">

                {{ method_field('PUT') }}
                {!! csrf_field() !!}

              <div class="form-group row">
              <div class="form-group {{ $errors->has('cur_value') ? ' has-error' : '' }}">
              <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Yuan Value</label>
              <div class="col-sm-5">
              <div class="input-group">
               
               <span class="input-group-addon" style="width:40px">¥</span>
               <input name="cur_value" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="Yuan Value" value="{{ number_format($value['cur_value'], 2) }}" style="width:80px">
              
              </div>

              @if ($errors->has('cur_value'))
                  <span class="help-block">
                      <strong>{{ $errors->first('cur_value') }}</strong>
                  </span>
              @endif
              </div>
              </div>
              </div>
              <div class="form-group row">
              <div class="col-md-2"></div>

              <button type="submit" class="btn btn-primary" >Update</button>

              </div>

               </form>
             

                @endforeach

           
      
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>