
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  <div class="page-container ">

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Update Back Image</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Update Back Image</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

              <form method="post" action="{{action('UpdateBackImageController@update', $id)}}" enctype="multipart/form-data">

              {{ method_field('PUT') }}
              {!! csrf_field() !!}

              <div class="form-group row">
              <div class="form-group {{ $errors->has('back_image') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Back Image</label>
              <div class="col-sm-5">
              <input type="file" class="form-control" name="back_image"  value="{{ asset('images/main_product/'.$product->back_image) }}">
              @if ($errors->has('back_image'))
              <span class="help-block">
              <strong>{{ $errors->first('back_image') }}</strong>
              </span>
              @endif
              </div>
              </div>
              </div>


      <center><input type="submit" name="submit" value = "Update Back Image"  class="btn btn-primary"></center>
      </form>

    <div class="panel-heading"> </div>

  </div>
  </div>
  </div>
  </div>
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       
    </div>
      @include('layouts.scripts')

  </body>
</html>