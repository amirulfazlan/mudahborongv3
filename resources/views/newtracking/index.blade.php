
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Tracking</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
           

           <!-- Modal -->

         
               <!-- /.modal-content -->
      

          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

              <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">List Of Order
                </div>
               
                <div class="clearfix"></div>
              </div>

                <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Date</th>
                        <th style="width:20%">Order ID</th>

                        <th style="width:20%">User ID</th>
                        <th style="width:20%">Status</th>
                        <th style="width:20%">Grand Total</th>
                       
                        <th style="width:20%">Tracking Details</th>
                        

                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                        @foreach($pesanan as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{$i}}</p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{  $post->created_at->todatestring() }}  </p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{$post->id}} </p>
                        </td>

                        <td class="v-align-middle">
                          <p>{{ $post->wh_name['name'] }} </p>
                        </td>

                        <td class="v-align-middle">
                           <p>{{$post->status}} </p>
                        </td>

                        <td class="v-align-middle">
                         <p>{{$post->grand_total }} </p>
                        </td>

                        <td class="v-align-middle">

                          <a href="{{action('TrackingNewController@show', $post['id'])}}" class="btn btn-info"> Show <span class="glyphicon glyphicon-eye-open"></span></a>

                        </td>

                      </tr>
                     @endforeach
                        
                        
                    </tbody>
                  </table>

                 
                  
                </div>
              </div>
                      

            
            </div>

          
                  
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

     
    </div>
      @include('layouts.scripts')

  </body>
</html>