
<!DOCTYPE html> 
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="/admin-orderhistory" class="active">Edit Tracking Details</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Tracking Details</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

      <form method="post" action="{{action('TrackingNewController@update', $tracking['id'])}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}

      <div class="form-group row">
              <div class="form-group {{ $errors->has('order_id') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Order Id</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="order_id" name="order_id" value="{{$pesanan['id']}}"> 
              @if ($errors->has('order_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('order_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

             <div class="form-group row">
              <div class="form-group {{ $errors->has('order_id') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Tracking Id</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="order_id" name="order_id" value="{{$tracking_id}}"> 
              @if ($errors->has('order_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('order_id') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

               <div class="form-group row">
               <div class="form-group {{ $errors->has('item_id') ? ' has-error' : '' }}">
               <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product</label>
               <div class="col-sm-5">
               <div class="dropdown">
               <select id="item_id" name="item_id" class="form-control" required>
               <span class="caret"></span>
               <ul class="dropdown-menu">
                @foreach($json_decode as $name)
                @if($name->rowId == $tracking->item_id )
                <option value="{{$tracking['item_id']}}">{{ $name->name}}</option>
                @endif
                @endforeach
                <div class="dropdown-divider"></div>
                @foreach($json_decode as $content)
                <option value="{{  $content->rowId }}">{{  $content->name }} Size:{{  $content->options->size }} Color:{{  $content->options->color}}</option>
                @endforeach
              </ul>
              </select>
              @if ($errors->has('item_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('item_id') }}</strong>
                    </span>
                @endif
              </div>
              </div>
              </div>
              </div>


            <div class="form-group row">
              <div class="form-group {{ $errors->has('tracking_no') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Tracking Number</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="tracking_num" name="tracking_no" value="{{$tracking['tracking_no'] }}"> 
              @if ($errors->has('tracking_num'))
                  <span class="help-block">
                      <strong>{{ $errors->first('tracking_num') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('weight') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Weight Parsel</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="weight" name="weight" value="{{  number_format($tracking['weight'], 2) }}"> 
              @if ($errors->has('weight'))
                  <span class="help-block">
                      <strong>{{ $errors->first('weight') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

            <div class="form-group row">
              <div class="form-group {{ $errors->has('ship_price') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Price Shipping</label>
              <div class="col-sm-5">
              
               <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="ship_price" name="ship_price" value="{{  number_format($tracking['ship_price'], 2) }}"> 
              @if ($errors->has('ship_price'))
                  <span class="help-block">
                      <strong>{{ $errors->first('ship_price') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div> 

           

      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
    
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>