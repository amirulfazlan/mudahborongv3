
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Product Tracking Details</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->




          <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Product Color <span class="semi-bold"></span></h5>

                    <div class="panel-heading">     

                        </div>
                    
                  </div>
                  <div class="modal-body">

               <form action="/newtracking" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}

               <input type="hidden" name="order_id"  value="<?php echo  $pesanan['id']; ?>">


               <div class="form-group row">
               <div class="form-group {{ $errors->has('item_id') ? ' has-error' : '' }}">
               <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product</label>
               <div class="col-sm-5">
               <div class="dropdown">
               <select id="item_id" name="item_id" class="form-control" required>
               <span class="caret"></span>
               <ul class="dropdown-menu">
                <option value="">Choose Product</option>
                <div class="dropdown-divider"></div>
                @foreach($json_decode as $content)
                <option value="{{  $content->rowId }}">{{  $content->name }} Size:{{  $content->options->size }} Color:{{  $content->options->color}}</option>
                @endforeach
              </ul>
              </select>
              @if ($errors->has('item_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('item_id') }}</strong>
                    </span>
                @endif
              </div>
              </div>
              </div>
              </div>

            


              <div class="form-group row">
              <div class="form-group {{ $errors->has('tracking_no') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Tracking No</label>
              <div class="col-sm-5">
              <input type="text" name="tracking_no" class="form-control" required>
              @if ($errors->has('tracking_no'))
                  <span class="help-block">
                      <strong>{{ $errors->first('tracking_no') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

             <div class="form-group row">
              <div class="form-group {{ $errors->has('weight') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Weight Parsel</label>
              <div class="col-sm-5">
              <input type="text" name="weight" class="form-control" required>
              @if ($errors->has('weight'))
                  <span class="help-block">
                      <strong>{{ $errors->first('weight') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

             <div class="form-group row">
              <div class="form-group {{ $errors->has('ship_price') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Shipping Price</label>
              <div class="col-sm-5">
              <input type="text" name="ship_price" class="form-control" required>
              @if ($errors->has('ship_price'))
                  <span class="help-block">
                      <strong>{{ $errors->first('ship_price') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>

              



       
      
            <div class="form-group row">
            <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right"> 
            <input type="submit" name="submit" value = "Create Product Tracking"  class="btn btn-primary btn-block m-t-5 ">
            </div>
            </div>      

             </form>
    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

             <div class="panel panel-transparent">
              <div class="panel-heading">

                <h4><b>Tracking detail for order number : {{$pesanan['id']}} </b></h4></br>
                <h6><b>Number of product : {{$count_item}} </b></h6>
                <h6><b>Tracking detail insert : {{$data_tracking}} </b></h6>



            

            </br>

                <div class="panel-title">List Of Product Tracking
                </div>
                <div class="btn-group pull-right m-b-10">
                 <button class="btn btn-primary" id="btnToggleSlideUpSize" >Add New Product Tracking</button>
                 
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">
                    
                    <h6><b style="color: red">Tracking detail insert remaining : {{$remaining}}</b></h6>

                 </br>
                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Image</th>
                        <th style="width:20%">Product</th>
                        <th style="width:10%">Size</th>
                        <th style="width:10%">Color</th>
                        <th style="width:20%">Tracking No</th>
                        <th style="width:10%">Weight</th>
                        <th style="width:10%">Shiping Price</th>
                
                        <th style="width:15%">Edit</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 0 ?>
                        @foreach($tracking as $post)
                       <?php $i++ ?>

                      <tr>   
                        
                        <td class="v-align-middle ">
                          <p>{{$i}}</p>
                        </td>

                        <td class="v-align-middle ">
                        @foreach($json_decode as $image) 
                        @if($image->rowId == $post->item_id )
                        @foreach ($pro_var as $var)
                        @if ( $var['p_id'] == $image->id)
                        @if ( $var['p_color'] == $image->options->color)
                       

                           <div class="row" id="thumbs">

                              <img src="{{ asset ('images/product/'.$var['main_image']) }}" alt="" height="100" width="100">

                          </div> 
                            
                       
                        @endif 
                        @endif
                        @endforeach 
                        @endif
                        @endforeach
                        </td>

                       
                          @foreach($json_decode as $product) 
                          @if($product->rowId == $post->item_id )
                          <td class="v-align-middle ">
                          <p>{{$product->name }}</p>
                          </td>

                          <td class="v-align-middle ">
                          <p>{{$product->options->size }}</p>
                          </td>

                          <td class="v-align-middle ">
                          <p>{{$product->options->color }}</p>
                          </td>

                           @endif
                         @endforeach
                       

                        <td class="v-align-middle ">
                          <p>{{$post->tracking_no }}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{$post->weight }}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>{{$post->ship_price }}</p>
                        </td>

                        <td class="v-align-middle ">
                          <a href="{{action('TrackingNewController@edit', [$pesanan['id'], $t_id=$post['id'] ] )}}" class="btn btn-warning">Edit Tracking <span class="glyphicon glyphicon-pencil"></span></a> 
                        </td>
                       
                       <td class="v-align-middle ">
                           <form action="{{action('TrackingNewController@destroy', $post['id'])}}" method="post">
                          {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                          <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                          </form>
                        </td>
                       
                      </tr>
                       @endforeach
                        

                        
                    </tbody>
                  </table>

                  
                  
                </div>
              </div>
            </div>


            
         
             
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>