
<!DOCTYPE html>
<html>

  @include('layouts.head')


  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')
  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Product Photo Manager</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->


          <!-- Modal -->

          <div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Product Photo <span class="semi-bold"></span></h5>

                    <div class="panel-heading">     

                        </div>
                    
                  </div>
                  <div class="modal-body">

               <form action="/product_photo" method="POST"  enctype="multipart/form-data">
              
              {!! csrf_field() !!}


               <div class="form-group row">
               <div class="form-group {{ $errors->has('p_id') ? ' has-error' : '' }}">
               <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Product</label>
               <div class="col-sm-5">
               <div class="dropdown">
               <select id="p_id" name="p_id" class="form-control" required>
               <span class="caret"></span>
               <ul class="dropdown-menu">
                <option value="">Choose Product</option>
                <div class="dropdown-divider"></div>
                @foreach($product as $post2)
                <option value="{{$post2['id']}}">{{$post2['p_name']}}</option>
                @endforeach
              </ul>
              </select>
              @if ($errors->has('p_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('p_id') }}</strong>
                    </span>
                @endif
              </div>
              </div>
              </div>
              </div>

 
                    <div class="form-group row">
                      <div class="form-group {{$errors->has('filename[]') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Image</label>
                          <div class="col-sm-5">
                            <input type="file" name="filename[]" class="form-control" multiple>
                            @if ($errors->has('filename[]'))
                            <span class="help-block">
                            <strong>{{ $errors->first('filename[]') }}</strong>
                            </span>
                            @endif

                            
                          </div>
                        
                      </div>
                      
                    </div>



            <div class="form-group row">
            <div class="col-sm-4 m-t-10 sm-m-t-10 pull-right"> 
            <input type="submit" name="submit" value = "Add Product Photo"  class="btn btn-primary btn-block m-t-5 ">
            </div>
            </div>      

             </form>
    
                  </div>
                </div>
              </div>
              </div>
              </div>
               <!-- /.modal-content -->



          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

             <div class="panel panel-transparent">
              <div class="panel-heading">
                <form method="get" action="/product_photo">

              <div class="form-group row">
              <div class="form-group {{ $errors->has('search_name') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Product Name</label>
              <div class="col-sm-5">
              <input type="text" name="search_name" class="form-control">
              @if ($errors->has('search_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('search_name') }}</strong>
                  </span>
              @endif


            </div>

             <button type="submit" class="btn btn-primary">Search</button>
            
            </div>

           

            </div>

          </form>

            </br>

                <div class="panel-title">List Of Product Photo ( Catalog Use )
                </div>
                <div class="btn-group pull-right m-b-10">
                 <button class="btn btn-primary" id="btnToggleSlideUpSize" >Add New Product Photo</button>
                 
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">

                <form method="get" action="/product_photo">

                               
                                   <div class="form-group row">
                                        <label class="col-sm-2 control-label">Filter</label>

                                        <script type="text/javascript">
                                        $(document).ready(function() {
                                        $('select[name="gender_id"]').on('change', function() {
                                        var typeID = $(this).val();
                                        if( typeID) {
                                        $.ajax({
                                        url: '/myform/ajax/'+ typeID,
                                        type: "GET",
                                        dataType: "json",
                                        success:function(data) {

                        
                                       $('select[name="category_id"]').empty();
                                       $.each(data, function(key, value) {
                                       $('select[name="category_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                                       });

                                       }
                                       });
                                       }else{
                                       $('select[name="category_id"]').empty();
                                       }
                                       });
                                       });
                                       </script>

                                        <div class="col-sm-2">    
                                        <select id="gender_id" name="gender_id" class="form-control" >
                                        <option value="0">All</option>  
                                        @foreach ($gen as $key => $value)
                                        <option value="{{ $key }}" >{{ $value }}</option>
                                        @endforeach     
                                        </select>

                                        </div>


                                         <div class="col-sm-2">
                                         <select id="category_id" name="category_id" class="form-control" >
                                         <option value="0">All</option>      
                                         </select>
                                         </div>
                                        

                                         <div class="col-sm-2">  
                                         <select name="brand_id" class="form-control" >
                                         <option value="0">All</option>    
                                         @foreach ($brand as $label)
                                         <option value="{{$label['id']}}">{{$label['brand_name']}}</option>
                                          @endforeach
                                         </select>
                                         </div>

                                        
                                        <button type="submit" class="btn btn-primary">Filter</button>
                                    </div>

                                    </form>



                 </br>



                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:2%">No</th>
                        <th style="width:20%">Product Name</th>
                        <th style="width:30%">Image</th>
                       
                        <th style="width:15%">View</th>                       
                        
                      </tr>
                    </thead>
                    <tbody>
                       <?php $i = 0 ?>
                        @foreach($product as $post)
                       <?php $i++ ?>
                      <tr>

                        
                        <td class="v-align-middle ">
                          <p>{{ $i}}</p>
                        </td>
                        
                        <td class="v-align-middle ">
                          <p>{{$post['p_name']}}</p>
                        </td>
                        
                        <td> 
         
                          <div class="row" id="thumbs">

                              <img src="{{ asset ('images/main_product/'.$post['main_image']) }}" alt="" height="100" width="100">

                          </div>
                          
                        
                        </td>
                        

                        <td class="v-align-middle">

                         <a href="{{action('Product_PhotosController@show', $post['id'])}}" class="btn btn-info">View <span class="glyphicon glyphicon-eye-open"></span></a>

                        </td>
                        
                        
                        
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>

                  {{ $product->links() }}

                </div>
              </div>
            </div>


          
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>