
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">View Product Price</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">View Product Price</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >


     <div class="form-group row">
                    
                      <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/product/'.$price->main_image) }}" alt="{{$price->brand_image}}" class="img-responsive"/> 
                        </div>

                        
                    </div>
     

      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_id') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product ID</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_id" name="p_id" value="{{$price->p_id}}">
          @if ($errors->has('p_id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_id') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" name="p_name" value="{{$price->product_name->p_name}}">
          @if ($errors->has('p_name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_name') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      <div class="form-group row">
       <div class="form-group {{ $errors->has('sku') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">SKU</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="sku" name="sku" value="{{$price->sku}}">
           @if ($errors->has('sku'))
                  <span class="help-block">
                      <strong>{{ $errors->first('sku') }}</strong>
                  </span>
              @endif
        </div>
      </div>
      </div>

      
       <div class="form-group row">
             <div class="form-group {{ $errors->has('p_color') ? ' has-error' : '' }}">
             <label class="col-sm-2 control-label">Color</label>
             <div class="col-sm-5">
              <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_color" name="p_color" value="{{$price->p_color}}">
             @if ($errors->has('p_color'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_color') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>


     
      

      <div class="form-group row">
        <div class="col-md-2"></div>
        <a href="{{action('Product_PricesController@edit', $price['id'])}}" class="btn btn-primary">Edit <span class="glyphicon glyphicon-pencil"></span></a>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>