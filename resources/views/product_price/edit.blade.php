
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Edit Product Price</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON --> 


          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Edit Product Price</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

    <form method="post" action="{{action('Product_PricesController@update', $id)}}">

     {{ method_field('PUT') }}
     {!! csrf_field() !!}

     <div class="form-group row">
                    
                      <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-5">
                         <img src="{{ asset('images/product/'.$price->main_image) }}" alt="{{$price->brand_image}}" class="img-responsive"/> 
                        </div>

                        
                    </div>

    

      <div class="form-group row">

       <div class="form-group {{ $errors->has('p_name') ? ' has-error' : '' }}">

        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Product Name</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="p_name" value="{{$price->product_name->p_name}}">
        </div>
      </div>
      </div>

      <div class="form-group row">
           <div class="form-group {{ $errors->has('main_image') ? ' has-error' : '' }}">
           <label class="col-sm-2 control-label">Main Image</label>
           <div class="col-sm-6">
           <a class="btn btn-primary" onclick='javascript:window.open(" {{action('UpdatePColorImageController@edit', $price['id'])}} ", "_$product->id", "scrollbars=1,resizable=1,height=400,width=450");' title="Update Main Image">Update</a>
           </div> 
           </div>
           </div>
      
      


      <div class="form-group row">
       <div class="form-group {{ $errors->has('sku') ? ' has-error' : '' }}">
        <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">SKU</label>
        <div class="col-sm-5">
          <input type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="sku" name="sku" value="{{$price->sku}}">
        
        </div>
      </div>
      </div>


            <div class="form-group row">
              <div class="form-group {{ $errors->has('p_color') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Product Color</label>
              <div class="col-sm-5">
              <input type="text" name="p_color" class="form-control" value="{{$price->p_color}}">
              @if ($errors->has('p_color'))
                  <span class="help-block">
                      <strong>{{ $errors->first('p_color') }}</strong>
                  </span>
              @endif
            </div>
            </div>
            </div>



      <div class="form-group row">
        <div class="col-md-2"></div>
        <button type="submit" class="btn btn-primary" >Update</button>
        <a href="javascript:history.back()" class="btn btn-primary">Back</a>
      </div>
    </form>
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>