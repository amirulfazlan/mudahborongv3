
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  @include('layouts.sidebar')

  <div class="page-container ">

     @include('layouts.topbar')
      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Service : Prime</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


           <div class="col-lg-12">
                    <h1 class="page-header">Prime Price</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

               @foreach($prines as $price)

                <form method="post" action="{{action('PrineController@update', $price['id'])}}">

                {{ method_field('PUT') }}
                {!! csrf_field() !!}

              <div class="form-group row">
              <div class="form-group {{ $errors->has('prine_price') ? ' has-error' : '' }}">
              <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Prine Price</label>
              <div class="col-sm-5">
              <div class="input-group">
             
               <input name="prine_price" type="text" class="form-control form-control-lg" id="lgFormGroupInput" placeholder="prine_price" value="{{ number_format($price['prine_price'], 2) }}" style="width:80px">
              <span class="input-group-addon" style="width:40px">%</span>
              </div>

              @if ($errors->has('prine_price'))
                  <span class="help-block">
                      <strong>{{ $errors->first('prine_price') }}</strong>
                  </span>
              @endif
              </div>
              </div>
              </div>
              <div class="form-group row">
              <div class="col-md-2"></div>

              <button type="submit" class="btn btn-primary" >Update</button>

              </div>

               </form>
             

                @endforeach

           
      
  </div>
  </div>
  </div>
  </div>


         <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>