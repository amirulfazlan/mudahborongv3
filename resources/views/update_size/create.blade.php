
<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin">
   

  <div class="page-container ">

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <a href="{{url('/')}}">MudahBorong</a>
                  </li>
                  <li><a href="#" class="active">Update Size Chart</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->


                 <!--  page header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Update Size Chart</h1>
                </div>
                 <!-- end  page header -->
           

             <div class="row">
                <div class="col-lg-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">     

                        </div>

             <div class="container" >

              <form method="post" action="{{action('UpdateSizeChartController@update', $id)}}" enctype="multipart/form-data">

              {{ method_field('PUT') }}
              {!! csrf_field() !!}

              <div class="form-group row">
              <div class="form-group {{ $errors->has('size_chart') ? ' has-error' : '' }}">
              <label class="col-sm-2 control-label">Size Chart</label>
              <div class="col-sm-5">
              <input type="file" class="form-control" name="size_chart" >
              @if ($errors->has('size_chart'))
              <span class="help-block">
              <strong>{{ $errors->first('size_chart') }}</strong>
              </span>
               @endif
              </div>
              </div>
              </div>


      <center><input type="submit" name="submit" value = "Update Size Chart"  class="btn btn-primary"></center>
      </form>

    <div class="panel-heading"> </div>

  </div>
  </div>
  </div>
  </div>
            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       
    </div>
      @include('layouts.scripts')

  </body>
</html>