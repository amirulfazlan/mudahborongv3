<!DOCTYPE html>
<html>

  @include('layouts.head')

  <body class="fixed-header menu-pin ">
   

  @include('layouts.sidebar')

     <div class="page-container ">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>MudahBorong</p>
                  </li>
                  <li><a href="#" class="active">Users</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg bg-white">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">List Of Customers
                </div> 
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">


                @if(Auth::check())
                @if(Auth::user()->admin==1)

                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:20%">User ID</th>
                        <th style="width:20%">Name</th>
                        <th style="width:20%">E-Mail</th>
                        <th style="width:20%">User Type</th>
                        <th style="width:15%">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                       
                         @foreach($users as $user)
                       
                      <tr>

                        
                        
                        <td class="v-align-middle">
                          <p>{{ $user->id }}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $user->name }}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $user->email }}</p>
                        </td>

                        @if($user->admin == 1)

                        <td class="v-align-middle ">
                          <p>Administrator</p>
                        </td>
                        @else
                        <td class="v-align-middle ">
                          <p>Wholesaler</p>
                        </td>
                        @endif
                        
                        <td class="v-align-middle">
                          <form action="{{action('CustomerController@destroy', $user['id'])}}" method="post">
                          {{csrf_field()}}
                          <input name="_method" type="hidden" value="DELETE">
                           <button class="btn btn-danger" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                           </form>
                        </td>
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>
                </div>


                @else

                <div class="table-responsive">
                  <table class="table table-hover" id="basicTable">
                    <thead>
                      <tr>
                        <!-- NOTE * : Inline Style Width For Table Cell is Required as it may differ from user to user
                      Comman Practice Followed
                      -->
                        
                        <th style="width:20%">User ID</th>
                        <th style="width:20%">Name</th>
                        <th style="width:20%">E-Mail</th>
                        <th style="width:20%">User Type</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      
                         @foreach($users as $user)
                      
                      <tr>

                        
                        
                        <td class="v-align-middle">
                          <p>{{ $user->id }}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $user->name }}</p>
                        </td>
                        <td class="v-align-middle ">
                          <p>{{ $user->email }}</p>
                        </td>

                        <td class="v-align-middle ">
                          <p>Jap eh</p>
                        </td>
                        
                        
                      </tr>
                      
                         @endforeach

                        
                    </tbody>
                  </table>

                   {{ $users->links() }}
                   
                </div>

                @endif
                @endif

              </div>
            </div>


            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      @include('layouts.leftsidebar')
    </div>
      @include('layouts.scripts')

  </body>
</html>