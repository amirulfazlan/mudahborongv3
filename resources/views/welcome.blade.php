<!DOCTYPE html>
<html>

  @include('layouts.head')


  


  <body class="fixed-header menu-pin">

  @if(Auth::guest())
  @include('layouts.sidebarWholesaler')
  @else
  @if ( Auth::user()->is_Admin() ) 
   


  @include('layouts.sidebar')
  @else
  @include('layouts.sidebarWholesaler')
  @endif
  @endif

    <div class="page-container">

     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                  <li>
                    <p>Mudahborong</p>
                  </li>
                  <li><a href="#" class="active">Dashboard</a>
                  </li>
                </ul>
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

               <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="panel-title">Dashboard
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">

                 

                


              </div>
            </div>



            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

       @include('layouts.footer')

      {{--@include('layouts.leftsidebar')--}}

      </div>
      @include('layouts.scripts')

  </body>
</html>