<!DOCTYPE html>
<html>


  @include('layouts.head')

  <!-- Product Catalog -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset ('p_catalog/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset ('p_catalog/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset ('p_catalog/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset ('p_catalog/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">

  <body class="fixed-header menu-pin">
   


    @if (Auth::check()) 
    @if ( Auth::user()->is_Admin() )
        @include('layouts.sidebar')
        @else
        @include('layouts.sidebarWholesaler')
    @endif
  @else
    @include('layouts.sidebarWholesaler')
  @endif

   <!-- START PAGE-CONTAINER -->
    <div class="page-container ">


     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel-heading">     

                        </div>

              <div id="all">
    
        <div id="content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}">MudahBorong</a>
                        </li>
                        <li>Cart Review</li>
                    </ul>
                </div>

                <div class="col-md-9" id="basket">

                    <div class="box">

                        <form method="post" action="{{url('/basket')}}">
                            {!! csrf_field() !!}

                            <h1>Basket Product</h1>


                           <script type="text/javascript">

                           $(document).ready(function(e) {   
                           $('#quantity').on('keyup',function(){
                           var tot = $('#price').val() * this.value;
                           $('#total').val(tot);
                           });
                           });


                

 
                            </script>

                            @if($cart->isEmpty())

                            <p class="text-muted">Your basket is EMPTY! Please add the item that you want to purchase in cart first. </p>

                            @else

                            
                            <p class="text-muted">Here is the list of item currently on your basket. Please confirm all the item before proceeding to get your final price. Price shown is not final.</p>

                            @endif
                            
                            <div class="table-responsive">
                                
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Product</th>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th>Quantity</th>         
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                        <?php 
                                        $total = 0; 
                                        $i = 0;
                                        ?>
                                        @foreach($cart as $cartItems)

                                         <?php $total += $cartItems->subtotal ; 
                                        $sub = $cartItems->price * $cartItems->qty ;
                                        $i++;

                        ?>


                                             
                                       
                                          <input type="hidden" name="product_name[]"  value="{{$cartItems->name}}">
                                          <input type="hidden" name="quantity[]"  value="{{$cartItems->qty}}">
                                          <input type="hidden" name="color[]"  value="{{$cartItems->options->color}}">
                                          <input type="hidden" name="size[]"  value="{{$cartItems->options->size}}">
                                          <input type="hidden" name="pid[]" value="{{$cartItems->id}}">
                                         
                                          <input type="hidden" name="total[]"  value="<?php echo $total; ?>">
                                        
                                    <tbody>
                                        <tr>

                                            <td>{{$i}}</td>       
                                            <td>{{$cartItems->name}}</td>
                                            <td>{{$cartItems->options->color}}</td>
                                            <td>{{$cartItems->options->size}}</td>


                                            <td>{{$cartItems->qty}}</td>


                                            

                                          

                                            <td><a href="{{ url('/removeItem', $cartItems->rowId) }}"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                            <td><a href="{{ url('/updateItem', $cartItems->rowId) }}"><i class="fa fa-refresh"></i></a>
                                            </td>
                                        </tr>
                                        
                                      </tbody>
                                    



                                    @endforeach

                            
                                    
                                    
                                </table>

                            </div>
                            <!-- /.table-responsive -->
                            

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/shop_men')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                                </div>

                            <p class="text-center buttons">

                            `` <button type="submit" class="btn btn-primary ><i class="fa fa-shopping-cart"></i> Add to cart</button>
                                   
                                   
                                    
                                </p>

                            


                            
                            </div>

                            

                        </form>

                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


             
          <!-- END CONTAINER FLUID -->
            </div>
        <!-- END PAGE CONTENT -->
        <div class="panel-heading">     

                        </div>

        </div>
        </div>

        

       @include('layouts.footer')

      @include('layouts.leftsidebar')
       </div>
      @include('layouts.scripts')

    <script src="{{ asset ('p_catalog/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/waypoints.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/modernizr.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/front.js') }}"></script>


  </body>
</html>
