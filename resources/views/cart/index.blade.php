<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

   window.onload=function(){
                  //Get the innerHTML of the div tag
     var a=document.getElementById('qty').innerHTML;
                  //Place the inner HTML into the text box 'Key'
      document.getElementById('new_qty').value=a;
                  };

function myFunction() {
    var x = document.getElementById("qty").innerHTML ;
     document.getElementById('new_qty').value=x;
}



 
</script>




<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->
         

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="/shopping_catalog">Shop</a>
                    <a href="/cart">Basket Review</a>
                </div>

                <form method="post" action="{{url('/basket')}}">
                  {{ csrf_field() }}
                    
                   

                <div class="information-blocks">
                    <div class="row">
                        <div class="col-sm-9 information-entry">
                            <h3 class="cart-column-title size-1">Products</h3>
                      
                             @if($cart->isEmpty())

                            <div class="product-description detail-info-entry">Your basket is EMPTY! Please add the item that you want to purchase in cart first.</div>

                            @else

                             <div class="product-description detail-info-entry">Here is the list of item currently on your basket. Please confirm all the item before proceeding to get your final price.</div>

                            @endif

                            <?php 
                             $total = 0; 
                             $i = 0;
                            ?>
                             
                             @foreach($cart as $cartItems)

                            <?php $total += $cartItems->subtotal ; 
                             $sub = $cartItems->price * $cartItems->qty ;
                             $i++;
                             ?>

                                
                                          <input type="hidden" name="product_name[]"  value="{{$cartItems->name}}">
                                          <input type="hidden" name="quantity[]"  value="{{$cartItems->qty}}">
                                          <input type="hidden" name="color[]"  value="{{$cartItems->options->color}}">
                                          <input type="hidden" name="size[]"  value="{{$cartItems->options->size}}">
                                          <input type="hidden" name="pid[]" value="{{$cartItems->id}}">
                                         
                                          <input type="hidden" name="total[]"  value="<?php echo $total; ?>">

                            
                            <div class="traditional-cart-entry style-1"> 

                                @foreach ($pro_var as $var)
                                 @if ( $var['p_id'] == $cartItems->id)
                                  @if ( $var['p_color'] == $cartItems->options->color)
                                    <a class="image" href="/shopping_catalog/{{$cartItems->id}}"><img alt="" src="{{ 'images/product/'.$var['main_image'] }}"></a>

                                    <input type="hidden" name="color_id[]"  value="<?php echo  $var['id']; ?>">

                                  @endif
                                 @endif
                                @endforeach 

                                 <div class="content">
                                    
                                    <div class="cell-view">
                                        <a class="tag" href="#">
                                 @foreach ($pro as $product)
                                 @if ( $product['id'] == $cartItems->id)

                                            @if ($product['gender_id'] == 1)
                                            Men's Clothing
                                            @else
                                            Women's Clothing
                                            @endif

                                        </a>

                                 @endif
                                 @endforeach 



                                        <a class="title" href="/shopping_catalog/{{$cartItems->id}}">{{$cartItems->name}}</a>
                                        <div class="inline-description">{{$cartItems->options->color}}</div>
                                        <!-- <div class="inline-description">Moq : {{$cartItems->options->min}}</div>
                                        <div class="inline-description">Code : {{$cartItems->options->code}}</div> -->

                                        <div class="inline-description">Size:{{$cartItems->options->size}}</div>

                                      

                                        
                                        
                                        
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title">Quantity</div>
                                            <!-- <a href="#"><div class="entry number-minus">&nbsp;</div></a>  --> 
                                            <a class="entry number-minus" href="/updateItem?id={{$cartItems->rowId}}&decrease=1"> &nbsp; </a>         
                                            <div class="entry number" id="qty" name="qty" value="{{$cartItems->qty}}" onmousemove="myFunction()">{{$cartItems->qty}}</div>
                                            <a class="entry number-plus" href="/updateItem?id={{$cartItems->rowId}}&increase=1"> &nbsp; </a> 


                                            <!-- <div class="entry number-plus"><a href="#">&nbsp;</a></div>
 -->
                                            <!-- New Quantity <input type="text" id="new_qty" name="new_qty" class="form-control"/>
                                             -->


                                             
                                            </br>
                                           
                                           <!--  <a href="{{ url('/updateItem', $cartItems->rowId)}}" class="button style-15" onmousemove="myFunction()">Update Cart</a> -->

                                            <a href="{{ url('/removeItem', $cartItems->rowId) }}" class="button style-17">remove</a> 

        

                                         </br>

                                      
                                         



                                        </div>
                                    </div>
                                </div>

                            </div>

                             @endforeach

                                    <div class="detail-info-entry">
                                    
                                    <button type="submit" class="button style-10"><i class="fa fa-shopping-cart"></i> Checkout</button> 

                                    <a href="/shopping_catalog"  class="button style-11" ><i class="fa fa-heart"></i> Continue shopping</a>

                                 <div class="clear"></div>
                                </div>

                        </form>


                           </br></br>

                            

                           <!--  <div class="row">
                                <div class="information-entry col-md-6">
                                    <div class="sale-entry">
                                        <div class="hot-mark red">hot</div>
                                        <div class="sale-price"><span>-40%</span> winter Sale</div>
                                        <div class="sale-description">Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do</div>
                                    </div>
                                </div>
                                <div class="information-entry col-md-6">
                                    <div class="sale-entry">
                                        <div class="hot-mark red">hot</div>
                                        <div class="sale-price"><span>FREE</span> UK delivery</div>
                                        <div class="sale-description">Lorem ipsum dolor sit amet, consectetur adipisc elit, sed do</div>
                                    </div>
                                </div>
                            </div> -->  

                        </div>

                       
                    </div>
                </div>

               <!-- @include('layouts.mango.layout.product_features_inline')  -->

               @include('layouts.mango.layout.product_features_tab_product')              

                <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
               
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">
                        <img class="gallery-image" src="img/portfolio-1.jpg" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.mango.layout.searchbox_popup')


    @include('layouts.mango.layout.cart_popup_item')

    

     @include('layouts.mango.layout.scripts')

   
</body>
</html>