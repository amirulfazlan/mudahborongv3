<!DOCTYPE html>
<html>


  @include('layouts.head')

  <!-- Product Catalog -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset ('p_catalog/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset ('p_catalog/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset ('p_catalog/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset ('p_catalog/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset ('p_catalog/js/respond.min.js') }}"></script>

    <link rel="shortcut icon" href="favicon.png">

  <body class="fixed-header menu-pin">
   


    @if (Auth::check()) 
    @if ( Auth::user()->is_Admin() )
        @include('layouts.sidebar')
        @else
        @include('layouts.sidebarWholesaler')
    @endif
  @else
    @include('layouts.sidebarWholesaler')
  @endif

   <!-- START PAGE-CONTAINER -->
    <div class="page-container ">


     @include('layouts.topbar')

      
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            <div class="panel-heading">     

                        </div>

              <div id="all">
    
        <div id="content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}">MudahBorong</a>
                        </li>
                        <li>Shopping cart</li>
                    </ul>
                </div>

                <div class="col-md-9" id="basket">

                    <div class="box">

                        <form method="post" action="{{url('/basket')}}">
                            {!! csrf_field() !!}

                            <h1>Shopping cart</h1>


                           <script type="text/javascript">

                           $(document).ready(function(e) {   
                           $('#quantity').on('keyup',function(){
                           var tot = $('#price').val() * this.value;
                           $('#total').val(tot);
                           });
                           });


                           $(document).ready(function () {
                           var $inputs = $('input[type="checkbox"]')
                           $inputs.on('change', function () {
                           var sum = 0;
                           $inputs.each(function() {
                           if(this.checked)
                           
                           sum += parseFloat(this.value);
                           });

                           var cal = sum - 1;
                        
                           $("#prine_price").val(cal.toFixed(2));

                           });
                           });

 
                            </script>

                            @if($cart->isEmpty())

                            <p class="text-muted">Your cart is EMPTY! Please add the item that you want to purchase in cart first. </p>

                            @else

                            
                            <p class="text-muted">Here is the list of item currently on your shopping cart. Please checkout to proceed or continue shopping</p>

                            @endif
                            
                            <div class="table-responsive">
                                
                                <table class="table">
                                    <thead>
                                        <tr>
                                            
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Color</th>
                                            <th>Size</th>
                                            <th>Unit price</th>
                                            <th colspan="2">Discount</th>
                                            <th>Prime</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                        <?php 
                                        $total = 0; 
                                        $i = 0;
                                        ?>
                                        @foreach($cart as $cartItems)

                                         <?php $total += $cartItems->subtotal ; 
                                        $sub = $cartItems->price * $cartItems->qty ;
                                        $i++;

                        ?>


                                             
                                       
                                          <input type="hidden" name="product_name[]"  value="{{$cartItems->name}}">
                                          <input type="hidden" name="quantity[]"  value="{{$cartItems->qty}}">
                                          <input type="hidden" name="color[]"  value="{{$cartItems->options->color}}">
                                          <input type="hidden" name="size[]"  value="{{$cartItems->options->size}}">
                                          <input type="hidden" name="pid[]" value="{{$cartItems->id}}">
                                         
                                          <input type="hidden" name="total[]"  value="<?php echo $total; ?>">
                                        
                                    <tbody>
                                        <tr>

                                                     
                                            <td>{{$cartItems->name}}</a>

                                            </td>
                                            <td>
                                                <input type="number" value="{{$cartItems->qty}}" class="form-control" style="width:70px">
                                            </td>
                                            <td>
                                                {{$cartItems->options->color}}

                                            </td>
                                            <td>
                                                {{$cartItems->options->size}}
                                            </td>

                                            <td >  


                                              {{ number_format($cartItems->price, 2) }}
                                             </td>

                                            <td colspan="2" >$0.00</td>
                                           <!-- <td>RM <?php echo $total; ?></td>-->

                                           <td > 

                                          @foreach($prines as $price)

                                          <?php $prine_total =  (number_format($cartItems->price, 2)* $cartItems->qty) *$price['prine_price'] ;

                                          ?>

                                          <input type="checkbox" name="check[]" value="{{$prine_total}}"  />
                                        
                                           @endforeach

                                            </td>
                                            <td><a href="{{ url('/removeItem', $cartItems->rowId) }}"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                            <td><a href="{{ url('/updateItem', $cartItems->rowId) }}"><i class="fa fa-refresh"></i></a>
                                            </td>
                                        </tr>
                                        
                                      </tbody>
                                    



                                    @endforeach

                            
                                    <tfoot>
                                        <tr>
                                            


                                            <th colspan="7">Total</th>
                                            <th colspan="2">RM <?php echo number_format($total, 2); ?></th>
                                         
                                        </tr>
                                        
                                    </tfoot>
                                    
                                </table>

                            </div>
                            <!-- /.table-responsive -->
                            

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{url('/shop_men')}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continue shopping</a>
                                </div>

                                                                 <p class="text-center buttons">
                                   
                                   
                                    
                                </p>

                            


                            
                            </div>

                            

                        </form>

                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class="col-md-3">
                    <div class="box" id="order-summary">
                        <div class="box-header">
                            <h3>Order summary</h3>
                        </div>
                        <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>RM <?php echo number_format($total, 2); ?></th>
                                    </tr>
                                    <tr>
                                        <td>Prime Service</td>
                                        <th>

                                            RM <input type="textbox" name="prine_price" id="prine_price" style="width:50px" value="0.00" />



                                        </th>
                                    </tr>

                                    <tr class="total">
                                        <td>Total</td>
                                        <th>RM <?php echo number_format($total, 2); ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>


                    
                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


             
          <!-- END CONTAINER FLUID -->
            </div>
        <!-- END PAGE CONTENT -->
        <div class="panel-heading">     

                        </div>

        </div>
        </div>

        

       @include('layouts.footer')

      @include('layouts.leftsidebar')
       </div>
      @include('layouts.scripts')

    <script src="{{ asset ('p_catalog/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/waypoints.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/modernizr.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset ('p_catalog/js/front.js') }}"></script>


  </body>
</html>
