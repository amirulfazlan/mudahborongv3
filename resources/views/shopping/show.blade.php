<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<style>



p.thick {
    font-weight: 500;
}
</style>

<body class="style-10">

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->
           

            <div class="content-push">



                <div class="breadcrumb-box">
                    <a href="/shopping_catalog">Shop</a>
                    <a href="/shopping_catalog">@if ($product->product_category['gender_id'] == 1)
                                            Men's Products
                                            @else
                                            Women's Products
                                            @endif</a>

                    <a href="#">{{ $product->product_category['category_product']}}</a>
                    <a href="#">{{ $product['p_name'] }}</a>                         
                </div>

                <div class="information-blocks">
                    <div class="row">

                        @if (Session::has('message'))
   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

                        <div class="col-sm-6 information-entry">
                            <div class="product-preview-box">
                                <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                                    <div class="swiper-wrapper">
                                        
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image"> 
                                                <img src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" data-zoom="{{ asset ('images/main_product/'.$product['main_image']) }}" />
                                            </div>
                                        </div>

                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('images/main_product/'.$product['back_image']) }}" alt="" data-zoom="{{ asset ('images/main_product/'.$product['back_image']) }}" />
                                            </div>
                                        </div>

                                        @foreach($product->variation as $colors)

                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('images/product/'.$colors['main_image'])}}" alt="" data-zoom="{{ asset ('images/product/'.$colors['main_image'])}}" />
                                            </div>
                                        </div>

                                        @endforeach

                                        @foreach($product->photo as $photos)
                                        <div class="swiper-slide">
                                            <div class="product-zoom-image">
                                                <img src="{{ asset ('images/other_image/'.$photos['filename'])}}" alt="" data-zoom="{{ asset ('images/other_image/'.$photos['filename'])}}" />
                                            </div>
                                        </div>
                                        @endforeach

                                       
                                    </div>

                                    <div class="pagination"></div>
                                    
                                    <div class="product-zoom-container">
                                        <div class="move-box"> 
                                            <img class="default-image" src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" />
                                            <img class="zoomed-image" src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" />
                                        </div>
                                        <div class="zoom-area"></div>
                                    </div>

                                </div>
                                <div class="swiper-hidden-edges">
                                    <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                        
                                        <div class="swiper-wrapper">
                                            
                                            <div class="swiper-slide selected">
                                                <div class="paddings-container"> 
                                                    <img src="{{ asset ('images/main_product/'.$product['main_image']) }}" alt="" />
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('images/main_product/'.$product['back_image']) }}" alt="" />
                                                </div>
                                            </div>

                                            @foreach($product->variation as $colors)

                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('images/product/'.$colors['main_image'])}}" alt="" />
                                                </div>
                                            </div>

                                             @endforeach

                                             @foreach($product->photo as $photos)

                                            <div class="swiper-slide">
                                                <div class="paddings-container">
                                                    <img src="{{ asset ('images/other_image/'.$photos['filename'])}}" alt="" />
                                                </div>
                                            </div>

                                             @endforeach


                                             

                                        </div>
                                        <div class="pagination"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 information-entry">
                            <div class="product-detail-box">
                                <h1 class="product-title">{{ $product['p_name'] }}</h1>
                                <h3 class="product-subtitle">@if ($product->product_category['gender_id'] == 1)
                                            Men's Products
                                            @else
                                            Women's Products
                                            @endif</h3>
                             
                                <div class="product-description detail-info-entry">{{ $product['description'] }}</div>
                                <div class="product-description detail-info-entry"><b>MOQ Price</b></div>
                                <div class="price">


                                    @foreach ($cur as $yuan)
                                    @foreach ($margin as $mar)
                                    @if( $mar['id'] == $product['margin_id'] )
                                    @foreach ($moq_item as $moq)
                                    @if( $moq['moqty_id'] == $product['moqty_id'] ) 
                                            
                                    <?php $admin_price1 = ( $product['base_price1'] / $yuan['cur_value']) +  $mar['mar_admin1']  ;
                                          
                                          $whole_price1 =  $admin_price1 + $mar['mar_whole1'] ;



                                          $admin_price2 = ( $product['base_price2'] /  $yuan['cur_value']) + $mar['mar_admin2'] ;

                                          $whole_price2 =  $admin_price2 + $mar['mar_whole2'] ;

                                          $admin_price3 = ( $product['base_price2'] /  $yuan['cur_value'] ) + $mar['mar_admin3'] ;

                                          $whole_price3 =  $admin_price3 + $mar['mar_whole3'] ;
  

                                           $round1= round($whole_price1 ,1,PHP_ROUND_HALF_UP) ;
                                           $round2= round($whole_price2 ,1,PHP_ROUND_HALF_UP) ;
                                           $round3= round($whole_price3 ,1,PHP_ROUND_HALF_UP) ;

                                            $bundar1 = ceil($whole_price1 * 2 ) / 2;

                                            $bundar2 = ceil($whole_price2 * 2 ) / 2;

                                            $bundar3 = ceil($whole_price3 * 2 ) / 2;


                                    ?>


                                     <div class="col-sm-12 col-md-10">
                                    <div class="information-blocks">
                                   <div class="detail-info-lines border-box">

                                     <table class="table table-sm">
                                     <thead>
                                       <tr>
                                              <th><font face = "Verdana" size = "3">  <p style ="color:red">MOQ</p> </font></th>
                                              <th> <font face = "Verdana" size = "3">  <p style ="color:red">{{$moq['min_unit1']}} - more</p> </font></</th>
                                              <th><font face = "Verdana" size = "3"> <p style ="color:red">{{$moq['min_unit2']}} - more </p> </font></th>
                                              <th><font face = "Verdana" size = "3">  <p style ="color:red">{{$moq['min_unit3']}} - more  </p> </font></th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                         <tr>
                                              <th scope="row"><font face = "Verdana" size = "3">  <p style ="color:red">Price</p> </font></font></th>
                                              <td> <div class="current"><font face = "Verdana" size = "3">  <p  style ="color:black">{{ number_format($bundar1, 2) }}</p> </font></font></td>
                                              <td> <div class="current">
                                                <font face = "Verdana" size = "3">  <p style ="color:black"> {{ number_format($bundar2, 2) }}</p> </font> </div></td>

                                              <td> <div class="current">
                                                <font face = "Verdana" size = "3">  <p style ="color:black"> {{ number_format($bundar3, 2) }}</p> </font>           </div>                                 </td>
                                         </tr>
    
                                     </tbody>
                                     </table>
                                 

                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                      

                                                   
                                      @endif 
                                      @endforeach 
                                      @endif 
                                      @endforeach
                                      @endforeach

                                  </br>

                                </div>


                                <div class="clear"></div>

                                 </br>


                                 <form action="/addProduct" method="POST" >
                                    {{ csrf_field() }}

                                     </br>

                                    <div class="clear"></div>

                                    <input type="hidden" name="id"  value="<?php echo $id; ?>">

                                    <div class="form-group row">
                                    <div class="form-group {{ $errors->has('cart_color') ? ' has-error' : '' }}">
                                    <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Color</label>
                                    <div class="col-sm-5">
                                    <div class="simple-drop-down simple-field size-1">
                                    <select id="cart_color" name="cart_color" required>
                                    <span class="caret"></span>
                                    <ul class="dropdown-menu">
                                    <option value="">Choose Color </option>
                                    <div class="dropdown-divider"></div>

                                     @foreach($pro_var as $color)
                                     @if( $color['p_id'] == $product['id'] ) 
                                     <option value="{{$color['p_color']}}">{{$color['p_color']}}</option>
                                     @endif
                                     @endforeach
                                     </ul>
                                     </select>
                                     @if ($errors->has('cart_color'))
                                     <span class="help-block">
                                     <strong>{{ $errors->first('cart_color') }}</strong>
                                     </span>
                                     @endif
                                     </div>
                                     </div>
                                     </div>
                                     </div>

                                 <div class="form-group row">
                                 <div class="form-group {{ $errors->has('cart_size') ? ' has-error' : '' }}">
                                 <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Size</label>
                                 <div class="col-sm-5">
                                 <div class="simple-drop-down simple-field size-1">
                                
                                 <select id="cart_size"  name="cart_size" required>
                                 <span class="caret"></span>
                                 <ul class="dropdown-menu">
                                 <option value="">Choose Size </option>
                                 <div class="dropdown-divider"></div>

                                 @foreach($size_item as $post2)
                                 @if( $post2['st_id'] == $product['st_id'] ) 
                                 <option value="{{$post2['st_item']}}">{{$post2['st_item']}}</option>
                                 @endif
                                 @endforeach
                                 </ul>
                                 </select>

                                 
                                 @if ($errors->has('cart_size'))
                                 <span class="help-block">
                                 <strong>{{ $errors->first('cart_size') }}</strong>
                                 </span>
                                 @endif
                                 </div>
                                 </div>
                                 </div>
                                 </div>

                                <div class="form-group row">
                                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Quantity </label>
                                <div class="col-sm-5">
                                <input type="text" id="cart_quantity" name="cart_quantity" class="form-control" onchange="sum();"/>                            
                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                <strong>{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif
                                </div>
                                </div>
                                </div>


                                  <div class="detail-info-entry">
                                    <button type="submit" class="button style-10"><i class="fa fa-shopping-cart"></i> Add to cart</button> 
                                    <a href="/shopping_catalog"  class="button style-11"><i class="fa fa-heart"></i> Continue shopping</a>

                                 <div class="clear"></div>
                                </div>


                                    </form>

                                    <div class="clear"></div></br>

                                

                                
                                
                            </div>
                        </div>
                    </div>
                </div>

                </br></br>

                <div class="information-blocks">
                    <div class="tabs-container style-1">
                        <div class="swiper-tabs tabs-switch">
                            <div class="title"><strong>PRODUCT INFO</strong></div>
                            <div class="list">
                                <a class="tab-switcher active">Product Description</a>
                                <a class="tab-switcher">Color Description</a>
                                <a class="tab-switcher">Size Description</a>
                                
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div>
                            
                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                            <h4>PRODUCT DETAILS</h4>
                                            <p>{{ $product['description'] }}</p>

                                           
                                        </div>
                                        <div class="col-md-6 information-entry">
                                            <h4>PRODUCT COLOR </h4>
                                            
                                            <ul>
                                                 @foreach($pro_var as $color)  
                                                 @if( $color['p_id'] == $product['id'] ) 
                                                 <li>{{$color['p_color']}}</li>
                                                 @endif
                                                 @endforeach
                                                
                                            </ul>

                                            <h4>PRODUCT Brand </h4>
                                            
                                            <ul>
                                                 @foreach($brand as $brands)  
                                                 @if( $brands['id'] == $product['brand_id'] )

                                                <div class="inline-product-entry">
                                                <a class="image"><img alt="" src="{{ asset ('images/brand/'.$brands['brand_image']) }}"></a>
                                                <div class="content">
                                                <div class="cell-view">
                                                <a href="#" class="title">{{$brands['brand_name']}}</a>
                                                </div>
                                                </div>
                                                <div class="clear"></div>
                                                </div> 
                                                @endif
                                                @endforeach
                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                             <h4>PRODUCT Color </h4>
                                            <ul>
                                                @foreach ($pro_var as $color_list)
                                                @if( $color_list['p_id'] == $product['id'] ) 

                                                <div class="inline-product-entry">
                                                <a class="image"><img alt="" src="{{ asset ('images/product/'. $color_list['main_image']) }}"></a>
                                                <div class="content">
                                                <div class="cell-view">
                                                <a href="#" class="title">{{$color_list['p_color']}}</a>
                                                </div>
                                                </div>
                                                <div class="clear"></div>
                                                </div> 

                                                @endif 
                                                @endforeach
                                                
                                            </ul>
                                        </div>

                                       
                                    </div>
                                </div>
                            </div>

                            <div class="tabs-entry">
                                <div class="article-container style-1">
                                    <div class="row">
                                        <div class="col-md-6 information-entry">
                                             <h4>PRODUCT SIZE & FIT </h4>
                                            <ul>
                                               

                                                @foreach ($size_item as $item)
                                                @if( $item['st_id'] == $product['st_id'] ) 
                                                <li>{{ $item['st_item'] }}</li>
                                                @endif 
                                                @endforeach
                                                
                                            </ul>
                                        </div>

                                        <div class="col-md-6 information-entry">
                                             <h4>PRODUCT SIZE CHART </h4>
                                            <ul>
                                            
                                            <div class="blog-entry">  
                                            <img src="{{ asset ('images/size_chart/'. $product['size_chart']) }}" alt="" style="width:100%;height:auto;" />
                                            </div>
                                                
                                            </ul>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>

                           


                        </div>
                    </div>
                </div>

               

                <!-- @include('layouts.mango.layout.product_features_inline') -->

                @include('layouts.mango.layout.product_features_tab_product')


                 <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

    @include('layouts.mango.layout.searchbox_popup')



    @include('layouts.mango.layout.cart_popup_item')


   
   @include('layouts.mango.layout.scripts')

</body>
</html>
