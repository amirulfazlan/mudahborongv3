<!DOCTYPE html>
<html>

@include('layouts.mango.layout.head')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"> </script>

<body class="style-10">

    <style type="text/css">
        
        img.resize {
        width:210px;
        height:290px;
        }
    </style>

    <!-- LOADER -->
    <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <div class="content-center fixed-header-margin">
            
            <!-- HEADER -->
            @include('layouts.mango.layout.header')
            <!-- HEADER -->
            

            <div class="content-push">

                <div class="breadcrumb-box">
                    <a href="#">Home</a>
                    <a href="/shopping_catalog">Shop</a>
                </div>


                


                <div class="information-blocks">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="page-selector">
                                
                                

                                <div class="shop-grid-controls">
                                    <form method="get" action="/searchfilter">
                                    <div class="entry">
                                        <div class="inline-text">Filter : </div> &nbsp;&nbsp;
                                       
                                        <div class="inline-text">Brand : </div>
                                        <div class="simple-drop-down">  
                                         <select name="brand_id" >
                                         <option value="0">All</option>    
                                         @foreach ($brand as $f_brand)
                                         <option value="{{$f_brand['id']}}">{{$f_brand['brand_name']}}</option>
                                          @endforeach
                                         </select>         
                                        </div>

                                        <div class="inline-text">Price </div>
                                        <div class="simple-drop-down">
                                           
                                         <select name="price" >
                                         <option value="0">All</option>
                                         <option value="1">Low to High</option>
                                         <option value="2">High to Low</option>
                                         </select>


                                        </div>

                                         <button type="submit" class="sort-button"></button>


                                         &emsp;&emsp;

                                        <div class="inline-text">&emsp;View</div>
                                        <div class="view-button active grid"><i class="fa fa-th"></i></div>
                                        <div class="view-button list"><i class="fa fa-list"></i></div></br>
                                        </form>
                                        
                                    </div>

                                    <div class="entry pull-left">
                                        
                                        {{ $product->appends(request()->input())->links() }}
                                        </div>
                                    

                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="row shop-grid grid-view">

                            <?php $i = 0 ?>

                            @foreach($product as $post)

                             <?php $i++ ?>


                                <div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-image">
                                            <a href="{{action('ShoppingCatalog_Controller@show', $post['id'])}}"> 
                                            
                                            <img class="resize" src="{{ 'images/main_product/'.$post['main_image'] }}" alt=""  />  
                                            <img class="resize" src="{{ 'images/main_product/'.$post['back_image'] }}" alt=""  />

                                            </a>
                                           
                                        </div>
                                        <a class="tag" href="#"> 
                                            @if ($post->product_category['gender_id'] == 1)
                                            Men
                                            @else
                                            Women
                                            @endif

                                            {{ $post->product_category['category_product']}}</a>
                                            
                                            <a class="title" href="{{action('ShoppingCatalog_Controller@show', $post['id'])}}"> 
                                                <font  size = "3"><b> {{ $post['p_name']}} </a></b> </font><br />
                                            
                                             
                                            
                                        <div class="article-container style-1">
                                            <p>{{ $post['description']}}</p>
                                        </div>
                                        <div class="price">
                                            <div class="description">From</div>
                                            <div class="current">@foreach ($cur as $yuan)

                                         @foreach ($margin as $mar)
 
                                         @if( $mar['id'] == $post['margin_id'] )

                                         @foreach ($moq_item as $moq)

                                         @if( $moq['moqty_id'] == $post['moqty_id'] )
       
                                         <?php $admin_price = ( $post['base_price2'] /  $yuan['cur_value']) + $mar['mar_admin3'];

                                               $whole_price =  $admin_price + $mar['mar_whole3'] ;

                                               $rounding = round($whole_price ,1,PHP_ROUND_HALF_UP) ;

                                               $bundar = ceil($whole_price * 2 ) / 2
                                         ?>

                                          <font face = "Verdana" size = "3" color="black"> RM {{  number_format($bundar, 2) }} </font><br />

                                         
                                         @endif 
                                         @endforeach 
       
                                         @endif 
       
                                          @endforeach
     
                                          @endforeach

                                            </div>
                                        </div>
                                        <div class="list-buttons">
                                            <a href="{{action('ShoppingCatalog_Controller@show', $post['id'])}}" class="button style-10">View Detail</a>
                                            
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div> 

                             @endforeach  

                             <?php

                             $count_no = count($product);

                             ?> 

                            </div>
                            

                            <div class="page-selector">


                                <div class="shop-grid-controls">
                                                     

                                    <div class="entry">
                                        <div class="description">Showing: {{ $i }} of {{ $count_no }}</div></br> 
                                    
                                        {{ $product->appends(request()->input())->links() }}



                                    </div>
                                    

                                </div>
                                <div class="clear"></div>
                            </div>


                            
                        </div>
                        
                        <div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">
                           <!--  <div class="information-blocks categories-border-wrapper">
                                <div class="block-title size-3">Categories</div>
                                <div class="accordeon">

                                    <div class="accordeon-title"> Products</div>
                                    <div class="accordeon-entry">
                                        <div class="article-container style-1">
                                            <ul>
                                               
                                                 <li><a href="/shopping_catalog">View All</a>
                                                 </li>
                                            </ul>
                                        </div>
                                    </div> 

                                    <div class="accordeon-title">Men's Products</div>
                                    <div class="accordeon-entry">

                                        <div class="article-container style-1">
                                            <ul>

                                                 @foreach ($cat as $cats)
                                                 @if ($cats['gender_id'] == 1)
                                                 <li><a href="/shopping_catalog?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                                 </li>
                                                 @endif
                                                 @endforeach                  
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="accordeon-title">Women's Products</div>
                                    <div class="accordeon-entry">
                                        <div class="article-container style-1">
                                            <ul>
                                                @foreach ($cat as $cats)
                                                 @if ($cats['gender_id'] == 2)
                                                 <li><a href="/shopping_catalog?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                                 </li>
                                                 @endif
                                                 @endforeach

                                            </ul>
                                        </div>
                                    </div> 

                                    <div class="accordeon-title">Unisex's Products</div>
                                    <div class="accordeon-entry">
                                        <div class="article-container style-1">
                                            <ul>
                                                @foreach ($cat as $cats)
                                                 @if ($cats['gender_id'] == 3)
                                                 <li><a href="/shopping_catalog?category_id={{$cats['id']}}">{{$cats['category_product']}}</a>
                                                 </li>
                                                 @endif
                                                 @endforeach

                                            </ul>
                                        </div>
                                    </div> 
                                </div>
                            </div>
 -->

                            <div class="page-selector">
                               <div class="block-title size-2">Shop</div>
                                
                               
                            </div>

         
                        </div>
                    </div>
                </div>
                
                <!-- @include('layouts.mango.layout.product_features_inline') -->

                @include('layouts.mango.layout.product_features_tab_product')




                <!-- FOOTER -->

                @include('layouts.mango.layout.footer')

                <!-- FOOTER -->
                
            </div>

        </div>
        <div class="clear"></div>

    </div>

    <div class="overlay-popup" id="image-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container"> 
                        <img class="gallery-image" src="{{ asset ('mango/img/portfolio-1.jpg') }}" alt="" />
                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.mango.layout.searchbox_popup')


    @include('layouts.mango.layout.cart_popup_item')

   
   @include('layouts.mango.layout.scripts')


    <!-- range slider --> 
    <script src="{{ asset ('mango/js/jquery-ui.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            var minVal = parseInt($('.min-price span').text());
            var maxVal = parseInt($('.max-price span').text());
            $( "#prices-range" ).slider({
                range: true,
                min: minVal,
                max: maxVal,
                step: 5,
                values: [ minVal, maxVal ],
                slide: function( event, ui ) {
                    $('.min-price span').text(ui.values[ 0 ]);
                    $('.max-price span').text(ui.values[ 1 ]);
                }
            });
        });
    </script>

</body>
</html>
