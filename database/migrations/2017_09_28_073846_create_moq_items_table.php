<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoqItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moq_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('moqty_id');
            $table->string('min_unit1'); 
            $table->string('min_unit2');
            $table->string('min_unit3');  
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moq_items');
    }
}
