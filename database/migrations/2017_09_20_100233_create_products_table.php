<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('main_image');
            $table->string('back_image');
            $table->string('p_name');
            $table->string('release_date');
            $table->string('brand_id');
            $table->string('category_id');
            $table->string('gender_id');
            $table->string('st_id');
            $table->string('size_chart');  
            $table->string('moqty_id');
            $table->string('margin_id');
            $table->double('base_price1');
            $table->double('base_price2');    
            $table->string('description');
            $table->string('featured_product');
            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
