<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSizeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('st_id');
            $table->string('st_item');
            $table->timestamps();

             $table->foreign('st_id')
              ->references('id')
              ->on('size_types')
               ->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_items');
    }
}
