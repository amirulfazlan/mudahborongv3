<?php

use Illuminate\Database\Seeder;
use App\AdminMudahborong;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new AdminMudahborong();
        $user->name = "Amirul Fazlan";
        $user->email = "amirulfazlan@mudahborong.com";
        $user->password = crypt("secret","");
        $user->save();
    }
}
