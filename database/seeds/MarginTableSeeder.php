<?php

use Illuminate\Database\Seeder;

class MarginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('margins')->insert([

        [
            'name' => 'Cloth',
            'mar_admin1' => 9.00,
            'mar_admin2' => 7.00,
            'mar_admin3' => 5.00,
            'mar_whole1' => 3.00,
            'mar_whole2' => 3.00,
            'mar_whole3' => 2.50,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ],

        [
            'name' => 'Shoes',
            'mar_admin1' => 18.00,
            'mar_admin2' => 15.00,
            'mar_admin3' => 10.00,
            'mar_whole1' => 10.00,
            'mar_whole2' => 10.00,
            'mar_whole3' => 8.00,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ],




    ]);

    }


}
