<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('currencies')->insert([
            
            'cur_value' =>  1.55 ,
            
        ]);
    }
}
