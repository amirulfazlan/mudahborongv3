<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
   
   {
        $user = new User();
        $user->name = "Amirul Fazlan";
        $user->email = "amirulfazlan@nekod.us";
        $user->admin = "1";
        $user->password = crypt("secret","");
        $user->save();
    }
}
