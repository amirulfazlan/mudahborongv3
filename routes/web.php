<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainPageController@index');


Route::resource('/update_size', 'UpdateSizeChartController');
Route::resource('/update_main-image', 'UpdateMainImageController');
Route::resource('/update_backimage', 'UpdateBackImageController');
Route::resource('/update_pc_main-image', 'UpdatePColorImageController');

Route::resource('/tracking', 'TrackingController');

Route::resource('/newtracking', 'TrackingNewController');


Route::resource('orderhistory', 'HistoryController');
Route::get('/trackinghistory/{id}', 'HistoryController@showtracking');

Route::resource('admin-orderhistory', 'AdminHistoryController');


Route::resource('shop', 'CatalogController');
Route::get('/men', 'CatalogController@index');
Route::get('/women', 'CatalogController@women');



Route::resource('shopping_catalog', 'ShoppingCatalog_Controller');

Route::resource('main', 'MainPageController');

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'ShoppingCatalog_Controller@myformAjax'));

//Route::post('/search', 'ShoppingCatalog_Controller@searchbox');
Route::get('/searchfilter', 'ShoppingCatalog_Controller@searchbox');









Route::get('/ayam', function () {
    return view('layouts.mango.checkout');
});


//angular route

Route::get('/newcheckout' , function(){
  return view('newcheckout.index');
});


// Route::get('/material', function () {
//     return view('material.create');
// });

Route::get('/checkout-shipping', function () {
    return view('checkout.shipping');
});

Route::get('/checkout-delivery', function () {
    return view('checkout.delivery');
});

Route::get('/checkout-payment', function () {
    return view('checkout.payment');
});

Route::get('/checkout-invoice', function () {
    return view('checkout.invoice');
});




Auth::routes();

Route::get('/print', function(){
    return view('admin_history.show');
});

Route::get('/orderadmin' , 'OrderRecordController@index');

Route::get('/printorder/{id}' , 'AdminHistoryController@downloadPDF');
Route::post('/printorder/{id}' , 'AdminHistoryController@downloadPDF');


Route::get('/admin_home', 'AdminHomeController@index');

//Route::get('/cart','CartController@index');
//Route::get('/cart/addItem/{id}','CartController@addItem');

Route::get('/addProduct/{id}', 'CartController@addItem');
Route::post('/addProduct', 'CartController@addItem');

Route::get('/removeItem/{id}', 'CartController@removeItem');

Route::get('/updateItem', 'CartController@updateItem');

Route::post('/updateItem', 'CartController@updateItem');

Route::get('/cart', 'CartController@showCart');

Route::get('/saveDetail', 'Cart_sizeController@masukLaSetan');

Route::resource('cart_size', 'Cart_sizeController');

Route::post('/simpanCart', 'CartController@store');

Route::get('/destroyItem', 'CartController@destroyItem');

Route::post('/destroyItem', 'CartController@destroyItem');




Route::get('/print', 'InvoiceController@printInvoice');

//Route::get('/product_photo', 'Product_photoController@index');
// Route::post('/product_photo', 'Product_photoController@uploadSubmit');
// Route::resource('/product_photo', 'Product_photoController');
// Route::get('/product_photo/{id}', 'CartController@show');

Route::resource('/product_photo', 'Product_PhotosController');

Route::get('/checkout-invoice', function () {
    return view('checkout.invoice');
});

Route::get('/basket' , 'BasketController@index');
Route::post('/basket' , 'BasketController@index');
Route::post('/hantarla' , 'BasketController@store');


Route::resource('category', 'CategoryController');
Route::resource('brand', 'BrandController');
Route::resource('size_type', 'Size_TypesController');
Route::resource('size_item', 'Size_ItemsController');
Route::resource('material', 'MaterialsController');
Route::resource('product', 'ProductController');

Route::resource('product_price', 'Product_PricesController');


Route::resource('customer', 'CustomerController');

Route::resource('prine', 'PrineController');
Route::resource('margin', 'MarginsController');
Route::resource('currency', 'CurrenciesController');




Route::get('/checkout' , 'CheckoutController@index');
Route::post('/formvalidate' , 'CheckoutController@formvalidate');
Route::get('/thankyou', function(){
  return view ('profile.thankyou');
});

//route for profile



Route::get('/orderdetail' , 'ProfileController@orders');

Route::get('/address' , 'ProfileController@address');
Route::get('/tukarla' , 'ProfileController@changeAddress');
Route::post('/changeAddress' , 'ProfileController@tukarAddress');

Route::get('/password' , 'ProfileController@Password');
Route::post('/tukarpassword' , 'ProfileController@updatePassword');

//end route for profile

Route::resource('shop_men', 'ShopController');
Route::get('/shop_women', 'ShopController@women');
Route::resource('order', 'OrderController');
Route::get('/bayar/{id}' , 'PaymentController@CreatePayment');
Route::post('/bayar/{id}' , 'PaymentController@CreatePayment');
Route::get('/bayarshipping' , 'TrackingPaymentController@CreatePayment');
Route::post('/bayarshipping' , 'TrackingPaymentController@CreatePayment');
Route::get('/paymentresponse','PaymentController@PaymentComplete');
Route::post('/paymentresponse','PaymentController@PaymentComplete');
Route::get('webhook', 'GuzzleController@handleWebhook');
Route::get('/testemail' , 'MailController@SendEmail');

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'Product_PricesController@myformAjax'));
Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'ProductController@myformAjax'));
Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'Product_PhotosController@myformAjax'));




Route::resource('moq_type', 'Moq_TypesController');
Route::resource('moq_item', 'Moq_ItemsController');

Route::get('/invoice' , 'InvoiceController@createInvoice');



Route::get('/order', function () {
    return view('order.index');
});

Route::group(['middleware' => 'auth'], function()
{
  
  Route::get('/welcome', function()
  {
       return view('welcome');
       
  })->name('welcome');

  
  
  Route::get('/wholesalerhome', function()
  {
       return view('wholesalerhome');

  })->name('wholesalerhome');

  Route::get('/shopping', function()
  {
       return view('shopping');

  })->name('shopping');

  Route::get('/profile', '\App\Http\Controllers\ProfileController@index')->name('profile');


  Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

  //Route::get('/', 'Auth\LoginController@showCat');


});

  Route::get('session/get', 'SessionController@getSession');
  Route::get('session/put', 'SessionController@storeSessionData');
  Route::get('session/forget', 'SessionController@forgetSession');




